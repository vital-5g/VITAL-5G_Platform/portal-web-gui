# Use the official Node.js image as a base image
FROM node:12

# Set the working directory inside the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json from the vital-5g-gui directory to the working directory
COPY vital-5g-gui/package*.json ./

# Install the project dependencies
RUN npm install

# Copy the rest of the application code from the vital-5g-gui directory to the working directory
COPY vital-5g-gui/ .

# Expose the port that the application will run on
EXPOSE 4200

# Command to start the application
CMD ["npm", "start"]
