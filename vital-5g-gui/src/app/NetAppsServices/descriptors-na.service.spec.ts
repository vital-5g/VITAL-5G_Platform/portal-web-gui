import { TestBed } from '@angular/core/testing';

import { DescriptorsNaService } from './descriptors-na.service';

describe('DescriptorsNaService', () => {
  let service: DescriptorsNaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DescriptorsNaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
