import { TestBed } from '@angular/core/testing';

import { BlueprintsNaService } from './blueprints-na.service';

describe('BlueprintsNaService', () => {
  let service: BlueprintsNaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlueprintsNaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
