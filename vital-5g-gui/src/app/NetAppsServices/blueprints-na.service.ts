import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { NaBlueprintInfo } from "../blueprints-components/blueprints-net-apps/na-blueprint-info";
import { environment } from "../environments/environments";
import { AuthService } from "../auth.service";
import { NaBlueprintDoc } from "../blueprints-components/blueprints-net-apps/na-blueprint-doc";

@Injectable({
  providedIn: "root",
})
export class BlueprintsNaService {
  private baseUrl = environment.portalBaseUrl;

  private vsBlueprintInfoUrl = "vsblueprint";
  private naBlueprintInfoUrl = "netapppackages";
  private naBlueprintDocsUrl = "softwaredocs";

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token"),
    }),
  };

  constructor(private http: HttpClient, private authService: AuthService) {}

  getVsBlueprints(): Observable<NaBlueprintInfo[]> {
    console.log('na getVsBlueprints');
    return this.http
      .get<NaBlueprintInfo[]>(
        this.baseUrl + this.vsBlueprintInfoUrl,
        this.httpOptions
      )
      .pipe(
        tap((_) => console.log("fetched vsBlueprintInfos - SUCCESS")),
        catchError(
          this.authService.handleError<NaBlueprintInfo[]>("getVsBlueprints", [])
        )
      );
  }

  getVsBlueprint(vsBlueprintId: string): Observable<NaBlueprintInfo> {
    return this.http
      .get<NaBlueprintInfo>(
        this.baseUrl + this.vsBlueprintInfoUrl + "/" + vsBlueprintId,
        this.httpOptions
      )
      .pipe(
        tap((_) => console.log("fetched vsBlueprintInfo - SUCCESS")),
        catchError(
          this.authService.handleError<NaBlueprintInfo>("getVsBlueprint")
        )
      );
  }

  postVsBlueprint(onBoardVsRequest: Object): Observable<String> {
    let httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/x-zip-compressed",
        Authorization: "Bearer " + localStorage.getItem("token"),
      }),
    };

    // const options = {
    //   params: params,
    //   reportProgress: true,
    // };

    return this.http
      .post(
        this.baseUrl + this.vsBlueprintInfoUrl,
        onBoardVsRequest,
        this.httpOptions
      )
      .pipe(
        tap((blueprintId: String) =>
          this.authService.log(
            `added VS Blueprint w/ id=${blueprintId}`,
            "SUCCESS",
            true
          )
        ),
        catchError(this.authService.handleError<String>("postVsBlueprint"))
      );
  }

  deleteVsBlueprint(blueprintId: string): Observable<String> {
    return this.http
      .delete(
        this.baseUrl + this.vsBlueprintInfoUrl + "/" + blueprintId,
        this.httpOptions
      )
      .pipe(
        tap((result: String) =>
          this.authService.log(
            `deleted VS Blueprint w/ id=${blueprintId}`,
            "SUCCESS",
            true
          )
        ),
        catchError(this.authService.handleError<String>("deleteVsBlueprint"))
      );
  }

  getNaBlueprints(): Observable<NaBlueprintInfo[]> {
    return this.http
      .get<NaBlueprintInfo[]>(
        this.baseUrl + this.naBlueprintInfoUrl,
        this.httpOptions
      )
      .pipe(
        tap((_) => console.log("fetched NetAppBlueprintInfos - SUCCESS")),
        catchError(
          this.authService.handleError<NaBlueprintInfo[]>(
            "getNetAppBlueprints",
            []
          )
        )
      );
  }

  getNaBlueprint(naBlueprintId: string): Observable<NaBlueprintInfo> {
    return this.http
      .get<NaBlueprintInfo>(
        this.baseUrl +
          this.naBlueprintInfoUrl +
          "/" +
          naBlueprintId +
          "/blueprint",
        this.httpOptions
      )
      .pipe(
        tap((_) => console.log("fetched NaBlueprintInfo - SUCCESS")),
        catchError(
          this.authService.handleError<NaBlueprintInfo>("getNetAppBlueprint")
        )
      );
  }

  getNaBlueprintZIP(naBlueprintId: string) {
     return this.http.get(this.baseUrl + this.naBlueprintInfoUrl + "/" + naBlueprintId, {
      responseType: 'arraybuffer',
      headers:{
        Authorization: "Bearer " + localStorage.getItem("token"),
      }});  
  }

  // postNaBlueprint(onBoardVsRequest: Object): Observable<String> {
  //   return this.http.post(this.baseUrl + this.naBlueprintInfoUrl, onBoardVsRequest, this.httpOptions)
  //     .pipe(
  //       tap((blueprintId: String) => this.authService.log(`added Net App Blueprint w/ id=${blueprintId}`, 'SUCCESS', true)),
  //       catchError(this.authService.handleError<String>('postVsBlueprint'))
  //     );
  // }

  postNaBlueprint(onBoardNetAppRequest: Object) {
    let httpOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'multipart/form-data',
        Accept: "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      }),
    };
    return this.http.post(
      this.baseUrl + this.naBlueprintInfoUrl + "/",
      onBoardNetAppRequest,
      httpOptions
    ).pipe(
      tap((_) => console.log("Post NaBlueprintInfo - SUCCESS")),
      catchError(
        this.authService.handleError<NaBlueprintInfo>("postNetAppBlueprint")
      )
    );
  }

  deleteNaBlueprint(blueprintId: string): Observable<String> {
    return this.http
      .delete(
        this.baseUrl +
          this.naBlueprintInfoUrl +
          "/" +
          blueprintId,
        this.httpOptions
      )
      .pipe(
        tap((result: String) =>
          this.authService.log(
            `deleted NetApp Blueprint w/ id=${blueprintId}`,
            "SUCCESS",
            true
          )
        ),
        catchError(
          this.authService.handleError<String>("deleteNetAppBlueprint")
        )
      );
  }
  forcedeleteNaBlueprint(blueprintId: string): Observable<String> {
    return this.http
      .delete(
        this.baseUrl +
          this.naBlueprintInfoUrl +
          "/" +
          blueprintId +
          "?forced=true",
        this.httpOptions
      )
      .pipe(
        tap((result: String) =>
          this.authService.log(
            `deleted NetApp Blueprint w/ id=${blueprintId}`,
            "SUCCESS",
            true
          )
        ),
        catchError(
          this.authService.handleError<String>("deleteNetAppBlueprint")
        )
      );
  }
  
  getNaBlueprintDocs(naBlueprintId: string): Observable<NaBlueprintDoc> {
    return this.http.get<NaBlueprintDoc>(this.baseUrl + this.naBlueprintInfoUrl + "/" + naBlueprintId + "/" + this.naBlueprintDocsUrl, this.httpOptions)
      .pipe(
        tap((_) => console.log("fetched NaBlueprintDocs - SUCCESS")),
          catchError(this.authService.handleError<NaBlueprintDoc>("getNetAppBlueprintDocs"))
      )
  }
  getNaBlueprintDoc(naBlueprintId: string, softwaredocId: string) {
    return this.http.get(this.baseUrl + this.naBlueprintInfoUrl + "/" + naBlueprintId + "/" + this.naBlueprintDocsUrl + "/" + softwaredocId,
      {
        responseType: 'arraybuffer',
        headers:{
          Authorization: "Bearer " + localStorage.getItem("token"),
        }})
  }
}
