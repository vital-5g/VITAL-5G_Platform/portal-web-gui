export class CoverageAreaInfo {
    coverageArea: string;
    vsBlueprintId: string;
    sst: string;
    endPointId: string;
    rat: string[];
}
