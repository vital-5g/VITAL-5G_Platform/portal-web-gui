import { VsBlueprint } from './vs-blueprint';

export class VsBlueprintInfo {
    verticalServiceBlueprintId: string;
    version: string;
    name: string;
    onBoardedNsdInfoId: string[];
    onBoardedVnfPackageInfoId: string[];
    onBoardedMecAppPackageInfoId: string[];
    activeVsdId: string[];
    vsBlueprint: VsBlueprint;
    serviceParameters:any;
}
