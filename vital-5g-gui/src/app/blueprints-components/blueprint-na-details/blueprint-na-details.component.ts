import { Component, OnInit, ViewChild } from "@angular/core";
import { VsbDetailsService } from "../../vsb-details.service";
import { MatSort } from "@angular/material/sort";
import { MatTable } from "@angular/material/table";
import { BlueprintsNaDetailsItemKV, BlueprintsNaDetailsDataSource } from "./blueprints-na-details.datasource";
import { NaBlueprintInfo } from "../blueprints-net-apps/na-blueprint-info";
import { CtxBlueprintInfo } from "../blueprints-ec/ctx-blueprint-info";
import { BlueprintsEcService } from "../../blueprints-ec.service";
import { NaDescriptorInfo  } from "../../descriptors-na/na-descriptor-info";
import { stringify } from "@angular/compiler/src/util";
import { BlueprintsNaService } from "src/app/NetAppsServices/blueprints-na.service";
import { DescriptorsNaService } from "src/app/NetAppsServices/descriptors-na.service";

@Component({
  selector: 'app-blueprint-na-details',
  templateUrl: './blueprint-na-details.component.html',
  styleUrls: ['./blueprint-na-details.component.css'],
  styles: [
    `
      app-blueprints-graph {
        height: 70vh;
        float: left;
        width: 100%;
        position: relative;
      }
    `,
  ],
})
export class BlueprintNaDetailsComponent implements OnInit {
  node_name: string;
  isComposite: boolean = false;

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false })
  table: MatTable<BlueprintsNaDetailsItemKV>;
  dataSource: BlueprintsNaDetailsDataSource;

  graphData = {
    nodes: [],
    edges: [],
  };

  tableData: BlueprintsNaDetailsItemKV[] = [];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ["key", "value"];
  vsbId ='-1';
  constructor(
    public vsbDetailservice: VsbDetailsService,
    private blueprintsNaService: BlueprintsNaService,
    private ctxBlueprintService: BlueprintsEcService,
    private naDescriptorService: DescriptorsNaService
  ) {}

  ngOnInit() {
    this.vsbId = localStorage.getItem("nabId");
    this.dataSource = new BlueprintsNaDetailsDataSource(
      this.vsbDetailservice._vsBlueprintDetailsItems
    );
    this.getVsBlueprint(this.vsbId);
  }

  nodeChange(event: any) {
    this.node_name = event;
  }

  getVsBlueprint(vsbId: string) {
    this.blueprintsNaService
      .getNaBlueprint(vsbId)
      .subscribe((NaBlueprintInfo: NaBlueprintInfo) => {
            this.ctxBlueprintService
                this.blueprintsNaService
                  .getNaBlueprints()
                  .subscribe((naBlueprintsInfo: NaBlueprintInfo[]) => {

                    console.log(NaBlueprintInfo);
                    
                    var vsBlueprint = NaBlueprintInfo["naBlueprint"];
                    var vsBlueprint = NaBlueprintInfo;
                    //console.log(JSON.stringify(vsBlueprint));
                    if (vsBlueprint["interSite"] !== undefined) {
                      this.isComposite = vsBlueprint["interSite"];
                      //console.log(vsBlueprint.interSite);
                    }
                    this.tableData.push({
                      key: "Name",
                      value: [vsBlueprint["name"]],
                    });
                    //this.tableData.push({key: "Id", value: [vsBlueprint['blueprintId']]});
                    this.tableData.push({
                      key: "Version",
                      value: [vsBlueprint["version"]],
                    });

                   

                    this.tableData.push({
                      key: "Description",
                      value: [vsBlueprint["description"]],
                    });

                    this.tableData.push({
                      key: "Testbed",
                      value: [vsBlueprint["testbed"]],
                    });




                  var values = [];
                     
                    if(vsBlueprint["accessLevel"]){
                      values.push(  `${ vsBlueprint["accessLevel"] }`  );
                    this.tableData.push({ key: "Access Level", value: values });  
                  }

                  values = [];

                  
                  if(vsBlueprint["specLevel"]){
                    values.push(  `${ vsBlueprint["specLevel"] }`  );
                  this.tableData.push({ key: "Spec Level", value: values });  
                }

                values = [];

                
                if(vsBlueprint["type"]){
                  values.push(  `${ vsBlueprint["type"] }`  );
                this.tableData.push({ key: "Type", value: values });  
              }

              values = [];

              if(vsBlueprint["atomicComponents"]){
                var valuesTo = vsBlueprint["atomicComponents"];
                for (var i = 0; i < valuesTo.length; i++) {
                   if(!values.some(x=> x == valuesTo[i].placement)){
                     values.push(  `${valuesTo[i].placement}`  );
                   }
                }
                this.tableData.push({ key: "Placements", value: values });  
              }

              values = [];



                    var values = [];
                    if (vsBlueprint["parameters"] !== undefined) {
                      for (
                        var i = 0;
                        i < vsBlueprint["parameters"].length;
                        i++
                      ) {
                        values.push(
                          vsBlueprint["parameters"][i]["parameterName"]
                        );
                      }
                      this.tableData.push({ key: "Parameters", value: values });
                    }
                    values = [];

                    var atomicComponents = vsBlueprint["atomicComponents"];
                    for (var i = 0; i < atomicComponents.length; i++) {
                      if (
                        atomicComponents[i]["placement"] !== null &&
                        atomicComponents[i]["placement"] !== undefined &&
                        atomicComponents[i]["placement"] !== ""
                      ) {
                        values.push(
                          atomicComponents[i]["componentId"] +
                            " (" +
                            atomicComponents[i]["placement"].toLowerCase() +
                            ")"
                        );
                      } else {
                        values.push(atomicComponents[i]["componentId"]);
                      }
                    }
                    this.tableData.push({ key: "Components", value: values });

                    values = [];
                  
                    if(vsBlueprint["endPoints"]){
                      var endPoints = vsBlueprint["endPoints"];
                      for (var i = 0; i < endPoints.length; i++) {
                        if(!endPoints[i]['mobileConnection']){
                          if(endPoints[i].sliceProfile && endPoints[i].sliceProfile.length > 0){
                            values.push(  `${endPoints[i]["endPointId"]}, ${endPoints[i]["type"]}, ${endPoints[i].sliceProfile[0].sliceProfileType}`  );
                          }
                          else{
                            values.push(  `${endPoints[i]["endPointId"]}, ${endPoints[i]["type"]}`  );
                          }
                        }
                      }
                      this.tableData.push({ key: "Endpoints", value: values });  
                    }
  
                    values = [];

                    if(vsBlueprint["endPoints"]){
                      var endPoints = vsBlueprint["endPoints"];
                      for (var i = 0; i < endPoints.length; i++) {
                        if(endPoints[i]['mobileConnection']){

                    

                          if(endPoints[i].sliceProfile && endPoints[i].sliceProfile.length > 0){
                            let par = '';
                            if(endPoints[i].sliceProfile[0].profileParams && Object.keys(endPoints[i].sliceProfile[0].profileParams).length > 0){
                              par = ', Params: ';
                              let pParams = endPoints[i].sliceProfile[0].profileParams; 
                              for (var key in pParams){
                                if (pParams.hasOwnProperty(key)) {
                                  par += `[${key}: ${pParams[key]}] ,`
                                }
                              }
                              //remove last char
                              par = par.trim().slice(0, -1)
                            }

                            // values.push(  `${endPoints[i]["endPointId"]}, EP type: ${endPoints[i]["type"]}, slice Profile: ${endPoints[i].sliceProfile[0].sliceProfileType} ${par}`  );
                            values.push(  `${endPoints[i]["endPointId"]}, Slice Profile: ${endPoints[i].sliceProfile[0].sliceProfileType} ${par}`  );
                          }
                          else{
                            // values.push(  `${endPoints[i]["endPointId"]}, EP type: ${endPoints[i]["type"]}, (No Slice profile)`  );
                            values.push(  `${endPoints[i]["endPointId"]}, (No Slice profile)`  );

                            //Id, EP type:External, (No Slice profile)
                            //Id, EP type:External, slice Profile:URLLC, params:- [availability:99], [latancy:60]
                          }
                        }
                      }
                      this.tableData.push({ key: "5G Endpoints", value: values });  
                    }
  
                    var values = [];
                    if(vsBlueprint['applicationMetrics'] && vsBlueprint['applicationMetrics'].length > 0){
                      for(let i =0; i<vsBlueprint['applicationMetrics'].length;i++){
                        values.push(vsBlueprint['applicationMetrics'][i]['name']) 
                      }

                      this.tableData.push({
                        key: "Application Metrics",
                        value: values,
                      });
                    }


                    values = [];

                    if(vsBlueprint["infrastructureMetrics"]){
                      var valuesTo = vsBlueprint["infrastructureMetrics"];
                      for (var i = 0; i < valuesTo.length; i++) {
                        let name = valuesTo[i].name;
                        let ids = '';
                        for(let j=0; j< valuesTo[i].componentIds.length;j++){
                            ids+= (valuesTo[i].componentIds[j]+ ',')
                        }
                        ids = ids.substring(0, ids.length - 1);

                        ids == ''?values.push(  `${name}`  ):values.push(  `${name} ( ${ids} )`  );
                      }
                      this.tableData.push({ key: "Infrastructure Metrics", value: values });  
                    }
  
                    values = [];



                    if(vsBlueprint["applicationMetrics"]){
                      var valuesTo = vsBlueprint["applicationMetrics"];
                      for (var i = 0; i < valuesTo.length; i++) {
                        values.push(  `${ valuesTo[i].name}`  );
                      }
                      this.tableData.push({ key: "Application Metrics", value: values });  
                    }
  
                    values = [];
                    
              if(vsBlueprint["providedInterfaceSpec"]){
                var valuesTo = vsBlueprint["providedInterfaceSpec"];
                for (var i = 0; i < valuesTo.length; i++) {
                  values.push(  `${ valuesTo[i].name}, ${ valuesTo[i].dataFormat}, ${ valuesTo[i].protocol}`  );
                }
                this.tableData.push({ key: "Interface specifications", value: values });  
              }

              values = [];




                    /*
        if( vsBlueprint['compatibleContextBlueprint'] !== undefined){
          for (var i = 0; i < vsBlueprint['compatibleContextBlueprint'].length; i++) {
            for(var j = 0; j < ctxBlueprintInfos.length; j ++){
              if (vsBlueprint['compatibleContextBlueprint'][i] === ctxBlueprintInfos[j]['ctxBlueprintId']){
                values.push(ctxBlueprintInfos[j]['name']);
              }
            }
          }

          this.tableData.push({key: "Context Blueprints", value: values});
        }
        */
                    values = [];
                    //TODO [removed]
                    // vsBlueprint["compatibleSites"] = []
                    // for (
                    //   var i = 0;
                    //   i < vsBlueprint["compatibleSites"].length;
                    //   i++
                    // ) {
                    //   values.push(vsBlueprint["compatibleSites"][i]);
                    // }
                    // this.tableData.push({
                    //   key: "Compatible Sites",
                    //   value: values,
                    // });

                    //console.log(this.tableData);
                    this.vsbDetailservice.updateVSBTable(this.tableData);
                    this.dataSource = new BlueprintsNaDetailsDataSource(
                      this.tableData
                    );
                    this.dataSource.sort = this.sort;
                    this.table.dataSource = this.dataSource;

                    /*
        values = [];

        for (var i = 0; i < NaBlueprintInfo['activeVsdId'].length; i++) {
          for(var j = 0; j < naDescriptors.length; j++) {
            if (NaBlueprintInfo['activeVsdId'][i] === naDescriptors[j]['vsDescriptorId']){
              values.push(naDescriptors[j]['name']);
            }
          }
        }
        this.tableData.push({key: "Active Vsds", value: values});
*/


                    var atomicComponentsCps = [];

                    if (this.isComposite == false) {
                      for (var i = 0; i < atomicComponents.length; i++) {
                        this.graphData.nodes.push({
                          data: {
                            id: atomicComponents[i]["componentId"],
                            name: atomicComponents[i]["componentId"],
                            weight: 70,
                            colorCode: "white",
                            shapeType: "ellipse",
                          },
                          classes: "bottom-center vnf",
                        });

                        atomicComponentsCps.push(
                          ...atomicComponents[i]["endPointsIds"]
                        );
                        //console.log(atomicComponentsCps);
                      }

                      var sapCps = [];
                      
                      for (var i = 0; i < endPoints.length; i++) {
                        if (
                          atomicComponentsCps.includes(
                            endPoints[i]["endPointId"]
                          )
                        ) { 
                          if(endPoints[i].sliceProfile !== undefined){
                            console.log(endPoints[i].sliceProfile[0].profileParams);
                            let pParams = endPoints[i].sliceProfile[0].profileParams; 
                                for (var key in pParams){
                                  if (pParams.hasOwnProperty(key)) {
                                    //par += `[${key}: ${pParams[key]}] ,`
                                 
                          sapCps.push(endPoints[i]["endPointId"]);
                          this.graphData.nodes.push({
                            data: {
                              id: endPoints[i]["endPointId"],
                              name: `[${key}: ${pParams[key]}],${endPoints[i]["endPointId"]}`,
                              weight: 50,
                              colorCode: "white",
                              shapeType: "ellipse",
                            },
                            classes: "bottom-center sap",
                          }); 
                        }
                        }
                        }
                        else{
                          sapCps.push(endPoints[i]["endPointId"]);
                          this.graphData.nodes.push({
                            data: {
                              id: endPoints[i]["endPointId"],
                              name: `${endPoints[i]["endPointId"]}`,
                              weight: 50,
                              colorCode: "white",
                              shapeType: "ellipse",
                            },
                            classes: "bottom-center sap",
                        })
                        }
                      }
                    }
                      
                      if(vsBlueprint["connectivityServices"])
                        var connectivityServices = vsBlueprint["connectivityServices"];
                      else
                        var connectivityServices = [];
                      for (var i = 0; i < connectivityServices.length; i++) {
                        this.graphData.nodes.push({
                          data: {
                            id: "conn_service_" + i,
                            name: connectivityServices[i]["name"],
                            weight: 50,
                            colorCode: "white",
                            shapeType: "ellipse",
                          },
                          classes: "bottom-center net",
                        });

                        for (var j = 0; j < atomicComponents.length; j++) {
                          for (
                            var h = 0;
                            h < atomicComponents[j]["endPointsIds"].length;
                            h++
                          ) {
                            if (
                              connectivityServices[i]["endPointIds"].includes(
                                atomicComponents[j]["endPointsIds"][h]
                              )
                            ) {
                              this.graphData.edges.push({
                                data: {
                                  source: atomicComponents[j]["componentId"],
                                  target: "conn_service_" + i,
                                  colorCode: "black",
                                  strength: 70,
                                },
                              });
                            }
                          }
                        }

                        for (var j = 0; j < sapCps.length; j++) {
                          if (
                            connectivityServices[i]["endPointIds"].includes(
                              sapCps[j]
                            )
                          ) {
                            this.graphData.edges.push({
                              data: {
                                source: sapCps[j],
                                target: "conn_service_" + i,
                                colorCode: "grey",
                                strength: 70,
                              },
                            });
                          }
                        }
                      }
                    } else {
                      //console.log(JSON.stringify(vsBlueprint));
                      for (let a = 0; a < naBlueprintsInfo.length; a++) {
                        var element = naBlueprintsInfo[a]["vsBlueprint"];
                        for (
                          let b = 0;
                          b < vsBlueprint.atomicComponents.length;
                          b++
                        ) {
                          if (
                            element.blueprintId ===
                            vsBlueprint.atomicComponents[b]["associatedVsbId"]
                          ) {
                            for (
                              var i = 0;
                              i < element.atomicComponents.length;
                              i++
                            ) {
                              this.graphData.nodes.push({
                                data: {
                                  id:
                                    element.atomicComponents[i]["componentId"] +
                                    "_" +
                                    vsBlueprint.atomicComponents[b][
                                      "compatibleSite"
                                    ],
                                  name:
                                    element.atomicComponents[i]["componentId"],
                                  weight: 70,
                                  colorCode: "white",
                                  shapeType: "ellipse",
                                },
                                classes: "bottom-center vnf",
                              });
                              for (
                                let c = 0;
                                c <
                                element.atomicComponents[i]["endPointsIds"]
                                  .length;
                                c++
                              ) {
                                atomicComponentsCps.push(
                                  element.atomicComponents[i]["endPointsIds"][
                                    c
                                  ] +
                                    "_" +
                                    vsBlueprint.atomicComponents[b][
                                      "compatibleSite"
                                    ]
                                );
                              }
                              //atomicComponentsCps.push(...element.atomicComponents[i]['endPointsIds']+'_'+vsBlueprint.atomicComponents[b]['compatibleSite']);
                              //console.log(atomicComponentsCps);
                            }
                            var sapCps = [];
                            for (var i = 0; i < element.endPoints.length; i++) {
                              if (
                                !atomicComponentsCps.includes(
                                  element.endPoints[i]["endPointId"] +
                                    "_" +
                                    vsBlueprint.atomicComponents[b][
                                      "compatibleSite"
                                    ]
                                )
                              ) {
                                sapCps.push(
                                  element.endPoints[i]["endPointId"] +
                                    "_" +
                                    vsBlueprint.atomicComponents[b][
                                      "compatibleSite"
                                    ]
                                );
                                this.graphData.nodes.push({
                                  data: {
                                    id:
                                      element.endPoints[i]["endPointId"] +
                                      "_" +
                                      vsBlueprint.atomicComponents[b][
                                        "compatibleSite"
                                      ],
                                    name: element.endPoints[i]["endPointId"],
                                    weight: 50,
                                    colorCode: "white",
                                    shapeType: "ellipse",
                                  },
                                  classes: "bottom-center sap",
                                });
                              }
                            }
                            //                  console.log(JSON.stringify(this.graphData));
                            var connectivityServices =
                              element["connectivityServices"];
                            //console.log(JSON.stringify(connectivityServices));
                            for (
                              var i = 0;
                              i < connectivityServices.length;
                              i++
                            ) {
                              this.graphData.nodes.push({
                                data: {
                                  id:
                                    "conn_service_" +
                                    i +
                                    "_" +
                                    vsBlueprint.atomicComponents[b][
                                      "compatibleSite"
                                    ],
                                  name: connectivityServices[i]["name"],
                                  weight: 50,
                                  colorCode: "white",
                                  shapeType: "ellipse",
                                },
                                classes: "bottom-center net",
                              });

                              for (
                                var j = 0;
                                j < element.atomicComponents.length;
                                j++
                              ) {
                                for (
                                  var h = 0;
                                  h <
                                  element.atomicComponents[j]["endPointsIds"]
                                    .length;
                                  h++
                                ) {
                                  if (
                                    connectivityServices[i][
                                      "endPointIds"
                                    ].includes(
                                      element.atomicComponents[j][
                                        "endPointsIds"
                                      ][h]
                                    )
                                  ) {
                                    this.graphData.edges.push({
                                      data: {
                                        source:
                                          element.atomicComponents[j][
                                            "componentId"
                                          ] +
                                          "_" +
                                          vsBlueprint.atomicComponents[b][
                                            "compatibleSite"
                                          ],
                                        target:
                                          "conn_service_" +
                                          i +
                                          "_" +
                                          vsBlueprint.atomicComponents[b][
                                            "compatibleSite"
                                          ],
                                        colorCode: "black",
                                        strength: 70,
                                      },
                                    });
                                  }
                                }
                              }

                              for (var j = 0; j < sapCps.length; j++) {
                                for (
                                  var k = 0;
                                  k <
                                  connectivityServices[i]["endPointIds"].length;
                                  k++
                                ) {
                                  if (
                                    connectivityServices[i]["endPointIds"][k] +
                                      "_" +
                                      vsBlueprint.atomicComponents[b][
                                        "compatibleSite"
                                      ] ===
                                    sapCps[j]
                                  ) {
                                    this.graphData.edges.push({
                                      data: {
                                        source: sapCps[j],
                                        target:
                                          "conn_service_" +
                                          i +
                                          "_" +
                                          vsBlueprint.atomicComponents[b][
                                            "compatibleSite"
                                          ],
                                        colorCode: "grey",
                                        strength: 70,
                                      },
                                    });
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                    this.vsbDetailservice.updateVSBGraph(this.graphData);
                  });
          
      });
  }

  downloadNetApp(){
    this.blueprintsNaService
    .getNaBlueprint(this.vsbId)
    .subscribe((NaBlueprintInfo: NaBlueprintInfo) => {
      var sJson = JSON.stringify(NaBlueprintInfo);
      var element = document.createElement('a');
      element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(sJson));
      element.setAttribute('download', "netApp.json");
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click(); // simulate click
      document.body.removeChild(element);
    });
  }

  downloadNetAppZIP(){

    this.blueprintsNaService.getNaBlueprintZIP(this.vsbId)
    .subscribe(data => this.getZipFile(data)),
        error => console.log("Error downloading the file."),
        () => console.log('Completed file download.');
  }

  getZipFile(data: any){
        var a: any = document.createElement("a");
        document.body.appendChild(a);

        a.style = "display: none";
        var blob = new Blob([data], { type: 'application/zip' });

        var url= window.URL.createObjectURL(blob);

        a.href = url;
        a.download = `netapp-${this.vsbId}.zip`;
        a.click();
        window.URL.revokeObjectURL(url);

    }
}
