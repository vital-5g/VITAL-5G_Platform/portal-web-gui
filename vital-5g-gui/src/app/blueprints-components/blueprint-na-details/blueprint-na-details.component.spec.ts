import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueprintNaDetailsComponent } from './blueprint-na-details.component';

describe('BlueprintNaDetailsComponent', () => {
  let component: BlueprintNaDetailsComponent;
  let fixture: ComponentFixture<BlueprintNaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueprintNaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueprintNaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
