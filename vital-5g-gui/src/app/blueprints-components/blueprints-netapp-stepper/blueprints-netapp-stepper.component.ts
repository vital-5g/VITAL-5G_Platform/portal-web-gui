import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/auth.service";
import { EncService } from "src/app/enc.service";
import { BlueprintsNaService } from "src/app/NetAppsServices/blueprints-na.service";
import {
  AcceptedTypes,
  namesToInclude,
  MaxFileSize,
  files,
} from "src/app/Validation/blueprintUploadValidator";

@Component({
  selector: "app-blueprints-netapp-stepper",
  templateUrl: "./blueprints-netapp-stepper.component.html",
  styleUrls: ["./blueprints-netapp-stepper.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlueprintsNetappStepperComponent implements OnInit {
  @Output() netAppEmitter = new EventEmitter<boolean>();
  filesStep1: any[] = [];
  filesStep2: any[] = [];
  @ViewChild("blueprintFile", { static: false }) fileDropElStep1: ElementRef;
  @ViewChild("vnfFiles", { static: false }) fileDropElStep2: ElementRef;
  @ViewChild("stepper", { static: false }) stepper: any;

  validator: boolean;
  dfs: String[] = [];
  showSteps: boolean;

  instLevels: String[] = [];

  translationParams: String[] = [];

  items: FormArray;
  firstStepValid=false;
  secondStepValid=false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(
    // @Inject(DOCUMENT) document,
    private _formBuilder: FormBuilder,
    private blueprintsNaService: BlueprintsNaService,
    // private blueprintsNetAppsComponent: BlueprintsNetAppsComponent,
    // private nsdsService: NsdsService,
    private authService: AuthService,
    private encService: EncService
  ) {}

  ngOnInit(): void {
    this.showSteps = true;
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ["", Validators.required],
      deploymentType: [""],
    });
  }

  ///////////////////
  // step 1 function
  ///////////////////

  bluePrintFileFormData;
  // on file upload
  onUploadedNab(event: any, Nabs: File[]) {
    let promises = [];

    for (let Nab of Nabs) {
      // if(Nab.type=='application/json' && Nab.name.includes('json')){
      // if(AcceptedTypes.some(x=> x == Nab.type) && namesToInclude.some(x=> x == Nab.name) && Nab.size <= MaxFileSize){
      //console.log(Nab.name);
      //console.log(Nab.type);
      //debugger

      if (
        AcceptedTypes.indexOf(Nab.type) &&
        Nab.name.includes(namesToInclude[files.ZIP]) &&
        Nab.size <= MaxFileSize
      ) {
        //add file to view
        this.prepareFilesList(Nabs, 1);

        this.bluePrintFileFormData = new FormData();
        this.bluePrintFileFormData.append("file", Nab);
        this.firstStepValid =true;

      } else {
      this.firstStepValid =false;
        this.authService.log("the file is not ZIP", "FAILED", false);
        (<HTMLInputElement>document.getElementById("firstNext")).disabled =
          true;
      }
    }
  }

  // add files to the view
  prepareFilesList(files: Array<any>, stepper) {
    if (stepper == 1) {
      for (const item of files) {
        item.progress = 0;
        this.filesStep1.push(item);
      }
      this.fileDropElStep1.nativeElement.value = "";
    } else if (stepper == 2) {
      for (const item of files) {
        item.progress = 0;
        this.filesStep2.push(item);
      }
      this.fileDropElStep2.nativeElement.value = "";
    }
  }

  deleteFile(index: number = -1, stepper, forceAll = false) {
    if (forceAll) {
      if (stepper == 1) {
        this.filesStep1.splice(0, 1);
      } else if (stepper == 2) {
        this.filesStep2.splice(0, 1);
      }
    } else if (stepper == 1) {
      (<HTMLInputElement>document.getElementById("firstNext")).disabled = true;
      this.filesStep1.splice(index, 1);
      const box = document.getElementById("dropzone1");
      box.classList.remove("fileover");

      // resetForm1
      this.firstStepValid =false;
    } else if (stepper == 2) {
      (<HTMLInputElement>document.getElementById("secondNext")).disabled = true;
      this.filesStep2.splice(index, 1);
      const box = document.getElementById("dropzone2");
      box.classList.remove("fileover");

      // resetForm2
      this.secondStepValid =false;

    }
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  createItem(addition = false): FormGroup {
    console.log(addition);
    return this._formBuilder.group({
      parameterId: "",
      minValue: "",
      maxValue: "",
      added: addition,
    });
  }

  ///////////////////
  // step 2 function
  ///////////////////

  onUploadedVnf(event: any, Vnfs: File[]) {
    let promises = [];

    for (let Vnf of Vnfs) {
      console.log(Vnf.type);
      // if(Vnf.type=='application/json' && Vnf.name.includes('json')){
      // if(AcceptedTypes.some(x=> x == Vnf.type) && namesToInclude.some(x=> x == Vnf.name) && Vnf.size <= MaxFileSize){
      if (
        (AcceptedTypes.indexOf(Vnf.type) &&
        Vnf.name.includes(namesToInclude[files.GZIP]) &&
        Vnf.size <= MaxFileSize)
        ||
        (AcceptedTypes.indexOf(Vnf.type) &&
          Vnf.name.includes(namesToInclude[files.ZIP]) &&
          Vnf.size <= MaxFileSize)
          ||
          (AcceptedTypes.indexOf(Vnf.type) &&
            Vnf.name.includes(namesToInclude[files.ZIP2]) &&
            Vnf.size <= MaxFileSize)
      ) {

        //add file to view
        this.prepareFilesList(Vnfs, 2);

        this.bluePrintFileFormData.append("vnfPackage", Vnf);
        this.secondStepValid =true;

        // let VnfPromise = new Promise((resolve) => {
        //   let reader = new FileReader();
        //   reader.readAsText(Vnf);
        //   reader.onload = () => resolve(reader.result);
        // });
        // promises.push(VnfPromise);
      } else {
      this.secondStepValid =false;
        this.authService.log("the file is not ZIP", "FAILED", false);
        (<HTMLInputElement>document.getElementById("secondNext")).disabled =
          true;
      }
    }
  }

  ///////////////////
  // submit 3 function
  ///////////////////

  triggerParentUpdate() {
    this.netAppEmitter.emit(true);
  }

  submitOnBoard() {
    console.log(this.bluePrintFileFormData);

    this.blueprintsNaService
      .postNaBlueprint(this.bluePrintFileFormData)
      .subscribe(
        (resp) => {
          console.log(this.bluePrintFileFormData);
          this.triggerParentUpdate();
          this.deleteFile(null, 1, true);
          this.deleteFile(null, 2, true);
          this.stepper.reset();
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
