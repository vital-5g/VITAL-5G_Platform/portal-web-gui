import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueprintsNetappStepperComponent } from './blueprints-netapp-stepper.component';

describe('BlueprintsNetappStepperComponent', () => {
  let component: BlueprintsNetappStepperComponent;
  let fixture: ComponentFixture<BlueprintsNetappStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueprintsNetappStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueprintsNetappStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
