
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { BlueprintsVsService } from '../../blueprints-vs.service';
import { BlueprintsVsComponent} from '../blueprints-vs/blueprints-vs.component';
import { NsdsService} from '../../nsds.service';
import { AuthService} from '../../auth.service';
import { EncService } from '../../enc.service';
import { AcceptedTypes, files, MaxFileSize, namesToInclude } from 'src/app/Validation/blueprintUploadValidator';
import { BlueprintsNaService } from 'src/app/NetAppsServices/blueprints-na.service';
import { NaBlueprintInfo } from '../blueprints-net-apps/na-blueprint-info';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: "app-blueprints-vs-stepper",
  templateUrl: "./blueprints-vs-stepper.component.html",
  styleUrls: ["./blueprints-vs-stepper.component.css"],
})
export class BlueprintsVsStepperComponent implements OnInit {
  filesStep1: any[] = [];
  filesStep2: any[] = [];
  @ViewChild("blueprintFile", { static: false }) fileDropElStep1: ElementRef;
  @ViewChild("nsdFiles", { static: false }) fileDropElStep2: ElementRef;

  nsdObj: Object;

  vsbObj: Object;
  validator: boolean;
  dfs: String[] = [];
  showSteps: boolean;
  naBlueprintInfos: NaBlueprintInfo[] = [];
  translationRules = {
    input: [
      // {
      //     "parameterId": "iot_sensors",
      //     "minValue": 0,
      //     "maxValue": 1000
      // }
    ],
    nsdId: "cirros_nsd",
    nsFlavourId: "testNsdFlavour",
    nsInstantiationLevelId: "testNsdIL",
  };

  instLevels: String[] = [];

  translationParams: String[] = [];

  isLinear = true;
  items: FormArray;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;

  @ViewChild('stepper',{read:MatStepper}) stepper:MatStepper;
  valid: boolean = false;
  
  constructor(
    @Inject(DOCUMENT) document,
    private _formBuilder: FormBuilder,
    private blueprintsVsService: BlueprintsVsService,
    private blueprintsVsComponent: BlueprintsVsComponent,
    private nsdsService: NsdsService,
    private authService: AuthService,
    private encService: EncService,
    private blueprintsNaService: BlueprintsNaService
  ) {}

  ngOnInit() {
    this.getNaBlueprints();
    this.showSteps = true;
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ["", Validators.required],
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ["", Validators.required],
      deploymentType: [""],
    });
    this.thirdFormGroup = this._formBuilder.group({
      nsdId: ["", Validators.required],
      nsdVersion: ["", Validators.required],
      nsFlavourId: ["", Validators.required],
      nsInstLevel: ["", Validators.required],
      items: this._formBuilder.array([]),
    });
  }

  getNaBlueprints(): void {
    this.blueprintsNaService.getNaBlueprints().subscribe(
      (naBlueprintInfos: NaBlueprintInfo[]) => {
        this.naBlueprintInfos = naBlueprintInfos;
      },
      (error) => {}
    );
  }

  currentNaId = "";
  onNaChange(value) {
    this.currentNaId = value.value;
  }

  createItem(): FormGroup {
    return this._formBuilder.group({
      parameterId: "",
      minValue: "",
      maxValue: "",
    });
  }

  onUploadedVsb(event: any, vsbs: File[]) {
    let promises = [];

    for (let vsb of vsbs) {
      if (
        vsb.type == AcceptedTypes[files.JSON] &&
        vsb.name.includes(namesToInclude[files.JSON]) &&
        vsb.size <= MaxFileSize
      ) {
        //add file to view
        this.prepareFilesList(vsbs, 1);
        let vsbPromise = new Promise((resolve) => {
          let reader = new FileReader();
          reader.readAsText(vsb);
          reader.onload = () => resolve(reader.result);
        });
        promises.push(vsbPromise);
      } else {
        this.authService.log("the file is not json", "FAILED", false);
        (<HTMLInputElement>document.getElementById("firstNext")).disabled =
          true;
      }
    }

    if (promises.length > 0) {
      Promise.all(promises).then((fileContents) => {
        this.vsbObj = JSON.parse(fileContents[0]);
        if (this.vsbObj["vsBlueprint"] !== undefined){
        if (this.vsbObj["vsBlueprint"]["serviceParameters"] !== undefined) {
          //empty the array before pushing the new data --> another file uploaded
          this.items = this.thirdFormGroup.get("items") as FormArray;
          while (this.items.length !== 0) {
            this.items.removeAt(0);
          }

          console.log(this.vsbObj)

          this.translationParams = [];
          for (const [key, value] of Object.entries(
            this.vsbObj["vsBlueprint"]["serviceParameters"]
          )) {
            this.items = this.thirdFormGroup.get("items") as FormArray;
            this.items.push(this.createItem());
            this.translationParams.push(key);
          }
        }
      }
        // if(this.vsbObj.hasOwnProperty('interSite')){
        //   (<HTMLInputElement> document.getElementById("firstNext")).style.display = 'none';
        //   (<HTMLInputElement> document.getElementById("submitButtonFirst")).style.display = 'inline';
        //   this.showSteps=false;
        //   }
        //   else{

        //     (<HTMLInputElement> document.getElementById("firstNext")).style.display = 'inline';
        //     (<HTMLInputElement> document.getElementById("submitButtonFirst")).style.display = 'none';
        //     this.showSteps=true;
        //     /*
        //     this.blueprintsVsService.validateVsBlueprint(this.vsbObj)
        //     .subscribe(res => {
        //       if(res===undefined){
        //         (<HTMLInputElement> document.getElementById("firstNext")).disabled = true;
        //       }else{
        //         (<HTMLInputElement> document.getElementById("firstNext")).disabled = false;

        //       }

        //     });
        //     */
        //   }

        //TODO: Commented by Amro(DONE)
        // this.encService.validateVsBlueprint(this.vsbObj)
        this.encService.validateVSBlueprintSyntax(this.vsbObj)
        .subscribe(resSyntax => {
          if(resSyntax===undefined){
            (<HTMLInputElement> document.getElementById("firstNext")).disabled = true;
          }else{
            this.valid = resSyntax["valid"];
          if(this.valid===true){
            (<HTMLInputElement> document.getElementById("firstNext")).disabled = false;
          }
          else{
            (<HTMLInputElement> document.getElementById("firstNext")).disabled = true;
            console.log(resSyntax);
            console.log(this.valid);
            debugger
            this.authService.log("the syntax is invalid", "FAILED", false);
          }

          }

        });
      });
    }
  }

  prepareFilesList(files: Array<any>, stepper) {
    if (stepper == 1) {
      for (const item of files) {
        item.progress = 0;
        this.filesStep1.push(item);
      }
      this.fileDropElStep1.nativeElement.value = "";
    } else if (stepper == 2) {
      for (const item of files) {
        item.progress = 0;
        this.filesStep2.push(item);
      }
      this.fileDropElStep2.nativeElement.value = "";
    }
  }

  deleteFile(index: number, stepper) {
    if (stepper == 1) {
      (<HTMLInputElement>document.getElementById("firstNext")).disabled = true;
      this.filesStep1.splice(index, 1);
      const box = document.getElementById("dropzone1");
      box.classList.remove("fileover");

      // resetForm1
      this.firstFormGroup = this._formBuilder.group({
        firstCtrl: ["", Validators.required],
      });
    } else if (stepper == 2) {
      (<HTMLInputElement>document.getElementById("secondNext")).disabled = true;
      this.filesStep2.splice(index, 1);
      const box = document.getElementById("dropzone2");
      box.classList.remove("fileover");

      // resetForm2
      this.secondFormGroup = this._formBuilder.group({
        secondCtrl: ["", Validators.required],
        deploymentType: [""],
      });
    }
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  ogFileUplaod = true;
  onUploadedNsd(event: any, nsds: File[]) {
    let promises = [];
    console.log(nsds.length);
    for (let nsd of nsds) {
      if (
        AcceptedTypes.indexOf(nsd.type) && nsd.name.includes(namesToInclude[files.YAML])  &&
        nsd.size <= MaxFileSize
      ) {
        //add file to view
        this.prepareFilesList(nsds, 2);

        let nsdPromise = new Promise((resolve) => {
          let reader = new FileReader();
          reader.readAsText(nsd);
          reader.onload = () => resolve(reader.result);
        });
        promises.push(nsdPromise);
      } else {
        this.authService.log("the file is not YAML", "FAILED", false);
        (<HTMLInputElement>document.getElementById("secondNext")).disabled =
          true;
      }
    }

    //this.prepareFilesList(nsds, 2);

    if (promises.length > 0) {
      Promise.all(promises).then((fileContents) => {
       
        //get the data from the yaml file
        if (this.ogFileUplaod) {
          let fileLines = fileContents[0].trim().split(/\r?\n/);
          let idIndex = fileLines.findIndex((x) => x.trim().startsWith("id:"));
          let nsdId = fileLines[idIndex].split("id:")[1].trim();
          let versionLine = fileLines.filter((x) =>
            x.trim().startsWith("version:")
          );
          let version = versionLine[0].split("version:")[1].trim();
          if(version.trim().startsWith("'")){
            version = version.substring(1);
          }
          if (version.trim().substring(version.trim().length-1) == "'")
          {
            version = version.trim().substring(0, version.trim().length - 1);
          }

          let dfIndex = fileLines.findIndex((x) => x.trim().startsWith("df:"));
          if (dfIndex == -1 && !fileLines[dfIndex + 1].includes("- id:")) {
            return;
          }
          let nsdFalvour = fileLines[dfIndex + 1].split("id:")[1].trim();

          this.thirdFormGroup.get("nsdId").setValue(nsdId);
          this.thirdFormGroup.get("nsdVersion").setValue(version);
          this.thirdFormGroup.get("nsFlavourId").setValue(nsdFalvour);
          this.thirdFormGroup.get("nsInstLevel").setValue("testNsdIL");
        } else {
          this.nsdObj = JSON.parse(fileContents[0]);
          //console.log(JSON.stringify(this.nsdObj, null, 4));

          this.thirdFormGroup
            .get("nsdId")
            .setValue(this.nsdObj["nsdIdentifier"]);
          this.thirdFormGroup
            .get("nsdVersion")
            .setValue(this.nsdObj["version"]);

          this.dfs = this.nsdObj["nsDf"];
          //console.log("nsdid: " + this.thirdFormGroup.get('nsdId'));

          // //TODO: Commented by Amro
          // this.encService.validateNsDescriptor(this.nsdObj)
          // .subscribe(res => {
          //   if(res===undefined){
          //     (<HTMLInputElement> document.getElementById("secondNext")).disabled = true;
          //   }else{
          //     (<HTMLInputElement> document.getElementById("secondNext")).disabled = false;

          //   }
          // });

          //this.fourthFormGroup.get('nsFlavourIdCtrl').setValue(nsdObj['nsDf'][0]['nsDfId']);
          //this.fourthFormGroup.get('nsInstLevelIdCtrl').setValue(nsdObj['nsDf'][0]['nsInstantiationLevel'][0]['nsLevelId']);
        }
      });
    }
  }

  showNextBtn = true;
  showSubmitBtn = false;
  hideNextShowSubmit($event: any) {
    // var submit = document.getElementById("submitButton");
    // var next = document.getElementById("nextButton");
    // var uploadFile = document.getElementById("uploadFile");
    if ($event.source.checked) {
      // submit.style.display = 'inline';
      // next.style.display = 'none';
      this.showSubmitBtn = true;
      this.showNextBtn = false;
      this.secondFormGroup.controls["secondCtrl"].disable();
    } else {
      // next.style.display = 'inline';
      // submit.style.display = 'none';

      this.showSubmitBtn = false;
      this.showNextBtn = true;
      this.secondFormGroup.controls["secondCtrl"].enable();
    }
  }

  onNsDfSelected(event: any) {
    var selectedDf = event.value;

    for (var i = 0; i < this.nsdObj["nsDf"].length; i++) {
      if (this.nsdObj["nsDf"][i]["nsDfId"] == selectedDf) {
        this.instLevels = this.nsdObj["nsDf"][i]["nsInstantiationLevel"];
      }
    }
  }

  createOnBoardingRequestWithoutNsds(blueprints: File[]) {
    if (blueprints.length > 0) {
      var blueprint = blueprints[0];
      var onBoardVsRequest = JSON.parse("{}");
      let promises = [];
      let blueprintPromise = new Promise((resolve) => {
        let reader = new FileReader();
        reader.readAsText(blueprint);
        reader.onload = () => resolve(reader.result);
      });
      promises.push(blueprintPromise);

      Promise.all(promises).then((fileContents) => {
        onBoardVsRequest["vsBlueprint"] = JSON.parse(fileContents[0]);
        for (var i = 1; i < fileContents.length; i++) {
          onBoardVsRequest["nsds"].push(JSON.parse(fileContents[i]));
        }

        //var blueprintId = onBoardVsRequest.vsBlueprint.blueprintId;

        this.blueprintsVsService
          .postVsBlueprint(onBoardVsRequest)
          .subscribe((vsBlueprintId) => {
            //console.log("VS Blueprint with id " + vsBlueprintId);
            this.blueprintsVsComponent.selectedIndex = 0;
            this.blueprintsVsComponent.getVsBlueprints();
          });
      });
    }
  }

  createOnBoardVsBlueprintRequest(blueprints: File[], nsds: File[]) {
    //TODO : remove || true (DONE)
    // if (!this.thirdFormGroup.invalid || true) {
      if (!this.thirdFormGroup.invalid) {
      var onBoardVsRequest = JSON.parse("{}");
      // onBoardVsRequest['nsds'] = [];
      onBoardVsRequest["translationRules"] = [];

      // if (blueprints.length > 0) {
      if (this.filesStep1.length > 0) {
        var blueprint = this.filesStep1[0];

        let promises = [];
        let blueprintPromise = new Promise((resolve) => {
          let reader = new FileReader();
          reader.readAsText(blueprint);
          reader.onload = () => resolve(reader.result);
        });
        promises.push(blueprintPromise);

        // for (let nsd of nsds) {
        for (let nsd of this.filesStep2) {
          let nsdPromise = new Promise((resolve) => {
            let reader = new FileReader();
            reader.readAsText(nsd);
            reader.onload = () => resolve(reader.result);
          });
          promises.push(nsdPromise);
        }

        Promise.all(promises).then((fileContents) => {
          //TODO:uncomment below lines, if required later
          if (this.ogFileUplaod) {
            onBoardVsRequest["vsBlueprint"] = JSON.parse(fileContents[0])['vsBlueprint'];
            // for (var i = 1; i < fileContents.length; i++) {
            //   onBoardVsRequest['nsds'].push(JSON.parse(fileContents[i]));
            // }
          }

          if (
            this.translationParams !== undefined &&
            this.translationParams.length !== 0
          ) {
            //console.log(this.translationParams);

            var translationRule = JSON.parse("{}");
            //var blueprintId = onBoardVsRequest.vsBlueprint.blueprintId;
            var nsdId = this.thirdFormGroup.get("nsdId").value;
            var nsdVersion = this.thirdFormGroup.get("nsdVersion").value;
            var nsFlavourId = this.thirdFormGroup.get("nsFlavourId").value;
            var nsInstLevel = this.thirdFormGroup.get("nsInstLevel").value;

            translationRule["nsdId"] = nsdId;
            translationRule["nsdVersion"] = nsdVersion;
            translationRule["nsFlavourId"] = nsFlavourId;
            translationRule["nsInstantiationLevelId"] = nsInstLevel;

            //translationRule['blueprintId'] = blueprintId;
            var paramsRows = this.thirdFormGroup.controls.items as FormArray;
            var controls = paramsRows.controls;
            var paramsObj = [];

            for (var j = 0; j < controls.length; j++) {
              paramsObj.push(controls[j].value);
            }

            console.log(paramsObj);
            // onBoardVsRequest.vsBlueprint.verticalServiceBlueprintId = onBoardVsRequest.vsBlueprint.blueprintId;

            // var blueprintId = onBoardVsRequest.vsBlueprint.blueprintId;
            translationRule["input"] = paramsObj;
            onBoardVsRequest.translationRules.push(translationRule);
          }
          //console.log('onBoardVsRequest: ' + JSON.stringify(onBoardVsRequest, null, 4));
          // onBoardVsRequest.nsdId = this.thirdFormGroup.controls['nsdId'].value

          let bluePrintFileFormData = new FormData();

          bluePrintFileFormData.append("nsd", this.filesStep2[0]);
          if (this.ogFileUplaod) {
            // onBoardVsRequest.vsBlueprint.atomicComponents[0].netAppBlueprintId =
            //   this.currentNaId;
            console.log(onBoardVsRequest);
            bluePrintFileFormData.append(
              "vsb",
              JSON.stringify(onBoardVsRequest)
            );
          } else {
            let obj: any = this.vsbObj;
            console.log(obj);
            // obj.vsBlueprint.atomicComponents[0].netAppBlueprintId =
            //   this.currentNaId;
            bluePrintFileFormData.append("vsb", JSON.stringify(obj));
          }
          console.log(bluePrintFileFormData)
          // this.blueprintsVsService.postVsBlueprint(onBoardVsRequest)
          this.blueprintsVsService
            .postVsBlueprint(bluePrintFileFormData)
            .subscribe(
              (vsBlueprintId) => {
                //console.log("VS Blueprint with id " + vsBlueprintId);
                this.clearForms();
                this.blueprintsVsComponent.selectedIndex = 0;
                this.blueprintsVsComponent.getVsBlueprints();

              },
              (error) => {
                console.log(error);
              }
            );
        });
      }
    }
  }

  clearForms(){
    this.deleteFile(0, 1);
    this.deleteFile(0, 2);
    this.thirdFormGroup.reset();
    this.stepper.reset();
  }
}
