import { AfterViewInit, Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { BlueprintsTcService } from 'src/app/blueprints-tc.service';
import { TcBlueprintInfo } from '../blueprints-tc/tc-blueprint-info';
import { BlueprintsTCDetailsItemKV, BlueprintsTcDetailsDatasource } from './blueprints-tc-details-datasource.ts';

@Component({
  selector: 'app-blueprints-tc-details',
  templateUrl: './blueprints-tc-details.component.html',
  styleUrls: ['./blueprints-tc-details.component.css']
})
export class BlueprintsTcDetailsComponent implements OnInit, AfterViewInit {
  
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) 
  table: MatTable<BlueprintsTCDetailsItemKV>;
  dataSource: BlueprintsTcDetailsDatasource;

  tableData: BlueprintsTCDetailsItemKV[] = [];
  // test case info
  testcase: any;

  cps: BlueprintsTCDetailsItemKV[] = [];

  // columns to be displayed
  displayedColumns = ['key', 'value'];
  testcasecolumns = ['id', 'name', 'version','executionAction','configurationAction', 'resetAction']//','description', 'script','userParameters', 'infrastructureParameters'];


  constructor(private blueprintsTcService: BlueprintsTcService) { }
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    var tcbId = localStorage.getItem('tcbId');
    this.dataSource = new BlueprintsTcDetailsDatasource(this.tableData);
    this.getTcBlueprint(tcbId);
  }
  getTcBlueprint(tcbId: string) {
    this.blueprintsTcService.getTcBlueprint(tcbId).subscribe((tcBlueprintInfo: TcBlueprintInfo) =>
    {
      var tcBlueprint = tcBlueprintInfo;//['testCaseBlueprint'];
      // console.log(tcBlueprintInfo)
      this.tableData.push({key: "Name", value: [tcBlueprint['name']]});
      this.tableData.push({key: "Id", value: [tcBlueprint['testCaseBlueprintId']]});
      this.tableData.push({key: "Version", value: [tcBlueprint['version']]});
      // this.tableData.push({key: "description", value: [tcBlueprint['description']]});
      // this.tableData.push({key: "script", value: [tcBlueprint['script']]});
      // var values = [];
      // if(tcBlueprint['userParameters'] !== null){
      //   for (var i =0; i < tcBlueprint['userParameters'].keys.length; i++){
      //     values.push(tcBlueprint['userParameters'][i].value);
      //   }
      //   this.tableData.push({key:'User parameter', value: values});
      // }
      // values = [];
      // if(tcBlueprint['infrastructureParameters'] !== null){
      //   for (var i =0; i < tcBlueprint['infrastructureParameters'].keys.length; i++){
      //     values.push(tcBlueprint['infrastructureParameters'][i].value);
      //   }
      //   this.tableData.push({key:'Infrastructure parameter', value: values});
      // }
      var values = [];
      if (tcBlueprint['executionAction']){
        var executionAction = tcBlueprint['executionAction'];
        var id = executionAction['nfvoActionId'];
        var ref = executionAction['nfvoReferenceId'];
        var input = '';
        for(var i = 0; i < executionAction.inputParameters.length; i++){
          input += (executionAction.inputParameters[i] + ',');
        }
        input = input.substring(0, input.length - 1);
        input == ''?values.push(  `${id},${ref}`  ):values.push(  `${id},${ref} ( ${input} )`  );
      

        this.tableData.push({key:'Execution Action', value:values});
      }

      values = [];
      if (tcBlueprint['configurationAction']){
        var confAction = tcBlueprintInfo['configurationAction'];
        var id = confAction.nfvoActionId;
        var ref = confAction.nfvoReferenceId;
        var input = '';
        for(var i = 0; i < confAction.inputParameters.length; i++){
          input += (confAction.inputParameters[i] + ',');
        }
        input = input.substring(0, input.length - 1);
        input == ''?values.push( `${id},${ref}` ):values.push(  `${id},${ref}  ( ${input} )` );

        this.tableData.push({key:'Configuration Action', value:values});
      }

      values = [];
      if (tcBlueprint['resetAction']){
        var resetAction = tcBlueprintInfo['resetAction'];
        var id = resetAction.nfvoActionId;
        var ref = resetAction.nfvoReferenceId;
        var input = '';
        for(var i = 0; i < resetAction.inputParameters.length; i++){
          input += (resetAction.inputParameters[i] + ',');
        }
        input = input.substring(0, input.length - 1);
        input == ''?values.push( `${id},${ref}` ):values.push(  `${id},${ref}  ( ${input} )` );

        this.tableData.push({key:'Reset Action', value:values});
      }
     this.dataSource = new BlueprintsTcDetailsDatasource(this.tableData);
     this.dataSource.sort = this.sort;
     this.table.dataSource = this.dataSource;
     });
  }

}
