import { NaBlueprint } from './na-blueprint';

export class NaBlueprintInfo {
    netAppPackageId: string;
    version: string;
    name: string;
    onBoardedNsdInfoId: string[];
    onBoardedVnfPackageInfoId: string[];
    onBoardedMecAppPackageInfoId: string[];
    activeVsdId: string[];
    vsBlueprint: NaBlueprint;
}
