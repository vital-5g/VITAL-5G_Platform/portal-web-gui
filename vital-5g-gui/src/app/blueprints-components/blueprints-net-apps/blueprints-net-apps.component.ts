import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { BlueprintsNaService } from 'src/app/NetAppsServices/blueprints-na.service';
import { DescriptorsNaService } from 'src/app/NetAppsServices/descriptors-na.service';
import { SharedService } from 'src/app/shared.service';


import { VsbDetailsService } from 'src/app/vsb-details.service';
import { BlueprintsNaDataSource } from './blueprints-na-datasource';
import { NaBlueprintDoc } from './na-blueprint-doc';
import { NaBlueprintInfo } from './na-blueprint-info';
import { BlueprintsVsService } from 'src/app/blueprints-vs.service';

@Component({
  templateUrl: './blueprints-net-apps.component.html',
  styleUrls: ['./blueprints-net-apps.component.css']
})
export class BlueprintsNetAppsComponent implements OnInit,AfterViewInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false})
   table: MatTable<NaBlueprintInfo>;
  
  
  tableData: any = [];
  dataSource = new MatTableDataSource(this.tableData);
  //dataSource: BlueprintsNaDataSource;
  naBlueprintInfos: NaBlueprintInfo[] = [];
  idToVsdIds: Map<string, Map<string, string>> = new Map();
  naBlueprintDocs: NaBlueprintDoc[] = [];

  selectedIndex = 0;
  displayedColumns = ['id', 'name','testbed',  'version',/*'vsBlueprintId',/*'software_docs', /*'description', 'conf_params',*/ /*'buttons'*/'view','delete','forcedelete'];
  nabDocsId: string[] = [];
  vsbId: string = '-1';
  idToVsbId: Map<string, Map<string, string>> = new Map();
  
  selectedState :any;

  selectedSite:any;
    states: string[] = [
    "ATHENS",
    "DANUBE",
    "ANTWERP",
  ]

  constructor(private blueprintsNaService: BlueprintsNaService,
    private vsbDetailsService: VsbDetailsService,
    private descriptorsNaService: DescriptorsNaService,
    private router: Router,
    private sharedService:SharedService,
    private blueprintsVsService: BlueprintsVsService
    ) { }
  
    
    ngOnInit() {
      // this.dataSource = new BlueprintsVsDataSource(this.naBlueprintInfos);
      //this.dataSource = new BlueprintsNaDataSource([]);
      this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
      this.getVsBlueprints();
      this.vsbId = localStorage.getItem("vsbId");
    }

    ngAfterViewInit(): void {
      // this.fakeRow();
    }
    
    getVsBlueprints(): void {
      this.blueprintsNaService.getNaBlueprints().subscribe((naBlueprintInfos: NaBlueprintInfo[]) =>
      {
          console.log(naBlueprintInfos);

          this.naBlueprintInfos = naBlueprintInfos;
  
          for (var i = 0; i < naBlueprintInfos.length; i++) {
            if(naBlueprintInfos[i]['vsBlueprintId'] !== null &&
            naBlueprintInfos[i]['vsBlueprintId'] !== undefined)
            {
              this.idToVsdIds.set(naBlueprintInfos[i]['vsBlueprintId'], new Map());
              for (var j = 0; j < naBlueprintInfos[i]['activeVsdId'].length; j++) {
                this.getVsBlueprint(naBlueprintInfos[i]['vsBlueprintId'], naBlueprintInfos[i]['activeVsdId'][j]);
              }
            }
          }
          //this.dataSource = new BlueprintsNaDataSource(this.naBlueprintInfos);
          // this.dataSource = new BlueprintsVsDataSource([]);
          this.tableData = naBlueprintInfos;
          this.dataSource = new MatTableDataSource(this.tableData)
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          //this.table.dataSource = this.dataSource;
        }, error =>{
          
        });
    }
    getVsBlueprint(vsdId: string, vsbId: string) {
      this.blueprintsVsService.getVsBlueprint(vsbId).subscribe(vsBlueprintInfo => {
        var names = this.idToVsbId.get(vsdId);
        names.set(vsbId, vsBlueprintInfo['name']);
      })
    }
  
    viewVsBlueprint(vsbId: string) {
      //console.log(vsbId);
      localStorage.setItem('vsbId', vsbId);
  
      this.router.navigate(["/blueprints_vs_details"]);
    }
    getVsDescriptor(vsbId: string, vsdId: string) {
      this.descriptorsNaService.getVsDescriptor(vsdId).subscribe(vsDescriptorInfo => {
        var names = this.idToVsdIds.get(vsbId);
        names.set(vsdId, vsDescriptorInfo['name']);
      })
    }


    // hidden removed from VS table ( mostly not needed )
    viewVsDescriptor(vsdId: string) {
      localStorage.setItem('vsdId', vsdId);
      this.router.navigate(["/descriptors_vs_details"]);
    }
  
    deleteVsBlueprint(vsBlueprintId: string) {
      this.sharedService.openConfirmDialog().afterClosed().subscribe(result => {
       if(result){
         return this.blueprintsNaService.deleteNaBlueprint(vsBlueprintId).subscribe(
           () => { this.getVsBlueprints(); }
         );
       }
      });
    }
    forcedeleteVsBlueprint(vsBlueprintId: string) {
      this.sharedService.openConfirmDialog().afterClosed().subscribe(result => {
        if(result){
          return this.blueprintsNaService.forcedeleteNaBlueprint(vsBlueprintId).subscribe(
            () => { this.getVsBlueprints(); }
          );
        }
       });
    }
  
    viewVsBlueprintGraph(vsBlueprintId: string) {
      //console.log(vsBlueprintId);
      this.vsbDetailsService.updateVSBId(vsBlueprintId);
      //routerLink="/blueprints_vs_graph"
      localStorage.setItem("nabId",vsBlueprintId);
      this.router.navigate(["/blueprints_na_details"]);
    }


    triggerUpdate(value){
      this.selectedIndex = 0;
      this.getVsBlueprints();
    }


    fakeRow(){
      //row.vsBlueprint.parameters"><td> - {{param.parameterName}}
      let obj = {
        'netAppPackageId':'1',
        'name':'test',
        'version':'1',
        'vsBlueprint':{
          'description':'description test',
          'parameters': [{'parameterName':'param1'},{'parameterName':'param2'}]
        },
        'buttons':'1'
      };
      let arr = [];
      arr.push(obj)
      arr.push(obj)
      arr.push(obj)
      arr.push(obj)
      arr.push(obj)
      
      //this.dataSource = new BlueprintsNaDataSource(arr);
      // this.dataSource = new BlueprintsNaDataSource([]);

      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      //this.table.dataSource = this.dataSource;
    }
    getNaBlueprintsDocs(vsBlueprintId: string){
      this.blueprintsNaService.getNaBlueprintDocs(vsBlueprintId).subscribe(naBlueprintDocs => {
        if(naBlueprintDocs !== undefined){
        if(naBlueprintDocs.softwareDocId.length > 0){
          for( var i = 0; i < naBlueprintDocs.softwareDocId.length; i++){
            this.nabDocsId.push(naBlueprintDocs[i].softwareDocId);
          }
        }}
        if(this.nabDocsId.length >= 0){
          for (var i = 0; i <= this.nabDocsId.length; i++){
            console.log('here')
            this.blueprintsNaService.getNaBlueprintDoc(vsBlueprintId, this.nabDocsId[i]).subscribe(data => this.getPDFFile(data),
              error => console.log("Error downloading the file.")),
              () => console.log('Completed file download.');
          }
        }
      })
    }
  
    getPDFFile(data: any){
          var a: any = document.createElement("a");
          document.body.appendChild(a);
  
          a.style = "display: none";
          var blob = new Blob([data], { type: 'application/pdf' });
  
          var url= window.URL.createObjectURL(blob);
  
          a.href = url;
          a.download = `netapp-${this.vsbId}.pdf`;
          a.click();
          window.URL.revokeObjectURL(url);
  
      }
      onStatusSelected(event: any) {
        var selectedState = event.value;
        this.dataSource.filter = selectedState.trim();
      }
      clearFilter() {
    this.dataSource.filter = '';
  }
}

