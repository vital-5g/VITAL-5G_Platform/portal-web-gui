import { Component, OnInit, ViewChild } from "@angular/core";
import { VsbDetailsService } from "../../vsb-details.service";
import { MatSort } from "@angular/material/sort";
import { MatTable } from "@angular/material/table";
import {
  BlueprintsVsDetailsItemKV,
  BlueprintsVsDetailsDataSource,
} from "./blueprints-vs-details.datasource";
import { VsBlueprintInfo } from "../blueprints-vs/vs-blueprint-info";
import { BlueprintsVsService } from "../../blueprints-vs.service";
import { CtxBlueprintInfo } from "../blueprints-ec/ctx-blueprint-info";
import { BlueprintsEcService } from "../../blueprints-ec.service";
import { DescriptorsVsService } from "../../descriptors-vs.service";
import { VsDescriptorInfo } from "../../descriptors-vs/vs-descriptor-info";
import { stringify } from "@angular/compiler/src/util";
import { Router } from "@angular/router";
import { BlueprintsNaService } from "src/app/NetAppsServices/blueprints-na.service";

@Component({
  selector: "app-blueprints-vs-details",
  templateUrl: "./blueprints-vs-details.component.html",
  styles: [
    `
      app-blueprints-graph {
        height: 70vh;
        float: left;
        width: 100%;
        position: relative;
      }
    `,
  ],
})
export class BlueprintsVsDetailsComponent implements OnInit {
  node_name: string;
  isComposite: boolean = false;

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false })
  table: MatTable<BlueprintsVsDetailsItemKV>;
  dataSource: BlueprintsVsDetailsDataSource;

  graphData = {
    nodes: [],
    edges: [],
  };

  tableData: BlueprintsVsDetailsItemKV[] = [];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ["key", "value"];
  netAppBlueprintId: string;
  idToVsbId: Map<string, string> = new Map();


  constructor(
    public vsbDetailservice: VsbDetailsService,
    private blueprintsVsService: BlueprintsVsService,
    private ctxBlueprintService: BlueprintsEcService,
    private vsDescriptorService: DescriptorsVsService,
    private router: Router,
    private blueprintNaService: BlueprintsNaService
  ) {}

  vsbId = "";
  ngOnInit() {
    this.vsbId = localStorage.getItem("vsbId");
    this.dataSource = new BlueprintsVsDetailsDataSource(
      this.vsbDetailservice._vsBlueprintDetailsItems
    );
    this.getVsBlueprint(this.vsbId);
  }

  nodeChange(event: any) {
    this.node_name = event;
  }

  getVsBlueprint(vsbId: string) {
    this.blueprintsVsService
      .getVsBlueprint(vsbId)
      .subscribe((vsBlueprintInfo: any) => {
        this.blueprintsVsService
          .getVsBlueprints()
          .subscribe((vsBlueprintsInfo: any[]) => {
            // var vsBlueprint = vsBlueprintInfo["vsBlueprint"];
            var vsBlueprint = vsBlueprintInfo;

            console.log(vsBlueprint);
            if (vsBlueprint["interSite"] !== undefined) {
              this.isComposite = vsBlueprint["interSite"];
              //console.log(vsBlueprint.interSite);
            }
            this.tableData.push({
              key: "Name",
              value: [vsBlueprint["name"]],
            });
            //this.tableData.push({key: "Id", value: [vsBlueprint['blueprintId']]});
            this.tableData.push({
              key: "Version",
              value: [vsBlueprint["version"]],
            });
            this.tableData.push({
              key: "Description",
              value: [vsBlueprint["description"]],
            });

            this.tableData.push({
              key: "Access Level",
              value: [vsBlueprint["accessLevel"]],
            });

            this.tableData.push({
              key: "Testbed",
              value: [vsBlueprint["testbed"]],
            });

            var values = [];
            if (vsBlueprint["useCase"].length > 0) {
              for (let i = 0; i < vsBlueprint["useCase"].length; i++) {
                values.push(vsBlueprint["useCase"][i]);
              }

              this.tableData.push({
                key: "Use case",
                value: values,
              });
            }
            values = [];

            var values = [];
            if (
              vsBlueprint["applicationMetrics"] &&
              vsBlueprint["applicationMetrics"].length > 0
            ) {
              for (
                let i = 0;
                i < vsBlueprint["applicationMetrics"].length;
                i++
              ) {
                values.push(vsBlueprint["applicationMetrics"][i]["name"]);
              }

              this.tableData.push({
                key: "Application Metrics",
                value: values,
              });
            }

            values = [];

            if (vsBlueprint["infrastructureMetrics"]) {
              var valuesTo = vsBlueprint["infrastructureMetrics"];
              for (var i = 0; i < valuesTo.length; i++) {
                let name = valuesTo[i].name;
                let ids = "";
                for (let j = 0; j < valuesTo[i].componentIds.length; j++) {
                  ids += valuesTo[i].componentIds[j] + ",";
                }
                ids = ids.substring(0, ids.length - 1);

                ids == ""
                  ? values.push(`${name}`)
                  : values.push(`${name} ( ${ids} )`);
              }
              this.tableData.push({
                key: "Infrastructure Metrics",
                value: values,
              });
            }

            // if(vsBlueprint['infrastructureMetrics'] && vsBlueprint['infrastructureMetrics'].length > 0){
            //   for(let i =0; i<vsBlueprint['infrastructureMetrics'].length;i++){
            //     values.push(vsBlueprint['infrastructureMetrics'][i]['name'])
            //   }

            //   this.tableData.push({
            //     key: "Infrastructure Metrics",
            //     value: values,
            //   });
            // }

            var values = [];
            if (vsBlueprint["serviceParameters"] !== undefined) {
              for (const [key, value] of Object.entries(
                vsBlueprint["serviceParameters"]
              )) {
                values.push(key);
              }
              this.tableData.push({ key: "Parameters", value: values });
            }
            values = [];

            var atomicComponents = vsBlueprint["atomicComponents"];
            for (var i = 0; i < atomicComponents.length; i++) {
              if (
                atomicComponents[i]["placement"] !== null &&
                atomicComponents[i]["placement"] !== undefined &&
                atomicComponents[i]["placement"] !== ""
              ) {
                this.netAppBlueprintId = atomicComponents[i]["netAppBlueprintId"];
                values.push(
                  atomicComponents[i]["componentId"] +
                    " (" +
                    atomicComponents[i]["placement"].toLowerCase() +
                    ")"
                );
              } else {
                // values.push(atomicComponents[i]["componentId"]);
                this.idToVsbId.set(atomicComponents[i]["serviceComponentId"], atomicComponents[i]["netAppBlueprintId"]); 
                //this.netAppBlueprintId = atomicComponents[i]["serviceComponentId"];
                this.getNaBlueprint(atomicComponents[i]["serviceComponentId"], atomicComponents[i]["netAppBlueprintId"]);
                //debugger
                
                values = [];
                values.push(atomicComponents[i]["serviceComponentId"]);
                this.tableData.push({ key: "Components " + i+1, value: values });
              }
            }
            //this.tableData.push({ key: "Components", value: values });

            values = [];
            if (vsBlueprint["endPoints"]) {
              var endPoints = vsBlueprint["endPoints"];
              for (var i = 0; i < endPoints.length; i++) {
                values.push(endPoints[i]["endPointId"]);
              }
              this.tableData.push({ key: "Endpoints", value: values });
            }

            // values = [];
            // if (vsBlueprint["compatibleContextBlueprint"] !== undefined) {
            //   for (
            //     var i = 0;
            //     i < vsBlueprint["compatibleContextBlueprint"].length;
            //     i++
            //   ) {
            //     for (var j = 0; j < ctxBlueprintInfos.length; j++) {
            //       if (
            //         vsBlueprint["compatibleContextBlueprint"][i] ===
            //         ctxBlueprintInfos[j]["ctxBlueprintId"]
            //       ) {
            //         values.push(ctxBlueprintInfos[j]["name"]);
            //       }
            //     }
            //   }

            //   this.tableData.push({ key: "Context Blueprints", value: values });
            // }

            values = [];

            // if (vsBlueprint["testbed"]) {
            //   values.push(vsBlueprint["testbed"]);
            //   this.tableData.push({
            //     key: "Compatible Sites",
            //     value: values,
            //   });
            // }

            //console.log(this.tableData);
            this.vsbDetailservice.updateVSBTable(this.tableData);
            this.dataSource = new BlueprintsVsDetailsDataSource(this.tableData);
            this.dataSource.sort = this.sort;
            this.table.dataSource = this.dataSource;
            /*
        values = [];

        for (var i = 0; i < vsBlueprintInfo['activeVsdId'].length; i++) {
          for(var j = 0; j < vsDescriptors.length; j++) {
            if (vsBlueprintInfo['activeVsdId'][i] === vsDescriptors[j]['vsDescriptorId']){
              values.push(vsDescriptors[j]['name']);
            }
          }
        }
        this.tableData.push({key: "Active Vsds", value: values});
*/
            var atomicComponentsCps = [];

            if (this.isComposite == false) {
              for (var i = 0; i < atomicComponents.length; i++) {
                this.graphData.nodes.push({
                  data: {
                    //TODO
                    // id: atomicComponents[i]["componentId"],
                    // name: atomicComponents[i]["componentId"],
                    // i changed
                    id: atomicComponents[i]["serviceComponentId"],
                    name: atomicComponents[i]["serviceComponentId"],
                    weight: 70,
                    colorCode: "white",
                    shapeType: "ellipse",
                  },
                  classes: "bottom-center vnf",
                });

                atomicComponentsCps.push(
                  // ...atomicComponents[i]["endPointsIds"]
                  ...atomicComponents[i]["serviceEndpointIds"]
                );
                //console.log(atomicComponentsCps);
              }

              var sapCps = [];

              if (endPoints)
                for (var i = 0; i < endPoints.length; i++) {
                  if (
                    !atomicComponentsCps.includes(endPoints[i]["endPointId"])
                  ) {
                    sapCps.push(endPoints[i]["endPointId"]);
                    this.graphData.nodes.push({
                      data: {
                        id: endPoints[i]["endPointId"],
                        name: endPoints[i]["endPointId"],
                        weight: 50,
                        colorCode: "white",
                        shapeType: "ellipse",
                      },
                      classes: "bottom-center sap",
                    });
                  }
                }

              var connectivityServices = vsBlueprint["connectivityServices"];

              if (connectivityServices)
                for (var i = 0; i < connectivityServices.length; i++) {
                  this.graphData.nodes.push({
                    data: {
                      id: "conn_service_" + i,
                      name: connectivityServices[i]["name"],
                      weight: 50,
                      colorCode: "white",
                      shapeType: "ellipse",
                    },
                    classes: "bottom-center net",
                  });

                  if (false)
                    for (var j = 0; j < atomicComponents.length; j++) {
                      for (
                        var h = 0;
                        h < atomicComponents[j]["endPointsIds"].length;
                        h++
                      ) {
                        if (
                          connectivityServices[i]["endPointIds"].includes(
                            atomicComponents[j]["endPointsIds"][h]
                          )
                        ) {
                          this.graphData.edges.push({
                            data: {
                              source: atomicComponents[j]["componentId"],
                              target: "conn_service_" + i,
                              colorCode: "black",
                              strength: 70,
                            },
                          });
                        }
                      }
                    }

                  for (var j = 0; j < sapCps.length; j++) {
                    if (
                      connectivityServices[i]["endPointIds"].includes(sapCps[j])
                    ) {
                      this.graphData.edges.push({
                        data: {
                          source: sapCps[j],
                          target: "conn_service_" + i,
                          colorCode: "grey",
                          strength: 70,
                        },
                      });
                    }
                  }
                }
            } else {
              //console.log(JSON.stringify(vsBlueprint));
              for (let a = 0; a < vsBlueprintsInfo.length; a++) {
                var element = vsBlueprintsInfo[a]["vsBlueprint"];
                for (let b = 0; b < vsBlueprint.atomicComponents.length; b++) {
                  if (
                    element.blueprintId ===
                    vsBlueprint.atomicComponents[b]["associatedVsbId"]
                  ) {
                    for (var i = 0; i < element.atomicComponents.length; i++) {
                      this.graphData.nodes.push({
                        data: {
                          id:
                            element.atomicComponents[i]["componentId"] +
                            "_" +
                            vsBlueprint.atomicComponents[b]["compatibleSite"],
                          name: element.atomicComponents[i]["componentId"],
                          weight: 70,
                          colorCode: "white",
                          shapeType: "ellipse",
                        },
                        classes: "bottom-center vnf",
                      });
                      for (
                        let c = 0;
                        c < element.atomicComponents[i]["endPointsIds"].length;
                        c++
                      ) {
                        atomicComponentsCps.push(
                          element.atomicComponents[i]["endPointsIds"][c] +
                            "_" +
                            vsBlueprint.atomicComponents[b]["compatibleSite"]
                        );
                      }
                      //atomicComponentsCps.push(...element.atomicComponents[i]['endPointsIds']+'_'+vsBlueprint.atomicComponents[b]['compatibleSite']);
                      //console.log(atomicComponentsCps);
                    }
                    var sapCps = [];
                    for (var i = 0; i < element.endPoints.length; i++) {
                      if (
                        !atomicComponentsCps.includes(
                          element.endPoints[i]["endPointId"] +
                            "_" +
                            vsBlueprint.atomicComponents[b]["compatibleSite"]
                        )
                      ) {
                        sapCps.push(
                          element.endPoints[i]["endPointId"] +
                            "_" +
                            vsBlueprint.atomicComponents[b]["compatibleSite"]
                        );
                        this.graphData.nodes.push({
                          data: {
                            id:
                              element.endPoints[i]["endPointId"] +
                              "_" +
                              vsBlueprint.atomicComponents[b]["compatibleSite"],
                            name: element.endPoints[i]["endPointId"],
                            weight: 50,
                            colorCode: "white",
                            shapeType: "ellipse",
                          },
                          classes: "bottom-center sap",
                        });
                      }
                    }
                    //                  console.log(JSON.stringify(this.graphData));
                    var connectivityServices = element["connectivityServices"];
                    //console.log(JSON.stringify(connectivityServices));
                    for (var i = 0; i < connectivityServices.length; i++) {
                      this.graphData.nodes.push({
                        data: {
                          id:
                            "conn_service_" +
                            i +
                            "_" +
                            vsBlueprint.atomicComponents[b]["compatibleSite"],
                          name: connectivityServices[i]["name"],
                          weight: 50,
                          colorCode: "white",
                          shapeType: "ellipse",
                        },
                        classes: "bottom-center net",
                      });

                      for (
                        var j = 0;
                        j < element.atomicComponents.length;
                        j++
                      ) {
                        for (
                          var h = 0;
                          h <
                          element.atomicComponents[j]["endPointsIds"].length;
                          h++
                        ) {
                          if (
                            connectivityServices[i]["endPointIds"].includes(
                              element.atomicComponents[j]["endPointsIds"][h]
                            )
                          ) {
                            this.graphData.edges.push({
                              data: {
                                source:
                                  element.atomicComponents[j]["componentId"] +
                                  "_" +
                                  vsBlueprint.atomicComponents[b][
                                    "compatibleSite"
                                  ],
                                target:
                                  "conn_service_" +
                                  i +
                                  "_" +
                                  vsBlueprint.atomicComponents[b][
                                    "compatibleSite"
                                  ],
                                colorCode: "black",
                                strength: 70,
                              },
                            });
                          }
                        }
                      }

                      for (var j = 0; j < sapCps.length; j++) {
                        for (
                          var k = 0;
                          k < connectivityServices[i]["endPointIds"].length;
                          k++
                        ) {
                          if (
                            connectivityServices[i]["endPointIds"][k] +
                              "_" +
                              vsBlueprint.atomicComponents[b][
                                "compatibleSite"
                              ] ===
                            sapCps[j]
                          ) {
                            this.graphData.edges.push({
                              data: {
                                source: sapCps[j],
                                target:
                                  "conn_service_" +
                                  i +
                                  "_" +
                                  vsBlueprint.atomicComponents[b][
                                    "compatibleSite"
                                  ],
                                colorCode: "grey",
                                strength: 70,
                              },
                            });
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            this.vsbDetailservice.updateVSBGraph(this.graphData);
          });
      });
  }

  downloadVSB() {
    this.blueprintsVsService
      .getVsBlueprint(this.vsbId)
      .subscribe((VsBlueprintInfo: VsBlueprintInfo) => {
        var sJson = JSON.stringify(VsBlueprintInfo);
        var element = document.createElement("a");
        element.setAttribute(
          "href",
          "data:text/json;charset=UTF-8," + encodeURIComponent(sJson)
        );
        element.setAttribute("download", "VSB.json");
        element.style.display = "none";
        document.body.appendChild(element);
        element.click(); // simulate click
        document.body.removeChild(element);
      });
  }

  downloadVSBZIP() {
    this.blueprintsVsService
      .getVsBlueprint(this.vsbId)
      .subscribe((data) => this.getZipFile(data)),
      (error) => console.log("Error downloading the file."),
      () => console.log("Completed file download.");
  }

  getZipFile(data: any) {
    var a: any = document.createElement("a");
    document.body.appendChild(a);

    a.style = "display: none";
    var blob = new Blob([data], { type: "application/zip" });

    var url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = `netapp-${this.vsbId}.zip`;
    a.click();
    window.URL.revokeObjectURL(url);
  }
  getNaBlueprint(serviceComponentId: string, nabId: string) {
    this.blueprintNaService.getNaBlueprint(nabId).subscribe(naBlueprintInfo => {
      var names = this.idToVsbId.get(serviceComponentId);
      this.idToVsbId[serviceComponentId] = nabId
      //this.netAppBlueprintId = nabId;
      //names.set(nabId, naBlueprintInfo['name']);
    })
  }

  viewNaBlueprint(val: string) {
    console.log(this.idToVsbId.get(val.toString()));
    console.log(val.toString());
    debugger
    
    localStorage.setItem('nabId', this.idToVsbId.get(val.toString()));

    this.router.navigate(["/blueprints_na_details"]);
    
  }
}
