import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private router:Router, private authService:AuthService) {}

  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
 
    //check if user is authinticated
    let canRoute = (localStorage.getItem('token') && localStorage.getItem('logged') == 'true');
      if(!canRoute){
        //TODO: implement real logout after the login for users is implemnted
        // this.router.navigateByUrl('login')
        this.authService.fakeLogout();
      }
      else{
        return canRoute;
      }
  }
  
}
