import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewExperimentExecutionModalComponent } from './new-experiment-execution-modal.component';

describe('NewExperimentExecutionModalComponent', () => {
  let component: NewExperimentExecutionModalComponent;
  let fixture: ComponentFixture<NewExperimentExecutionModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewExperimentExecutionModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewExperimentExecutionModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
