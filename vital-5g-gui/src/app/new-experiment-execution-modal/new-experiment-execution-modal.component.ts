import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RunExperimentInfo } from '../run-experiments/run-experiment-info';

@Component({
  selector: 'app-new-experiment-execution-modal',
  templateUrl: './new-experiment-execution-modal.component.html',
  styleUrls: ['./new-experiment-execution-modal.component.css']
})
export class NewExperimentExecutionModalComponent implements OnInit {

  perfDiag = ['True','False'];
  constructor(
    private dialogRef: MatDialogRef<NewExperimentExecutionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    
  }

}
