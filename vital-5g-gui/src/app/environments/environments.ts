

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
​
let baseUrl = 'http://172.28.18.98:8082' //172.28.18.66'

export const environment = {
  production: false,
    portalBaseUrl: baseUrl + '/portal/catalogue/',
    lcmBaseUrl: baseUrl + '/portal.5g-eve.eu/portal/elm/',
    rbacBaseUrl: baseUrl + '/portal.5g-eve.eu/portal/rbac/',
    iwlBaseUrl: baseUrl + '/iwl/cat/',
    tsbBaseUrl: baseUrl + '/portal/tsb/',
    ibnBaseUrl: baseUrl + '/ibn/Intent/IntentPage.jsp',
    apiUrl: baseUrl + '/',
    dcsBaseUrl: baseUrl + '/portal/dcs/dashboard/',
    fsBaseUrl: baseUrl + '/portal/fs/',
    formulaCheckUrl: baseUrl + '/portal/formula/check/',
    supportBaseUrl: baseUrl + '/portal/enc/',
    iwfRepositoryUrl: baseUrl + '/portal/iwf/',
    backServerUrl: '',
    vsLcmBaseURL: 'http://172.28.18.98:8083/portal/vslcm/',
    eLcmBaseURL: 'http://172.28.18.98:8087/portal/elcm/',
    iframeURL: 'http://172.28.18.102:3000',
    IMSIsURL: 'http://172.28.18.98:8084/multisite-inventory/',

    nabvalidateUrl: 'http://172.28.18.103:8080/validator/api/blueprint/validate/',
    vsbvalidateUrl: 'http://172.28.18.103:8080/validator/api/blueprint/vsb/validate/',

    authURL : 'http://172.28.18.104:8080/auth/realms/vital5g/protocol/openid-connect/',
    client_id:'portal-gui',
    grant_type:'password',
    refresh_grant_type:'refresh_token',
    client_secret:'Byzu7CGRCObBOl9rLk5ixxjo0Vc9jDIT',
};
​
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
