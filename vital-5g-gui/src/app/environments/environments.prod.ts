/*
export const environment = {

    production: true,
    portalBaseUrl: 'http://10.50.80.18:8082/portal/catalogue/',
    lcmBaseUrl: 'http://10.50.80.18:8084/portal/elm/',
    rbacBaseUrl: 'http://10.50.80.13:8090/portal/rbac/',
    ibnBaseUrl: 'http://10.50.80.36:8080/Intent/IntentPage.jsp',
    apiUrl: 'http://10.50.80.18',
    backServerUrl: '',
    iwlBaseUrl: 'http://10.50.80.10:8083/',
    tsbBaseUrl: 'http://10.5.7.11:9090/portal/tsb/',

};
​*/

// export const environment = {
//     production: false,
//     portalBaseUrl: 'http://10.50.80.18:8082/portal/catalogue/',
//     lcmBaseUrl: 'http://10.50.80.18:8084/portal/elm/',
//     rbacBaseUrl: 'http://10.50.80.13:8090/portal/rbac/',
//     iwlBaseUrl: 'http://10.50.80.10:8083/',
//     tsbBaseUrl: 'http://10.5.7.11:9090/portal/tsb/',
//     ibnBaseUrl: 'http://10.50.80.36:8080/Intent/IntentPage.jsp',
//     apiUrl: 'http://10.50.80.18',
//     backServerUrl: ''
// };
​

export const environment = {
  production: true,
    portalBaseUrl: 'http://172.28.18.98:8082/portal/catalogue/',
    lcmBaseUrl: 'http://172.28.18.98:8082/portal.5g-eve.eu/portal/elm/',
    rbacBaseUrl: 'http://172.28.18.98:8082/portal.5g-eve.eu/portal/rbac/',
    iwlBaseUrl: 'http://172.28.18.98:8082/iwl/cat/',
    tsbBaseUrl: 'http://172.28.18.98:8082/portal/tsb/',
    ibnBaseUrl: 'http://172.28.18.98:8082/ibn/Intent/IntentPage.jsp',
    apiUrl: 'http://172.28.18.98:8082/',
    dcsBaseUrl: 'http://172.28.18.98:8082/portal/dcs/dashboard/',
    fsBaseUrl: 'http://172.28.18.98:8082/portal/fs/',
    formulaCheckUrl: 'http://172.28.18.98:8082/portal/formula/check/',
    supportBaseUrl: 'http://172.28.18.98:8082/portal/enc/',
    iwfRepositoryUrl: 'http://172.28.18.98:8082/portal/iwf/',
    backServerUrl: '',
    iframeURL: 'http://172.28.18.102:3000'

};