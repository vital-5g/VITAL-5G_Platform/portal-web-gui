export const environment = {
    production: false,
    portalBaseUrl: 'http://10.5.7.10:8082/portal/catalogue/',
    lcmBaseUrl: 'http://10.5.7.10:8084/portal/elm/',
    rbacBaseUrl: 'http://10.5.7.11:8090/portal/rbac/',
    iwlBaseUrl: 'http://10.3.3.30:8083/',
    tsbBaseUrl: 'http://10.5.7.11:9090/portal/tsb/',
    ibnBaseUrl: 'http://10.5.7.13:8080/Intent/IntentPage.jsp',
    apiUrl: 'http://10.5.7.10',
    dcsBaseUrl: 'http://10.5.7.12:8080/portal/dcs/dashboard/',
    fsBaseUrl: 'http://10.5.7.11:9191/portal/fs/',
    formulaCheckUrl: 'http://10.5.7.10:8989/check',
    backServerUrl: '',
    supportBaseUrl: 'http://10.5.7.10:8086/',
    iwfRepositoryUrl: 'http://10.3.3.30:8087/'
  };
