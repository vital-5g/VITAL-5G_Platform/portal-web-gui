import { TestBed } from '@angular/core/testing';

import { RunExperimentsService } from './run-experiments.service';

describe('RunExperimentsService', () => {
  let service: RunExperimentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RunExperimentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
