import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptorsVsStepperComponent } from './descriptors-vs-stepper.component';

describe('DescriptorsVsStepperComponent', () => {
  let component: DescriptorsVsStepperComponent;
  let fixture: ComponentFixture<DescriptorsVsStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriptorsVsStepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptorsVsStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
