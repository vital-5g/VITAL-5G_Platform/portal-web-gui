import { Component, OnInit, Inject, ViewChild, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { DOCUMENT } from '@angular/common';

import { NsdsService } from '../nsds.service';
import { AuthService } from '../auth.service';
import { EncService } from '../enc.service';

import { AcceptedTypes, files, MaxFileSize, namesToInclude } from 'src/app/Validation/blueprintUploadValidator';
import { BlueprintsNaService } from 'src/app/NetAppsServices/blueprints-na.service';
import { BlueprintsVsService } from '../blueprints-vs.service';
import { NaBlueprintInfo } from '../blueprints-components/blueprints-net-apps/na-blueprint-info';
import { VsBlueprintInfo } from '../blueprints-components/blueprints-vs/vs-blueprint-info';
import { DescriptorsVsService } from '../descriptors-vs.service';

@Component({
  selector: 'app-descriptors-vs-stepper',
  templateUrl: './descriptors-vs-stepper.component.html',
  styleUrls: ['./descriptors-vs-stepper.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DescriptorsVsStepperComponent implements OnInit {

  @Output() VsdAppEmitter = new EventEmitter<boolean>();
  
  nsdObj: Object;

  vsbObj: Object;
  validator:boolean;
  dfs: String[] = [];
  showSteps:boolean;
  naBlueprintInfos: NaBlueprintInfo[] = [];
  
  instLevels: String[] = [];

  pramNames: String[] = [];

  isLinear = true;
  items: FormArray;
  onboardFormGroup: FormGroup;

  vsbList =[];
  accessLevels =['RESTRICTED','PRIVATE','PUBLIC'];

  constructor(@Inject(DOCUMENT) document,
    private _formBuilder: FormBuilder,
    private blueprintsVsService: BlueprintsVsService,
    private descriptorsVsService: DescriptorsVsService, 
    private ref: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.getVsbIds();
    this.showSteps=true;
    this.onboardFormGroup = this._formBuilder.group({
      vsBlueprintId: ['', Validators.required],
      name: ['', Validators.required],
      version: ['', Validators.required],
      accessLevel: ['', Validators.required],
      items: this._formBuilder.array([])
    });

  }


  getVsbIds(): void {
    this.blueprintsVsService.getVsBlueprints().subscribe((vsbList: VsBlueprintInfo[]) =>
    {
      this.vsbList = vsbList;
    }, error =>{
        
    });
  }

  selectedVsb = '';
  onboardReady = false;
  vsbSelected(value){
    this.onboardReady = false;
    this.selectedVsb = value.value

    this.blueprintsVsService.getVsBlueprint(this.selectedVsb).subscribe( (blueprint:VsBlueprintInfo) =>{

      this.onboardFormGroup.patchValue({
        vsBlueprintId: this.selectedVsb, 
      });

      this.items = this.onboardFormGroup.get('items') as FormArray;
          while (this.items.length !== 0) {
            this.items.removeAt(0)
          }

          for (var prop in blueprint.serviceParameters) {
            this.items = this.onboardFormGroup.get('items') as FormArray;
            this.items.push(this.createItem(prop));
            this.pramNames.push(prop)
          }

      
    this.onboardReady = true;
    this.ref.detectChanges();

    document.getElementById('firstNext').click();

    },error =>{
      
    })

  }

  
  createItem(name): FormGroup {
    return this._formBuilder.group({
      // parameterId: '',
      name:name,
      minValue: '',
      // maxValue: ''
    });
  }

  submitOnboarding(){

    this.items = this.onboardFormGroup.get('items') as FormArray;
    console.log(this.items.value)

    let objToSend = this.onboardFormGroup.value;
    objToSend.qosParameters = {}

    objToSend.items.forEach(element => {
      objToSend.qosParameters[element.name] = element.minValue;
    });
    delete objToSend.items

    this.descriptorsVsService.addVsDescriptor(objToSend).subscribe( resp =>{
      this.VsdAppEmitter.emit(true);

    }, error => {

    })
  }



}
