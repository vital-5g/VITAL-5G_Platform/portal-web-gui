export class NaDescriptorInfo {
    vsDescriptorId: string;
    name: string;
    version: string;
    vsBlueprintId: string;
    sst: string;
    managementType: string;
    qosParameters: Map<string, string>;
    serviceConstraints: Object[];
    sla: Map<string, string>;
    sliceProfiles: Map<string, object> = new Map<string, object>();

}
