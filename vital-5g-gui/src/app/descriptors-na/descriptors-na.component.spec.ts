import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptorsNaComponent } from './descriptors-na.component';

describe('DescriptorsNaComponent', () => {
  let component: DescriptorsNaComponent;
  let fixture: ComponentFixture<DescriptorsNaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriptorsNaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptorsNaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
