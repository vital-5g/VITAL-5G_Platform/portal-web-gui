import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { NaDescriptorInfo } from './na-descriptor-info';
import { DescriptorsNaDataSource } from './descriptors-na-datasource';
import { Router } from '@angular/router';
import { BlueprintsNaService } from '../NetAppsServices/blueprints-na.service';
import { DescriptorsNaService } from '../NetAppsServices/descriptors-na.service';

@Component({
  selector: 'app-descriptors-na',
  templateUrl: './descriptors-na.component.html',
  styleUrls: ['./descriptors-na.component.scss']
})
export class DescriptorsNaComponent implements OnInit,AfterViewInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<NaDescriptorInfo>;
  dataSource: DescriptorsNaDataSource;
  tableData: NaDescriptorInfo[] = [];
  idToVsbId: Map<string, Map<string, string>> = new Map();

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'vsBlueprintId', 'sst', 'managementType', 'buttons'];

  constructor(private descriptorsNaService: DescriptorsNaService, 
    private blueprintsNaService: BlueprintsNaService,
    private router: Router) {}

  ngOnInit() {
    this.dataSource = new DescriptorsNaDataSource(this.tableData);
    // this.getVsDescriptors();
    // let elem1: HTMLElement = document.getElementById('show_blue');
    // elem1.setAttribute("style", "display:none;");
  }

  ngAfterViewInit(): void {
    this.fakeRow();
  }

  getVsDescriptors() {
    this.descriptorsNaService.getVsDescriptors().subscribe((vsDescriptorsInfos: NaDescriptorInfo[]) => 
      {
        //console.log(vsDescriptorsInfos);
        this.tableData = vsDescriptorsInfos;

        for (var i = 0; i < vsDescriptorsInfos.length; i ++) {
          this.idToVsbId.set(vsDescriptorsInfos[i]['vsDescriptorId'], new Map());
          this.getVsBlueprint(vsDescriptorsInfos[i]['vsDescriptorId'], vsDescriptorsInfos[i]['vsBlueprintId']);
        }        

        this.dataSource = new DescriptorsNaDataSource(this.tableData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
      });
  }

  getVsBlueprint(vsdId: string, vsbId: string) {
    this.blueprintsNaService.getVsBlueprint(vsbId).subscribe(vsBlueprintInfo => {
      var names = this.idToVsbId.get(vsdId);
      names.set(vsbId, vsBlueprintInfo['name']);
    })
  }

  viewVsBlueprint(vsbId: string) {
    //console.log(vsbId);
    localStorage.setItem('vsbId', vsbId);

    this.router.navigate(["/blueprints_na_details"]);
  }

  viewVsDescriptor(vsDescriptorId: string) {
    //console.log(vsDescriptorId);
    localStorage.setItem('vsdId', vsDescriptorId);

    this.router.navigate(["/descriptors_na_details"]);
  }



  fakeRow(){
    //row.vsBlueprint.parameters"><td> - {{param.parameterName}}
    let obj = {
      'vsDescriptorId':'1',
      'vsBlueprintId':'5',
      'name':'test',
      'sst':'test',
      'managementType':'test',

      
      'vsBlueprintVersion':'1',
      'vsBlueprint':{
        'description':'description test',
        'parameters': [{'parameterName':'param1'},{'parameterName':'param2'}]
      },
      'buttons':'1'
    };

    let vsDescriptorsInfos = [];
    vsDescriptorsInfos.push(obj);
    vsDescriptorsInfos.push(obj);
    vsDescriptorsInfos.push(obj);
    
        this.tableData = vsDescriptorsInfos;

        for (var i = 0; i < vsDescriptorsInfos.length; i ++) {
          this.idToVsbId.set(vsDescriptorsInfos[i]['vsDescriptorId'], new Map());
          // this.getVsBlueprint(vsDescriptorsInfos[i]['vsDescriptorId'], vsDescriptorsInfos[i]['vsBlueprintId']);

          var names = this.idToVsbId.get(vsDescriptorsInfos[i]['vsDescriptorId']);
          names.set(vsDescriptorsInfos[i]['vsDescriptorId'], 'nameTest');

        }        

        this.dataSource = new DescriptorsNaDataSource(this.tableData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
  }
}
