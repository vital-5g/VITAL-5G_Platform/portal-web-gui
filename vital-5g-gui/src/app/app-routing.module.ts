import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BlueprintsTcComponent } from './blueprints-components/blueprints-tc/blueprints-tc.component';
import { BlueprintsEcComponent } from './blueprints-components/blueprints-ec/blueprints-ec.component';
import { BlueprintsEComponent } from './blueprints-components/blueprints-e/blueprints-e.component';
import { BlueprintsVsComponent } from './blueprints-components//blueprints-vs/blueprints-vs.component';
import { ExperimentSwitchComponent } from './experiment-switch/experiment-switch.component';
import { DescriptorsESchedulerComponent } from './descriptors-e-scheduler/descriptors-e-scheduler.component';
import { DescriptorsEComponent } from './descriptors-e/descriptors-e.component';
import { ExperimentsComponent } from './experiments/experiments.component';
import { SitesSwitchComponent } from './sites-switch/sites-switch.component';
import { RegisterComponent } from './register/register.component';
import { BlueprintsEDetailsComponent } from './blueprints-components/blueprints-e-details/blueprints-e-details.component';
import { ExperimentsDetailsComponent } from './experiments-details/experiments-details.component';
import { BlueprintsEcDetailsComponent } from './blueprints-components/blueprints-ec-details/blueprints-ec-details.component';
import { BlueprintsVsDetailsComponent } from './blueprints-components/blueprints-vs-details/blueprints-vs-details.component';
import { DescriptorsEDetailsComponent } from './descriptors-e-details/descriptors-e-details.component';
import { DescriptorsVsDetailsComponent } from './descriptors-vs-details/descriptors-vs-details.component';
import { BlueprintsEStepperComponent } from './blueprints-components/blueprints-e-stepper/blueprints-e-stepper.component';
import { DescriptorsEcComponent } from './descriptors-ec/descriptors-ec.component';
import { DescriptorsTcComponent } from './descriptors-tc/descriptors-tc.component';
import { DescriptorsVsComponent } from './descriptors-vs/descriptors-vs.component';
import { NfvNsComponent } from './nfv-components/nfv-ns/nfv-ns.component';
import { NfvPnfComponent } from './nfv-components/nfv-pnf/nfv-pnf.component';
import { NfvVnfComponent } from './nfv-components/nfv-vnf/nfv-vnf.component';
import { TicketingSystemComponent } from './ticketing-system/ticketing-system.component';
import { ExperimentMetricDashboardComponent } from './experiment-metric-dashboard/experiment-metric-dashboard.component';
import { ExecutionTcDetailsComponent } from './execution-tc-details/execution-tc-details.component';
import { SupportToolsSchemasComponent } from './support-tools-schemas/support-tools-schemas.component';
import { SupportToolsNsdComponent } from './support-tools-nsd/support-tools-nsd.component';
import { SupportToolsComposerComponent } from './support-tools-composer/support-tools-composer.component';
import { FilesServiceComponent } from './files-service/files-service.component';
import { DeploymentRequestComponent } from './deployment-request/deployment-request.component';
import { BlueprintsNetAppsComponent } from './blueprints-components/blueprints-net-apps/blueprints-net-apps.component';
import { BlueprintNaDetailsComponent } from './blueprints-components/blueprint-na-details/blueprint-na-details.component';
import { DescriptorsNaComponent } from './descriptors-na/descriptors-na.component';
import { DescriptorsNaDetailsComponent } from './descriptors-na-details/descriptors-na-details.component';
import { AuthGuard } from './Guards/auth.guard';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { EquipmentsComponent } from './equipments/equipments.component';
import { BlueprintsTcDetailsComponent } from './blueprints-components/blueprints-tc-details/blueprints-tc-details.component';
import { DescriptorsTcDetailsComponent } from './descriptors-tc-details/descriptors-tc-details.component';
import { RunExperimentsComponent } from './run-experiments/run-experiments.component';
import { RunExperimentsDetailsComponent } from './run-experiments-details/run-experiments-details.component';
import { EquipmentsDetailComponent } from './equipments-detail/equipments-detail.component';
import { MultisiteFiveGComponent } from './multisite-five-g/multisite-five-g.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'register', component: RegisterComponent  },
  { path: 'design_experiment', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'home',component:HomeComponent, canActivate: [AuthGuard]},
  { path: 'blueprints_tc', component: BlueprintsTcComponent, canActivate: [AuthGuard] },
  // { path: 'blueprints_ec', component: BlueprintsEcComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_exp', component: BlueprintsEComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_vs', component: BlueprintsVsComponent, canActivate: [AuthGuard] },
  { path: 'request_experiment', component: ExperimentSwitchComponent, canActivate: [AuthGuard] },
  { path: 'schedule_experiment', component: DescriptorsESchedulerComponent, canActivate: [AuthGuard] },
  { path: 'descriptors_exp', component: DescriptorsEComponent, canActivate: [AuthGuard] },
  { path: 'vs_instances', component: ExperimentsComponent, canActivate: [AuthGuard] },
  { path: 'manage_site', component: SitesSwitchComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_e_details', component: BlueprintsEDetailsComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_ec_details', component: BlueprintsEcDetailsComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_vs_details', component: BlueprintsVsDetailsComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_na_details', component: BlueprintNaDetailsComponent, canActivate: [AuthGuard] },
  { path: 'blueprints_vs_monitoring', component: MonitoringComponent, canActivate: [AuthGuard] }, //here grafana
  { path: 'descriptors_e_details', component: DescriptorsEDetailsComponent, canActivate: [AuthGuard] },
  { path: 'descriptors_vs_details', component: DescriptorsVsDetailsComponent, canActivate: [AuthGuard] },
  // { path: 'descriptors_na_details', component: DescriptorsNaDetailsComponent, canActivate: [AuthGuard] },
  { path: 'onboard_exp_blueprint', component: BlueprintsEStepperComponent, canActivate: [AuthGuard]},
  { path: 'vs_instances_details', component: ExperimentsDetailsComponent, canActivate: [AuthGuard] },
  // { path: 'descriptors_ec', component: DescriptorsEcComponent , canActivate: [AuthGuard]},
  { path: 'descriptors_vs', component: DescriptorsVsComponent, canActivate: [AuthGuard] },
  // { path: 'descriptors_na', component: DescriptorsNaComponent, canActivate: [AuthGuard] },
  { path: 'descriptors_tc', component: DescriptorsTcComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: 'nfv_ns', component: NfvNsComponent, canActivate: [AuthGuard] },
  { path: 'nfv_pnf', component: NfvPnfComponent, canActivate: [AuthGuard] },
  { path: 'nfv_vnf', component: NfvVnfComponent, canActivate: [AuthGuard] },
  { path: 'net_apps', component: BlueprintsNetAppsComponent, canActivate: [AuthGuard] },
  // { path: 'tickets', component: TicketingSystemComponent, canActivate: [AuthGuard] },
  { path: 'metrics_dashboard', component: ExperimentMetricDashboardComponent, canActivate: [AuthGuard]},
  { path: 'execute_tc_details', component: ExecutionTcDetailsComponent, canActivate: [AuthGuard] },
  //TODO:remove comment when support tools is back
  { path: 'support_schemas', component: SupportToolsSchemasComponent, canActivate: [AuthGuard] },
  { path: 'support_nsd', component: SupportToolsNsdComponent, canActivate: [AuthGuard]},
  { path: 'support_composer', component: SupportToolsComposerComponent, canActivate: [AuthGuard] },
  { path: 'vnfs', component: FilesServiceComponent, canActivate: [AuthGuard] },
  { path: 'vnf-service', component: DeploymentRequestComponent, canActivate: [AuthGuard] },
  { path: 'multisite_equipment', component: EquipmentsComponent,canActivate: [AuthGuard]},
  { path: 'multisite_equipment_detail', component: EquipmentsDetailComponent, canActivate: [AuthGuard]},
  { path: 'blueprints_tc_details', component: BlueprintsTcDetailsComponent, canActivate: [AuthGuard]},
  { path: 'descriptors_tc_details', component: DescriptorsTcDetailsComponent, canActivate: [AuthGuard]},
  { path: 'experiments_run', component: RunExperimentsComponent, canActivate: [AuthGuard]},
  { path: 'equipments_run_details', component: RunExperimentsDetailsComponent, canActivate: [AuthGuard]},
  { path: 'multisite_fiveg_slice', component: MultisiteFiveGComponent, canActivate: [AuthGuard]},

  { path: '**', redirectTo: '/home' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }



