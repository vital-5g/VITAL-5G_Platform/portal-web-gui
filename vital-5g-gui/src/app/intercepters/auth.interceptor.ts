import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpClient,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, switchMap } from "rxjs/operators";
import { AuthService } from "../auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private http: HttpClient, private authService:AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {

    const req = request.clone({
      setHeaders: {
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
    
    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401 && localStorage.getItem("token")) {
          //TODO: update the url and the body below when the request for refresh token is avaiable
          return this.http
            .post("refresh url", {}, { withCredentials: true })
            .pipe(
              switchMap((res: any) => {
                //set new token
                this.authService.setUpToken(res);
                return next.handle(
                  request.clone({
                    setHeaders: {
                      Authorization: "Bearer " + localStorage.getItem("token"),
                    },
                  })
                );
                
              })
            );
        }
        else{
          return next.handle(
            request
          );
        }
      })
    );
  }
}
