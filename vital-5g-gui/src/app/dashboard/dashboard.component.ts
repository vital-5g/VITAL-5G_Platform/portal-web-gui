import { NsdsService } from './../nsds.service';
import { VnfdsService } from './../vnfds.service';
import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { BlueprintsEcService } from '../blueprints-ec.service';
import { BlueprintsExpService } from '../blueprints-exp.service';
import { BlueprintsTcService } from '../blueprints-tc.service';
import { BlueprintsVsService } from '../blueprints-vs.service';
import { DescriptorsEcService } from '../descriptors-ec.service';
import { DescriptorsExpService } from '../descriptors-exp.service';
import { DescriptorsTcService } from '../descriptors-tc.service';
import { DescriptorsVsService } from '../descriptors-vs.service';
import { HomeComponent } from '../home/home.component';

@Component({
  providers: [HomeComponent],
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // cards = [
  //   { title: 'Vertical Service Blueprints', subtitle: '', cols: 1, rows: 1, path: '/blueprints_vs' },
  //   { title: 'Execution Context Blueprints', subtitle: '', cols: 1, rows: 1, path: '/blueprints_ec' },
  //   { title: 'Test Case Blueprints', subtitle: '', cols: 1, rows: 1, path: '/blueprints_tc' },
  //   { title: 'Experiments Blueprints', subtitle: '', cols: 1, rows: 1, path: '/blueprints_exp' },
  //   { title: 'Virtual Network Functions', subtitle: '', cols: 1, rows: 1, path: '' }
  // ];


  // cards = [
  //   { title: 'NetApps', subtitle: '', path: '/net_apps', disabled:this.home.isBpNetappSubMenu(), requiredRole:null },
  //   { title: 'Vertical Service Blueprints', subtitle: '', path: '/blueprints_vs', disabled:this.home.isBpVSSubMenu(), requiredRole:null },
  //   // { title: 'Execution Context Blueprints', subtitle: '',  path: '/blueprints_ec', disabled:false, requiredRole:null },
  //   { title: 'Test Case Blueprints', subtitle: '', path: '/blueprints_tc', disabled:false, requiredRole:null },
  //   { title: 'Experiments Blueprints', subtitle: '', path: '/blueprints_exp', disabled:false, requiredRole:null },
  //   { title: 'Virtual Network Functions', subtitle: '', path: '/nfv_vnf', disabled:false, requiredRole:null },
  //   { title: 'Network Services', subtitle: '', path: '/nfv_ns', disabled:false, requiredRole:null },
  //   { title: 'NetApp validator', subtitle: '', path: '/support_schemas', disabled:true, requiredRole:null }
  // ];

  vsBCounter: number = 0;
  ecBCounter: number = 0;
  tcBCounter: number = 0;
  expBCounter: number = 0;
  vnfCounter: number = 0;
  nsdCounter : number = 0;
  breakpoint
  counters: number[] = [this.vsBCounter, this.ecBCounter, this.tcBCounter, this.expBCounter];
  isBool: boolean;
  cards: { title: string; subtitle: string; path: string; disabled: boolean; requiredRole: any; }[];

  constructor(private breakpointObserver: BreakpointObserver,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer,
    private router: Router,
    private blueprintsEcService: BlueprintsEcService,
    private blueprintsExpService: BlueprintsExpService,
    private bluepritnsTcService: BlueprintsTcService,
    private blueprintsVsService: BlueprintsVsService,
    private descriptorsEcService: DescriptorsEcService,
    private descriptorsExpService: DescriptorsExpService,
    private descriptorsTcService: DescriptorsTcService,
    private descriptorsVsService: DescriptorsVsService,
    private vnfdsService: VnfdsService,
    private nsdsService: NsdsService,
    private home: HomeComponent

    ) {
      iconRegistry.addSvgIcon(
        'library_add',
        sanitizer.bypassSecurityTrustResourceUrl('assets/images/library_add.svg'));
    }

    ngOnInit(): void {
      //TODO:check what functions are needed below, and which are no more required for vital-5g
      //this.getVsBlueprints();
      //this.getVsDescriptors();
      // this.getEcBlueprints();
      //this.getEcDescriptors();
      // this.getTcBlueprints();
      //this.getTcDescriptors();
      //this.getExpBlueprints();
      this.home.roles = this.home.getRoles();
      //this.getExpDescriptors();
      // this.getVnfDescriptor();
      // this.getNsDescriptor();
      
  this.cards = [
    { title: 'Network Applications', subtitle: '', path: '/net_apps', disabled:!this.home.isBpNetappSubMenu(), requiredRole:null },
    { title: 'Vertical Service Blueprints', subtitle: '', path: '/blueprints_vs', disabled:!this.home.isBpVSSubMenu(), requiredRole:null },
    // { title: 'Execution Context Blueprints', subtitle: '',  path: '/blueprints_ec', disabled:false, requiredRole:null },
    { title: 'Test Case Blueprints', subtitle: '', path: '/blueprints_tc', disabled:!this.home.isBpTCSubMenu(), requiredRole:null },
    { title: 'Experiments Blueprints', subtitle: '', path: '/blueprints_exp', disabled:!this.home.isBpExpSubMenu(), requiredRole:null },
    //{ title: 'Virtual Network Functions', subtitle: '', path: '/nfv_vnf', disabled:!this.home.isDesignVNFSubMenu(), requiredRole:null },
    //{ title: 'Network Services', subtitle: '', path: '/nfv_ns', disabled:!this.home.isDesignNetworkServiceSubMenu(), requiredRole:null },
    { title: 'Support Tools', subtitle: '', path: '/support_schemas', disabled:true, requiredRole:null }//disabled:!this.home.isDesignNetworkValidatorSubMenu(), requiredRole:null }
  ];


      //this.breakpoint = (window.outerWidth <= 400) ? 1 : 6;
      //this.breakpoint = (window.outerWidth <= 1300) ? 1 : 6;
      //this.breakpoint = (window.outerWidth <= 2000) ? 2 : 3;
      
      // call functions to disable or enable submenu
    }

    getVsBlueprints() {
      this.blueprintsVsService.getVsBlueprints().subscribe(vsBlueprints => {
        //this.cards.push({ title: 'Vertical Service Blueprints', subtitle: '', counter: vsBlueprints.length, cols: 1, rows: 1, path: '/blueprints_vs' });
        this.vsBCounter = vsBlueprints.length;
        this.counters[0] = vsBlueprints.length;
       
      });
    }

    getEcBlueprints() {
      this.blueprintsEcService.getCtxBlueprints().subscribe(ctxBlueprints => {
        //this.cards.push({ title: 'Execution Context Blueprints', subtitle: '', counter: ctxBlueprints.length, cols: 1, rows: 1, path: '/blueprints_ec' });
        this.ecBCounter = ctxBlueprints.length;
        this.counters[1] = ctxBlueprints.length;
      });
    }

    getTcBlueprints() {
      this.bluepritnsTcService.getTcBlueprints().subscribe(tcBlueprints => {
        //this.cards.push({ title: 'Test Case Blueprints', subtitle: '', counter: tcBlueprints.length, cols: 1, rows: 1, path: '/blueprints_tc' });
        this.tcBCounter = tcBlueprints.length;
        this.counters[2] = tcBlueprints.length;
      });
    }

    getExpBlueprints() {
      this.blueprintsExpService.getExpBlueprints().subscribe(expBlueprints => {
        //this.cards.push({ title: 'Experiments Blueprints', subtitle: '', counter: expBlueprints.length, cols: 1, rows: 1, path: '/blueprints_exp' });
        //this.cards.push({ title: 'Virtual Network Functions', subtitle: '', counter: 0, cols: 1, rows: 1, path: '' });
        this.expBCounter = expBlueprints.length;
        this.counters[3] = expBlueprints.length;
      });
    }

    getVnfDescriptor() {
      this.vnfdsService.getVnfPackageInfos().subscribe(vnfDescriptor => {
        this.vnfCounter = vnfDescriptor.length;
        this.counters[4] = vnfDescriptor.length;
      });
    }
    getNsDescriptor() {
      this.nsdsService.getNsdInfos().subscribe(nsdDescriptor => {
        this.nsdCounter = nsdDescriptor.length;
        this.counters[5] = nsdDescriptor.length;
        //console.log(this.counters[5])
      });
    }
    
    getVsDescriptors() {
      this.descriptorsVsService.getVsDescriptors().subscribe(vsDescriptors => {
        //this.cards.push({ title: 'Vertical Service Descriptors', subtitle: '', counter: vsDescriptors.length, cols: 1, rows: 1, path: '/descriptors_vs' })
      });
    }      

    getEcDescriptors(){
      this.descriptorsEcService.getCtxDescriptors().subscribe(ctxDescriptors => {
        //this.cards.push({ title: 'Execution Context Descriptors', subtitle: '', counter: ctxDescriptors.length, cols: 1, rows: 1, path: '/descriptors_ec' })
      });
    }

    getTcDescriptors() {
      this.descriptorsTcService.getTcDescriptors().subscribe(tcDescriptors => {
        //this.cards.push({ title: 'Test Case Descriptors', subtitle: '', counter: tcDescriptors.length, cols: 1, rows: 1, path: '/descriptors_tc' })
      });
    }  

    getExpDescriptors() {
      this.descriptorsExpService.getExpDescriptors().subscribe(expDescriptors => {
        //this.cards.push({ title: 'Experiments Descriptors', subtitle: '', counter: expDescriptors.length, cols: 1, rows: 1, path: '/descriptors_exp' })
      });
    }


    goTo(path: string) {
      if (path.indexOf('http') >= 0) {
        window.open(path, '_blank');
      } else {
        this.router.navigate([path]);
      }
    }
    
    onResize(event) {
      this.breakpoint = (event.target.innerWidth <= 400) ? 3 :3;
      //this.breakpoint = (event.target.Output <= 800) ? 3 : 3;
      //this.breakpoint = (event.target.innerWidth >= 700) ? 3 : 3;
     //this.breakpoint = (event.target.innerWidth <= 800) ? 2 : 3;
      
    }
}
