
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ExperimentInfo } from './experiments/experiment-info';
import { RunExperimentInfo } from './run-experiments/run-experiment-info';
import { environment } from './environments/environments';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RunExperimentsService {

  private baseUrl = environment.eLcmBaseURL;
  private experimentInfoUrl = 'experiment/';
  private executionUrl = 'execution/';

  httpOptions = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
  };


  httpOptionsWithAccept:Object = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json',
        Accept: 'text/plain;charset=ISO-8859-1',
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }),
      responseType: 'text'
  };

  constructor(private http: HttpClient,
    private authService: AuthService,
    private router: Router) { }

  getExperiments(): Observable<RunExperimentInfo[]> {
    return this.http.get<RunExperimentInfo[]>(this.baseUrl + this.experimentInfoUrl, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched experimentInfos - SUCCESS')),
        catchError(this.authService.handleError<RunExperimentInfo[]>('getExperiments', []))
      );
  }

  getExperiment(experimentId: string, expDId): Observable<ExperimentInfo[]> {
    // var requestParams = '';
    // if (experimentId != null) {
    //   requestParams += '?expId=' + experimentId;
    // }
    // if (expDId != null) {
    //   requestParams += '?expDId=' + expDId;
    // }
    //console.log('get this experiment');
    return this.http.get<ExperimentInfo[]>(this.baseUrl + this.experimentInfoUrl + experimentId, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched experimentInfo - SUCCESS')),
        catchError(this.authService.handleError<ExperimentInfo[]>('getExperiment', []))
      );
  }

  postExperiment(expRequest: object, redirection = ''): Observable<any> {
    return this.http.post(this.baseUrl + this.experimentInfoUrl , expRequest,  this.httpOptions)
      .pipe(
        tap(
          instanceId => {
            this.authService.log(`created experiment w/ id=${instanceId}`, 'SUCCESS', false);
            // this.router.navigate([redirection]).then(() => {
            //   window.location.reload();
            // });
        }
        ),
        catchError(this.authService.handleError<string>('postExperiment'))
      );
  }
  postExperimentExecution(experimentId: string, expRequest: object) : Observable<any> {
    return this.http.post(this.baseUrl + this.experimentInfoUrl + experimentId + this.executionUrl, expRequest, this.httpOptions)
    .pipe(
      tap(
        instanceId => {
          this.authService.log('created experiment execution', 'SUCCESS', false);
        }
      )
    )
  }
  postExperimentRun(experimentId: string, executionId: string, expRequest: object) : Observable<any> {
    return this.http.post(this.baseUrl + this.experimentInfoUrl + experimentId + this.executionUrl + executionId + '/run', expRequest, this.httpOptions)
    .pipe(
      tap(
        instanceId => {
          this.authService.log('created experiment execution', 'SUCCESS', false);
        }
      )
    )
  }

  deleteExperiment(experimentId: string): Observable<String> {
    debugger
    return this.http.delete(this.baseUrl + this.experimentInfoUrl + experimentId+ '/delete', this.httpOptions)
    .pipe(
      tap((result: String) => this.authService.log(`deleted Experiment w/ id=${experimentId}`, 'SUCCESS', false)),
      catchError(this.authService.handleError<String>('deleteExperiment'))
    );
  }
  forcedeleteExperiment(experimentId: string): Observable<String> {
    return this.http.delete(this.baseUrl + this.experimentInfoUrl + experimentId + '?forced=true', this.httpOptions)
    .pipe(
      tap((result: String) => this.authService.log(`deleted Experiment w/ id=${experimentId}`, 'SUCCESS', false)),
      catchError(this.authService.handleError<String>('deleteExperiment'))
    );
  }
  
 
}

