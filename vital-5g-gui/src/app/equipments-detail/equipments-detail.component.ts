import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { EquipmentsService } from '../equipments.service';
import { EquipmentDatasource } from '../equipments/equipment-datasource';
import { EquipmentsDetailDataSource, EquipmentsDetailItemKV } from './equipments-detail.datasource';

@Component({
  selector: 'app-equipments-detail',
  templateUrl: './equipments-detail.component.html',
  styleUrls: ['./equipments-detail.component.css']
})
export class EquipmentsDetailComponent implements OnInit {

  @ViewChild(MatPaginator, { static: false})paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<EquipmentsDetailItemKV>;
  dataSource: EquipmentsDetailDataSource;

  tableData: EquipmentsDetailItemKV[] = [];
  displayedColumns = ["key", "value"];
  executionsColumns = ["id","name","version"]
  equipment: any;
  constructor(
    private equipmentService: EquipmentsService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    var eqpId = localStorage.getItem("eqpId");
    this.dataSource = new EquipmentsDetailDataSource(this.tableData);
    this.getEquipment(eqpId);
  }

  getEquipment(equipmentId:string) {
    this.equipmentService.getEquipments().subscribe
    (
      ((equipmentInfos: any) => { 
        this.equipmentService
        .getEquipment(equipmentId)
        .subscribe((equipmentInfo: any) => {
          equipmentInfo["activeExperiments"]

          this.equipment = equipmentInfo;
          console.log(this.equipment)
          this.tableData.push({
            key: "Id",
            value: this.equipment.expBlueprintId
          });

          this.tableData.push({
            key: "Name",
            value: this.equipment.name
          });

          this.tableData.push({
            key: "Version",
            value: this.equipment.version
          });
          var values = [];

        if (this.equipment['kpis']) {
          for (var i = 0; i < this.equipment['kpis'].length; i++) {
            values.push(this.equipment['kpis'][i]['name']);
          }
          this.tableData.push({key: "KPIs", value: values});
        }

        this.tableData.push({
          key: "Access Level",
          value: this.equipment.accessLevel
        });
        this.tableData.push({
          key: "Description",
          value: this.equipment.description
        });
        this.tableData.push({
          key: "Test case Blueprint",
          value: this.equipment.tcBlueprintId
        });
        this.tableData.push({
          key: "Testbed",
          value: this.equipment.testbed
        });
        this.tableData.push({
          key: "UseCase",
          value: this.equipment.useCase
        });
        this.tableData.push({
          key: "Vertical service blueprint",
          value: this.equipment.vsBlueprintId
        });

          this.dataSource = new EquipmentsDetailDataSource(this.tableData);
          this.dataSource.sort = this.sort;
          //this.dataSource.paginator = this.paginator;
          this.table.dataSource = this.dataSource;

        })
       })
    )
      
  }
}
