import { DataSource } from "@angular/cdk/collections";
import { MatSort } from "@angular/material/sort";
import { merge, Observable, of as observableOf } from "rxjs";
import { map } from "rxjs/operators";

export interface EquipmentsDetailItemKV {
    key: string;
    value: string[];
}

export class EquipmentsDetailDataSource extends DataSource<EquipmentsDetailItemKV> {
    data: EquipmentsDetailItemKV[] = [];
    sort: MatSort;

    constructor(data: EquipmentsDetailItemKV[]) {
        super();
        this.data = data;
    }

    connect(): Observable<EquipmentsDetailItemKV[]> {
        const dataMutations = [
            observableOf(this.data),
            this.sort.sortChange
        ];
        return merge(...dataMutations).pipe(map(() => {
            return this.getSortedData([...this.data]);
          }));
    }
    disconnect() {}

    private getSortedData(data: EquipmentsDetailItemKV[]){
        if (!this.sort.active || this.sort.direction === '') {
            return data;
          }
      
          return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'key':  return compare(a.key, b.key, isAsc);
          default: return 0;
            }
          });
    }
}
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }