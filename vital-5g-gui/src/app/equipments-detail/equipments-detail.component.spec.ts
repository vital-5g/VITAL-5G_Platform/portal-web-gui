import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';

import { EquipmentsDetailComponent } from './equipments-detail.component';

describe('EquipmentsDetailComponent', () => {
  let component: EquipmentsDetailComponent;
  let fixture: ComponentFixture<EquipmentsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [MatSnackBarModule,RouterTestingModule],
      declarations: [ EquipmentsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientModule]})
    fixture = TestBed.createComponent(EquipmentsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
