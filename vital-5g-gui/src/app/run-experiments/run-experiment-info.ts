export class RunExperimentInfo {
    elcmId: string;
experimentConfigurationParameters: 
    {
    additionalProp1: string,
    additionalProp2: string, 
    additionalProp3: string
    };
experimentDescriptorId: string;
status: string;
vsId: string;
}
