import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ExperimentInfo } from '../experiments/experiment-info';
import { RunExperimentInfo } from './run-experiment-info';
import { RunExperimentsService } from '../run-experiments.service';
import { NewRunExperimentModalComponent } from "../new-run-experiment-modal/new-run-experiment-modal.component";

@Component({
  selector: 'app-run-experiments',
  templateUrl: './run-experiments.component.html',
  styleUrls: ['./run-experiments.component.css']
})
export class RunExperimentsComponent implements OnInit {
  selectedState :any;
  selectedSite: any;
  states: string[] = [
    "SCHEDULING",
    "ACCEPTED",
    "READY",
    "INSTANTIATING",
    "INSTANTIATED",
    "CONFIGURING",
    "RUNNING",
    "TERMINATING",
    "TERMINATED",
    "FAILED",
    "REFUSED",
    "ABORTED"
  ];
  tableData: any = [];
  dataSource = new MatTableDataSource(this.tableData);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  displayedColumns = ['elcmId', 'status', 'vsId','view','delete']//,'forcedelete'];//, 'buttons'];

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private run_experimentService: RunExperimentsService
  ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.tableData);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.getExperiments();
  }
  
  getExperiments() {
    this.run_experimentService.getExperiments().subscribe((experimentInfos: RunExperimentInfo[]) =>
      {
        this.tableData = experimentInfos;

        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }
  viewExperiment(elcmId: string) {
    console.log(elcmId);
    localStorage.setItem('elcmId', elcmId);
    this.router.navigate(["/equipments_run_details"]);
  }
  deleteExperiment(elcmId: string) {
    this.run_experimentService.deleteExperiment(elcmId).subscribe(
      () => {this.getExperiments();}
    );
  }
  onStatusSelected(event: any) {
    var selectedState = event.value;
    this.dataSource.filter = selectedState.trim();
  }
  clearFilter() {
    this.dataSource.filter = '';
  }
  openDialogs(): void {
    const dialogRef = this.dialog.open(NewRunExperimentModalComponent, {
      width: '30%',
      data: {experimentConfigurationParameters: {}},//{tenantId: 'NXW'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(!result){
        return
      }
      console.log(result);
        debugger;
      this.run_experimentService.postExperiment(result).subscribe(resp => {
        
        this.getExperiments();
      })
    });
  }

}
