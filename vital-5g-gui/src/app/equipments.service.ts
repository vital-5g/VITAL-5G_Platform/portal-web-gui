import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { environment } from './environments/environments';
import { EquipmentInfo } from './equipments/equipment-info';

@Injectable({
  providedIn: 'root'
})
export class EquipmentsService {

  // to do portalUrl to the real Url when provided
  private baseUrl = environment.portalBaseUrl;
  //private equipmentInfoUrl = '{testbed}';
  private equipmentInfoUrl = 'expbs';

  httpOptions = {
    headers: new HttpHeaders(
      {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    )
  };

  constructor(private http: HttpClient,
    private authServicce: AuthService) { }
  
  getEquipments(): Observable<EquipmentInfo[]> {
    console.log('getEquipmentinfos');
    return this.http.get<EquipmentInfo[]>(this.baseUrl + this.equipmentInfoUrl, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched equimentInfos - SUCCESS')),
        catchError(this.authServicce.handleError<EquipmentInfo[]>('getEquipments', []))
      );
  }

  getEquipment(equipmentId: string): Observable<EquipmentInfo[]> {
    return this.http.get<EquipmentInfo[]>(this.baseUrl + this.equipmentInfoUrl + "/" + equipmentId, this.httpOptions)
    .pipe(
      tap(_ => console.log('fetch equipmentInfo - SUCCESS')),
      catchError(this.authServicce.handleError<EquipmentInfo[]>('getEquipment'))
    )
  }

  createEquipment(onBoardEquipRequest: Object): Observable<String> {
    return this.http.post(this.baseUrl + this.equipmentInfoUrl, onBoardEquipRequest, this.httpOptions)
    .pipe(
      tap((equipmentId: String) => this.authServicce.log(`added equipment w/ id=${equipmentId}`, 'SUCCESS', true)),
      catchError(this.authServicce.handleError<String>('postEquipment'))
    );
  }

  deleteEquipment(equipmentId: string): Observable<String> {
    return this.http.delete(this.baseUrl + this.equipmentInfoUrl + "/" + equipmentId, this.httpOptions)
    .pipe(
      tap((result: String) => (this.authServicce.log(`deleted equipment w/ ${equipmentId}`, 'SUCCESS', true)),
      catchError(this.authServicce.handleError<String>('deleteEquipment'))
    )
    );
  }
  forcedeleteEquipment(equipmentId: string): Observable<String> {
    return this.http.delete(this.baseUrl + this.equipmentInfoUrl + "/" + equipmentId+ '?forced=true', this.httpOptions)
    .pipe(
      tap((result: String) => (this.authServicce.log(`deleted equipment w/ ${equipmentId}`, 'SUCCESS', true)),
      catchError(this.authServicce.handleError<String>('deleteEquipment'))
    )
    );
  }
}
