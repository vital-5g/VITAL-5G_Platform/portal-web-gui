import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import {  AfterContentChecked } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements AfterContentChecked,AfterViewInit, OnInit {
  @ViewChild('sidenav') public sidenav: MatSidenav;
  
  showHeader = false;
  constructor(public router: Router) {
    this.router.events.subscribe((e:any) => {
      let url = e.url;
      if(url != undefined){
        if( e.url == ''|| e.url == '/' || e.url == '/login'|| e.url == '/register')
        {
          this.sidenav.close();
          this.showHeader = false;
          // this.showHeader = true;
        }
        else{
          this.showHeader = true;
          
          window.innerWidth > 800?
          this.sidenav.open():
          this.sidenav.close();
        }
      }
    });
  }
  ngOnInit(): void {
    // this.sidenav.close();
  }
  title = 'responsive-mat';

  ngAfterViewInit(): void {
    
    document.getElementById("cmain").style.marginBottom  = (  document.getElementById("footer").offsetHeight).toString()+"px" ;
    document.getElementById("sideNavContainter").style.marginBottom  = (  document.getElementById("footer").offsetHeight+10).toString()+"px" ;
    // document.getElementById("page-container").style.marginBottom  = (  document.getElementById("footer").offsetHeight).toString()+"px" ;
    
    
  }

  ngAfterContentChecked() {
     
  }

  

}
