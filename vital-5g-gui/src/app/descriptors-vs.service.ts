import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VsDescriptorInfo } from './descriptors-vs/vs-descriptor-info';
import { environment } from './environments/environments';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DescriptorsVsService {

  private baseUrl = environment.portalBaseUrl;
  // private vsDescriptorInfoUrl = 'vsdescriptor';
  private vsDescriptorInfoUrl = 'vsds';

  httpOptions = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
  };

  constructor(private http: HttpClient,
    private authService: AuthService) { }

  getVsDescriptors(): Observable<VsDescriptorInfo[]> {
    return this.http.get<VsDescriptorInfo[]>(this.baseUrl + this.vsDescriptorInfoUrl, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched vsDescriptorInfos - SUCCESS')),
        catchError(this.authService.handleError<VsDescriptorInfo[]>('getVsDescriptors', []))
      );
  }

  getVsDescriptor(vsDescriptorId: string): Observable<VsDescriptorInfo> {
    return this.http.get<VsDescriptorInfo>(this.baseUrl + this.vsDescriptorInfoUrl + "/" + vsDescriptorId, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched vsDescriptorInfo - SUCCESS')),
        catchError(this.authService.handleError<VsDescriptorInfo>('getVsBlueprint'))
      );
  }
  
  addVsDescriptor(vsDescriptor: VsDescriptorInfo): Observable<any> {
    return this.http.post<VsDescriptorInfo>(this.baseUrl + this.vsDescriptorInfoUrl , vsDescriptor , this.httpOptions)
      .pipe(
        tap(_ => this.authService.log(`added VS Descriptor`, 'SUCCESS', true)),
        catchError(this.authService.handleError<String>('postVsDescriptor'))
      );
  }
  
  deleteVsDescriptor(vsDescriptorId: string): Observable<any> {
    return this.http.delete<VsDescriptorInfo>(this.baseUrl + this.vsDescriptorInfoUrl + "/" + vsDescriptorId, this.httpOptions)
      .pipe(
        tap(_ => this.authService.log(`deleted VS Descriptor`, 'SUCCESS', true)),
        catchError(this.authService.handleError<String>('deleteVsDescriptor'))
      );
  }
  forcedeleteVsDescriptor(vsDescriptorId: string): Observable<any> {
    return this.http.delete<VsDescriptorInfo>(this.baseUrl + this.vsDescriptorInfoUrl + "/" + vsDescriptorId + "?forced=true" , this.httpOptions)
      .pipe(
        tap(_ => this.authService.log(`deleted VS Descriptor`, 'SUCCESS', true)),
        catchError(this.authService.handleError<String>('deleteVsDescriptor'))
      );
  }
}
