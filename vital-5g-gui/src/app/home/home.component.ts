import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService, User } from '../users.service';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  
  username: string = '';
  roles: string = ''; 
  disableBpNetAppSubmenu: boolean=true;
  disableBpVSSubmenu: boolean=true;
  disableBpTCSubmenu: boolean=true;
  disableBpExpSubmenu: boolean=true;

  constructor(private router: Router,
    private usersService: UsersService,
    private authService: AuthService) { }

  ngOnInit() {
    this.username = localStorage.getItem('username');
    this.roles = this.getRoles();
    localStorage.setItem('rolemenu',this.roles);
    console.log(this.roles);
    // this.assignDefaultRole(this.roles);
    // let elem1: HTMLElement = document.getElementById('show_blue');
    // elem1.setAttribute("style", "display:inline;");
    // let elem2: HTMLElement = document.getElementById('show_desc');
    // elem2.setAttribute("style", "display:inline;");
    // let elem3: HTMLElement = document.getElementById('show_manage_site');
    // elem3.setAttribute("style", "display:none;");
  }

  goTo(path: string) {
    if (path.indexOf('/design_experiment') >= 0) {
      localStorage.setItem('role', 'DESIGNER');
    }
    if (path.indexOf('/request_experiment') >= 0) {
      localStorage.setItem('role', 'Experimenter');
    }
    if (path.indexOf('/vs_instances') >= 0) {
      localStorage.setItem('role', 'Experimenter');
    }
    if (path.indexOf('/manage_site') >= 0) {
      localStorage.setItem('role', 'SITE_MANAGER');
    }
    this.router.navigate([path]);
  }

  logout() {
    this.authService.logout('/login').subscribe(tokenInfo => console.log(JSON.stringify(tokenInfo, null, 4)));
  }

  getRoles() {
    return localStorage.getItem('roles');
  }

  isInRole(role:string){
    //console.log(role);
    return this.roles.indexOf(role) >= 0;

  }

  assignDefaultRole(role:any){
    if(typeof(role) === 'object'){
      if(role.indexOf('Platform_admin')){
        this.roles = "Platform_admin";
        localStorage.setItem('roles',this.roles);
        return this.roles;
      }
      else{
        return this.roles;
      }
    }
    else{
      this.roles=role;
      return this.roles;
    }
  }

  admin = "Exprerimenter,Platform_admin,Vertical_Service_Provider,NetApp_Developer";
  platform_admin: string = "Platform_admin";
  test_admin: string = "Testbed_Admin";//,default-roles-vital-5g,offline_access,uma_authorization";
  platform_technician: string= "Platform_Technician";//,default-roles-vital-5g,offline_access,uma_authorization";
  netapp_dev: string= "NetApp_Developer";//"default-roles-vital-5g,offline_access,uma_authorization,NetApp_Developer";
  vs_provider: string= "Vertical_Service_Provider";// default-roles-vital-5g,offline_access,Vertical_Service_Provider,uma_authorization
  experimenter: string= "Experimenter"; // Exprerimenter,default-roles-vital-5g,offline_access,uma_authorization
  managesite_manager: string= "T&L_Site_Infrastructure_Manager";// "default-roles-vital-5g,offline_access,T&L_Site_Infrastructure_Manager,uma_authorization";
  //for request experiment
  getDesignMenu(){
    const menu= localStorage.getItem('rolemenu');
    //console.log(menu);
    //let AcceptedDesignRole = [this.platform_admin,this.test_admin,this.platform_technician,this.netapp_dev,this.vs_provider];
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.netapp_dev,0)){
      return true;
    }
    else{
      return false
    }
  }
  
  isDesignRoute(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.netapp_dev,0)){
      return this.router.navigate(['/design_experiment']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  //for request experiment
  getRequestMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      return true;
    }
    else{
      return false
    }
  }
  
  isRequestRoute(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      console.log(this.roles)
      return this.router.navigate(['/request_experiment']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }
  
  //for manage VS instances menu
  getManageMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      return true;
    }
    else{
      return false
    }
  }
  
  isManageRoute(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0) ){
      return this.router.navigate(['/vs_instances']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  
  //for manage site menu
  getManageSiteMenu(){
    const menu= localStorage.getItem('rolemenu');
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
        this.roles.includes(this.managesite_manager,0)){
      return true;
    }
    else{
      return false
    }
  }
  
  isManageSiteRoute(){
    // when manage site and site inventory are clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.managesite_manager,0)){
      this.router.navigate(['/multisite_equipment']);
    }
    else{
      this.router.navigate(['/home']);
    }
  }
  isManageSite5GRoute(){
    // when 5G slice is clicked
    if(this.roles == this.platform_admin || this.roles == this.test_admin || this.roles == this.platform_technician || this.roles == this.managesite_manager){
      //this.router.navigate(['/multisite_fiveg_slice']);
      this.router.navigate(['/home']);
    }
    else{
      this.router.navigate(['/home']);
    }
  }

  // BLUEPRINTS
  //Net App
  isBpNetappSubMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0) ||
  this.roles.includes(this.netapp_dev,0)){
        return true;
  }
  else {
    return false;
  }
}
  isDesignNetAppRoute(){
    // when Blueprint Network Applications is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
      this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0) ||
    this.roles.includes(this.netapp_dev,0)){
      //console.log(this.roles);
      return this.router.navigate(['/net_apps']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  // Vertical Service
  isBpVSSubMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
        return true;
  }
  else{
    return false;
  }
}
  isDesignBlueprintVSRoute(){
    // when Blueprint VS is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      return this.router.navigate(['/blueprints_vs']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  //Test Case

  isBpTCSubMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0)){
        return true;
  }
else{
  return false;
}}

  isDesignBlueprintTCRoute(){
    // when Blueprint TC is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0)){
      return this.router.navigate(['/blueprints_tc']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  // Experiment

  
  isBpExpSubMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.experimenter,0)){
        return true;
  }
else{
  return false;
}}

  isDesignBlueprintExpRoute(){
    // when Blueprint Exp is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.experimenter,0)){
        return this.router.navigate(['/blueprints_exp']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  // DESCRIPTOR
  isDesignDescriptorVSRoute(){
    // when descriptor VS is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      return this.router.navigate(['/descriptors_vs']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }
  isDesignDescriptorTCRoute(){
    // when descriptor TC is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0)){
      return this.router.navigate(['/descriptors_tc']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }
  isDesignDescriptorExpRoute(){
    // when descriptor Exp is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.experimenter,0) ){
        return this.router.navigate(['/descriptors_exp']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  isVSInstanceRoute(){
    // when VS instance is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
     this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      return this.router.navigate(['/vs_instances']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

  isRunExpRoute(){
    // when run experiment is clicked
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
     this.roles.includes(this.vs_provider,0) || this.roles.includes(this.experimenter,0)){
      return this.router.navigate(['/experiments_run']);
    }
    else{
      return this.router.navigate(['/home']);
    }
  }

   // when Virtual Network function submenu is clicked
   isDesignVNFSubMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.netapp_dev,0)){
        return true;
  }
  else {
    return false;
  }
}

  // when Network service submenu is clicked
  isDesignNetworkServiceSubMenu(){
    if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
    this.roles.includes(this.netapp_dev,0)){
        return true;
  }
  else {
    return false;
  }
}
  
    // when Network validator is clicked
    isDesignNetworkValidatorSubMenu(){
      if(this.roles.includes(this.platform_admin,0) || this.roles.includes(this.test_admin,0) || this.roles.includes(this.platform_technician,0) ||
      this.roles.includes(this.netapp_dev,0)){
          return true;
    }
    else {
      return false;
    }
  }
}
