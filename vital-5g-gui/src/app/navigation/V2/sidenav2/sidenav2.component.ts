import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { HomeComponent } from 'src/app/home/home.component';

@Component({
  providers: [HomeComponent],
  selector: 'app-sidenav2',
  templateUrl: './sidenav2.component.html',
  styleUrls: ['./sidenav2.component.scss']
})
export class Sidenav2Component implements OnInit {
  //roles: string;

  constructor(private _location: Location,
      private home: HomeComponent) { }

  ngOnInit(): void {
    this.home.roles = this.home.getRoles()
  }

  backClicked() {
    this._location.back();
  }
  
  getRole() {
    return localStorage.getItem('role');
  }
  
  //for manage site menu
  getManageSiteMenu(){
    return this.home.getManageSiteMenu();
  }
  isManageSiteRoute(){
    //console.log(this.roles);
    this.home.isManageSiteRoute();
  }
  isManageSite5GRoute(){
    //console.log(this.roles);
    this.home.isManageSite5GRoute();
  }

  // DASHBOARD nav
  isDesignRoute(){
    this.home.isDesignRoute();
  }
  // BLUEPRINTS
  isDesignNetAppRoute(){
    this.home.isDesignNetAppRoute();
  }
  isDesignBlueprintVSRoute(){
    this.home.isDesignBlueprintVSRoute();
  }
  isDesignBlueprintTCRoute(){
    this.home.isDesignBlueprintTCRoute();
  }
  isDesignBlueprintExpRoute(){
    this.home.isDesignBlueprintExpRoute();
  }

  //DESCRIPTOR
  isDesignDescriptorVSRoute(){
    this.home.isDesignDescriptorVSRoute();
  }
  isDesignDescriptorTCRoute(){
    this.home.isDesignDescriptorTCRoute();
  }
  isDesignDescriptorExpRoute(){
    this.home.isDesignDescriptorExpRoute();
  }

  //VS instances
  isVSInstanceRoute(){
    this.home.isVSInstanceRoute();
  }

  // run experiment
  isRunExpRoute(){
    this.home.isRunExpRoute();
  }
}
