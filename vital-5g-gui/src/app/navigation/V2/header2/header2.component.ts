import { Component, OnInit , EventEmitter,Output} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-header2',
  templateUrl: './header2.component.html',
  styleUrls: ['./header2.component.css']
})
export class Header2Component implements OnInit {
  @Output() sideNavigationToggle = new EventEmitter();
  urlInfo : string;
  linkDash:boolean;
  showSideNav = false;
  username: string;
  constructor(private router: Router,
    private authService: AuthService) {
    this.router.events.subscribe((e:any) => {

      let url = e.url;
      if(url){
        if( e.url == '/login'|| e.url == '/register'/*|| e.url == '/home' ||e.url == '/'*/)
        {
          this.showSideNav = false;
        }
        else{
          this.showSideNav = true;
        }
      }
    });
}


  ngOnInit(): void {
    this.username = localStorage.getItem('username');
    this.menuToggleOnOutClick();
    /*
    this.router.events.subscribe((routerData) => {
      this.urlInfo=routerData['url'];
      if(this.urlInfo=='/login')
      this.linkDash=true;
      else{
        this.linkDash=false;
      }
   })   
   */
  }
  
  onToggleOpenSidenav(){
    this.sideNavigationToggle.emit()
  }
  changeStyleReg(){
    let elem1: HTMLElement = document.getElementById('show_reg');
    elem1.setAttribute("style", "display:none;");

    let elem: HTMLElement = document.getElementById('show_login');
    elem.setAttribute("style", "display:inline;");
  }
  changeStyleLog(){
    let elem1: HTMLElement = document.getElementById('show_reg');
    elem1.setAttribute("style", "display:inline;");

    let elem: HTMLElement = document.getElementById('show_login');
    elem.setAttribute("style", "display:none;");
  }

  toggleMenu(id:string) {
    document.getElementById(id).classList.toggle("show");

    //close the other dropdown if opened
    if(id.includes('1')){
      document.getElementById('myDropdown2').classList.contains('show')?document.getElementById('myDropdown2').classList.toggle("show"):'';
    }
    else{
      document.getElementById('myDropdown1').classList.contains('show')?document.getElementById('myDropdown1').classList.toggle("show"):'';
    }

  }

  menuToggleOnOutClick(){
    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
          var openDropdown = dropdowns[i];
          if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
          }
        }
      }
    }
  }
  
  logout() {
    //TODO: return to the old logout method fter the login for users is implemnted
    // this.authService.logout('/login').subscribe(tokenInfo => console.log(JSON.stringify(tokenInfo, null, 4)));
    this.authService.fakeLogout();
  }

  loggedIn(){
    return localStorage.getItem('logged')
  }

}

