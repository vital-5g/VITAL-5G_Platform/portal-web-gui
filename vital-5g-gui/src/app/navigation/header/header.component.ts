import { Component, OnInit , EventEmitter,Output} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() sideNavigationToggle = new EventEmitter();
  urlInfo : string;
  linkDash:boolean;
  showSideNav = false;
  constructor(private router: Router) {
    this.router.events.subscribe((e:any) => {
      let url = e.url;
      if(url){
        if(e.url == ''|| e.url == '/' || e.url == '/login'|| e.url == '/register')
        {
          this.showSideNav = false;
        }
        else{
          this.showSideNav = true;
        }
      }
    });
}
  ngOnInit(): void {
    /*
    this.router.events.subscribe((routerData) => {
      this.urlInfo=routerData['url'];
      if(this.urlInfo=='/login')
      this.linkDash=true;
      else{
        this.linkDash=false;
      }
   })   
   */
  }
  
  onToggleOpenSidenav(){
    this.sideNavigationToggle.emit()
  }
  changeStyleReg(){
    let elem1: HTMLElement = document.getElementById('show_reg');
    elem1.setAttribute("style", "display:none;");

    let elem: HTMLElement = document.getElementById('show_login');
    elem.setAttribute("style", "display:inline;");
  }
  changeStyleLog(){
    let elem1: HTMLElement = document.getElementById('show_reg');
    elem1.setAttribute("style", "display:inline;");

    let elem: HTMLElement = document.getElementById('show_login');
    elem.setAttribute("style", "display:none;");
  }
  
}
