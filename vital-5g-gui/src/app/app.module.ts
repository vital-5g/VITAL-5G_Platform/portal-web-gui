import { RegisterComponent } from './register/register.component';
import { DescriptorsVsComponent } from './descriptors-vs/descriptors-vs.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import{FlexLayoutModule} from '@angular/flex-layout'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import{ MaterialModule }from './material/material.module'
import { from } from 'rxjs';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BlueprintsEcComponent } from './blueprints-components/blueprints-ec/blueprints-ec.component';
import { BlueprintsTcComponent } from './blueprints-components/blueprints-tc/blueprints-tc.component';
import { BlueprintsEComponent } from './blueprints-components//blueprints-e/blueprints-e.component';
import { BlueprintsVsComponent } from './blueprints-components/blueprints-vs/blueprints-vs.component';
import { DescriptorsEComponent } from './descriptors-e/descriptors-e.component';
import { DescriptorsEDetailsComponent } from './descriptors-e-details/descriptors-e-details.component';
import { DescriptorsESchedulerComponent } from './descriptors-e-scheduler/descriptors-e-scheduler.component';
import { DescriptorsEStepperComponent } from './descriptors-e-stepper/descriptors-e-stepper.component';
import { DescriptorsEcComponent } from './descriptors-ec/descriptors-ec.component';
import { DescriptorsTcComponent } from './descriptors-tc/descriptors-tc.component';
import { ExperimentSubToolbarComponent } from './experiment-sub-toolbar/experiment-sub-toolbar.component';
import { ExperimentsMgmtDialogComponent } from './experiments-mgmt-dialog/experiments-mgmt-dialog.component';
import { ExperimentsExecuteDialogComponent } from './experiments-execute-dialog/experiments-execute-dialog.component';
import { ExperimentsResultsDialogComponent } from './experiments-results-dialog/experiments-results-dialog.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle'
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule,MatDialogRef } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//import { CytoscapeModule } from 'ngx-cytoscape';
import { AppRoutes } from './app.routes';
import { BlueprintsVsStepperComponent } from './blueprints-components/blueprints-vs-stepper/blueprints-vs-stepper.component';
import { BlueprintsEStepperComponent } from './blueprints-components/blueprints-e-stepper/blueprints-e-stepper.component';
import { BlueprintsEDetailsComponent } from './blueprints-components/blueprints-e-details/blueprints-e-details.component';
import { BlueprintsGraphComponent } from './blueprints-graph/blueprints-graph.component';
import { BlueprintsVsDetailsComponent } from './blueprints-components/blueprints-vs-details/blueprints-vs-details.component';
import { MessagesComponent } from './messages/messages.component';
import { BlueprintsEcDetailsComponent } from './blueprints-components/blueprints-ec-details/blueprints-ec-details.component';
import { BlueprintsEcStepperComponent } from './blueprints-components/blueprints-ec-stepper/blueprints-ec-stepper.component';
import { DescriptorsVsDetailsComponent } from './descriptors-vs-details/descriptors-vs-details.component';
import { CatalogueSubToolbarComponent } from './catalogue-sub-toolbar/catalogue-sub-toolbar.component';
import { DesignSwitchComponent } from './design-switch/design-switch.component';
import { ExperimentSwitchComponent } from './experiment-switch/experiment-switch.component';
import { ExperimentsDetailsComponent } from './experiments-details/experiments-details.component';
import { SitesSwitchComponent } from './sites-switch/sites-switch.component';
import {FooterComponent} from './navigation/footer/footer.component';
import {HeaderComponent} from './navigation/header/header.component';
import {SidenavListComponent} from './navigation/sidenav-list/sidenav-list.component';
import {LoginSubToolbarComponent} from './login-sub-toolbar/login-sub-toolbar.component';
import { NfvVnfDialogComponent } from './nfv-components/nfv-vnf-dialog/nfv-vnf-dialog.component';
import { NfvNsDialogComponent } from './nfv-components/nfv-ns-dialog/nfv-ns-dialog.component';
import { NfvNsGraphDialogComponent } from './nfv-components/nfv-ns-graph-dialog/nfv-ns-graph-dialog.component';
import { NfvVnfGraphDialogComponent } from './nfv-components/nfv-vnf-graph-dialog/nfv-vnf-graph-dialog.component';
import { NfvNsComponent } from './nfv-components/nfv-ns/nfv-ns.component';
import { NfvVnfComponent } from './nfv-components/nfv-vnf/nfv-vnf.component';
import { NfvPnfComponent } from './nfv-components/nfv-pnf/nfv-pnf.component';
import { TicketingSystemComponent } from './ticketing-system/ticketing-system.component';
import { ExperimentMetricDashboardComponent } from './experiment-metric-dashboard/experiment-metric-dashboard.component';
import { ExecutionTcDetailsComponent } from './execution-tc-details/execution-tc-details.component';
import {APP_BASE_HREF} from '@angular/common';
import { SupportToolsSchemasComponent } from './support-tools-schemas/support-tools-schemas.component';
import { SupportToolsNsdComponent } from './support-tools-nsd/support-tools-nsd.component';
import { SupportToolsComposerComponent } from './support-tools-composer/support-tools-composer.component';
import { FilesServiceComponent } from './files-service/files-service.component';
import { FileServiceDialogComponent } from './files-service/file-service-dialog/file-service-dialog.component';
import { FileUploadDialogComponent } from './files-service/file-upload-dialog/file-upload-dialog.component';
import { FileDialogComponent } from './files-service/file-dialog/file-dialog.component';
import { FileDpRequestDialogComponent } from './files-service/file-dp-request-dialog/file-dp-request-dialog.component';
import { SupportToolsValidationComponent } from './support-tools-validation/support-tools-validation.component';
import { DeploymentRequestComponent } from './deployment-request/deployment-request.component';
import { BlueprintsNetAppsComponent } from './blueprints-components/blueprints-net-apps/blueprints-net-apps.component';
import { BlueprintsNetappStepperComponent } from './blueprints-components/blueprints-netapp-stepper/blueprints-netapp-stepper.component';
import {MatDividerModule} from '@angular/material/divider';
import { Header2Component } from './navigation/V2/header2/header2.component';
import { Sidenav2Component } from './navigation/V2/sidenav2/sidenav2.component';
import { FileUploadDNDDirective } from './Directives/file-upload-dnd.directive';
import { BlueprintNaDetailsComponent } from './blueprints-components/blueprint-na-details/blueprint-na-details.component';
import { DescriptorsNaComponent } from './descriptors-na/descriptors-na.component';
import { DescriptorsNaDetailsComponent } from './descriptors-na-details/descriptors-na-details.component';
import { AuthGuard } from './Guards/auth.guard';
import { DescriptorsVsStepperComponent } from './descriptors-vs-stepper/descriptors-vs-stepper.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { NewVSInstanceModalComponent } from './new-vs-instance-modal/new-vs-instance-modal.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { AuthInterceptor } from './intercepters/auth.interceptor';
import { ExperimentsComponent } from './experiments/experiments.component';
import { EquipmentsComponent } from './equipments/equipments.component';
import { BlueprintsTcDetailsComponent } from './blueprints-components/blueprints-tc-details/blueprints-tc-details.component';
import { DescriptorsTcDetailsComponent } from './descriptors-tc-details/descriptors-tc-details.component';
import { NewRunExperimentModalComponent } from './new-run-experiment-modal/new-run-experiment-modal.component';
import { RunExperimentsComponent } from './run-experiments/run-experiments.component';
import { RunExperimentsDetailsComponent } from './run-experiments-details/run-experiments-details.component';
import { NewExperimentExecutionModalComponent } from './new-experiment-execution-modal/new-experiment-execution-modal.component';
import { EquipmentsDetailComponent } from './equipments-detail/equipments-detail.component';
import { MultisiteFiveGComponent } from './multisite-five-g/multisite-five-g.component';
//import { EquipmentsComponent } from './src/app/equipments/equipments.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    BlueprintsVsComponent,
    BlueprintsEcComponent,
    BlueprintsEComponent,
    DescriptorsVsComponent,
    DescriptorsEcComponent,
    DescriptorsEComponent,
    NfvNsComponent,
    NfvVnfComponent,
    NfvPnfComponent,
    FooterComponent,
    HeaderComponent,
    BlueprintsVsStepperComponent,
    BlueprintsEStepperComponent,
    BlueprintsEDetailsComponent,
    LoginComponent,
    BlueprintsGraphComponent,
    BlueprintsVsDetailsComponent,
    MessagesComponent,
    BlueprintsEcDetailsComponent,
    BlueprintsEcStepperComponent,
    BlueprintsTcComponent,
    DescriptorsEStepperComponent,
    DescriptorsTcComponent,
    DescriptorsVsDetailsComponent,
    DescriptorsEDetailsComponent,
    CatalogueSubToolbarComponent,
    DesignSwitchComponent,
    ExperimentSwitchComponent,
    DescriptorsESchedulerComponent,
    ExperimentsComponent,
    ExperimentsDetailsComponent,
    SitesSwitchComponent,
    ExperimentSubToolbarComponent,
    SidenavListComponent,
    ExperimentsMgmtDialogComponent,
    ExperimentsExecuteDialogComponent,
    ExperimentsResultsDialogComponent,
    RegisterComponent,
    LoginSubToolbarComponent,
    NfvVnfDialogComponent,
    NfvNsDialogComponent,
    NfvNsGraphDialogComponent,
    NfvVnfGraphDialogComponent,
    NfvNsComponent,
    NfvVnfComponent,
    NfvPnfComponent,
    TicketingSystemComponent,
    ExperimentMetricDashboardComponent,
    ExecutionTcDetailsComponent,
    SupportToolsSchemasComponent,
    SupportToolsNsdComponent,
    SupportToolsComposerComponent,
    FilesServiceComponent,
    FileServiceDialogComponent,
    FileUploadDialogComponent,
    FileDialogComponent,
    FileDpRequestDialogComponent,
    SupportToolsValidationComponent,
    DeploymentRequestComponent,
    BlueprintsNetAppsComponent,
    BlueprintsNetappStepperComponent,
    Header2Component,
    Sidenav2Component,
    FileUploadDNDDirective,
    BlueprintNaDetailsComponent,
    DescriptorsNaComponent,
    DescriptorsNaDetailsComponent,
    DescriptorsVsStepperComponent,
    MonitoringComponent,
    NewVSInstanceModalComponent,
    ConfirmDialogComponent,
    EquipmentsComponent,
    BlueprintsTcDetailsComponent,
    DescriptorsTcDetailsComponent,
    NewRunExperimentModalComponent,
    RunExperimentsComponent,
    RunExperimentsDetailsComponent,
    NewExperimentExecutionModalComponent,
    EquipmentsDetailComponent,
    MultisiteFiveGComponent
  ],
  imports: [
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    BrowserModule,
    RouterModule.forRoot(AppRoutes, { useHash: true }),
    BrowserAnimationsModule,
    LayoutModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatBadgeModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatGridListModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTabsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDialogModule,
    //MatDialogRef,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRippleModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    HttpClientModule,
    //CytoscapeModule
    // MatDivider
    MatDividerModule

    
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
],

entryComponents: [
  ExperimentsMgmtDialogComponent,
  ExperimentsExecuteDialogComponent,
  ExperimentsResultsDialogComponent,
  NfvVnfDialogComponent,
  NfvNsDialogComponent,
  NfvNsGraphDialogComponent,
  NfvVnfGraphDialogComponent,
  ExecutionTcDetailsComponent,
  FileServiceDialogComponent,
  FileUploadDialogComponent,
  FileDialogComponent,
  FileDpRequestDialogComponent
],

providers: [
  //TODO: uncomment to run the token interceptor
  {
    provide:HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi:true
  },
{provide: LocationStrategy, useClass: HashLocationStrategy},
AuthGuard
],
bootstrap: [AppComponent]
})
export class AppModule { }


