import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BlueprintsVsService } from '../blueprints-vs.service';
import { ExpDescriptorInfo } from '../descriptors-e/exp-descriptor-info';
import { DescriptorsVsService } from '../descriptors-vs.service';
import { VsDescriptorInfo } from '../descriptors-vs/vs-descriptor-info';
import { ExperimentsService } from '../experiments.service';

@Component({
  selector: 'app-new-vs-instance-modal',
  templateUrl: './new-vs-instance-modal.component.html',
  styleUrls: ['./new-vs-instance-modal.component.css']
})
export class NewVSInstanceModalComponent implements OnInit {
  selectedVSD: string;
  selectedTestbed: string;
  allIMSI: [];

  constructor(
    private descriptorsVsService: DescriptorsVsService,
    public dialogRef: MatDialogRef<NewVSInstanceModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private blueprintsVsService: BlueprintsVsService,
    private experiment: ExperimentsService
  ) {}

  vsdList:VsDescriptorInfo[] = [];
  accessLevel = ["PUBLIC", "PRIVATE", "RESTRICTED"];
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  ngOnInit(): void {
    this.getVsdList();
  }

  getVsdList(){
    this.descriptorsVsService.getVsDescriptors().subscribe((vsDescriptorsInfos: VsDescriptorInfo[]) => 
    {
      this.vsdList = vsDescriptorsInfos;
    });
  }
  getVsDescriptor(vsdId: string) {
    this.descriptorsVsService.getVsDescriptor(vsdId).subscribe((vsDescriptorInfo: VsDescriptorInfo) => 
      {
        this.getVsBlueprint(vsDescriptorInfo['vsDescriptorId'], vsDescriptorInfo['vsBlueprintId']);
        
      });
    }
    getVsBlueprint(vsdId: string, vsbId: string) {
      this.blueprintsVsService.getVsBlueprint(vsbId).subscribe(vsBlueprintInfo => {
        this.selectedTestbed = vsBlueprintInfo['testbed'];
        this.experiment.getIMSIs(this.selectedTestbed).subscribe(any => {
          if(any['availableImsis'] != undefined){
          this.allIMSI = any['availableImsis'];
          //console.log(this.allIMSI)

          }
          })
      })
    }
    check: string[] = [];
    onCheckboxChange(option,alloptions, event) {
      if(event.target.checked) {
        //console.log(option)
        this.check.push(option)
      } 
      else {
        for(var i=0 ; i < alloptions.length; i++) {
          if(this.check[i] == option) {
            this.check.splice(i,1);
         }
       }
       //this.data={"imsiIds" : this.check}
      }
      this.data.imsiIds = this.check;
 }
  getTestbed(event: any){
    const value = event.value;
   this.selectedVSD = value;
   this.getVsDescriptor(this.selectedVSD);
   
  }
  
}
