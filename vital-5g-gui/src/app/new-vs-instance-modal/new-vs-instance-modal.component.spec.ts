import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVSInstanceModalComponent } from './new-vs-instance-modal.component';

describe('NewVSInstanceModalComponent', () => {
  let component: NewVSInstanceModalComponent;
  let fixture: ComponentFixture<NewVSInstanceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVSInstanceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVSInstanceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
