import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { DescriptorsTcService } from '../descriptors-tc.service';
import { TcDescriptorInfo } from '../descriptors-tc/tc-descriptor-info';
import { DescriptorsTcDetailsDatasource, DescriptorTCDetailsItemKV } from './descriptors-tc-details-datasource';

@Component({
  selector: 'app-descriptors-tc-details',
  templateUrl: './descriptors-tc-details.component.html',
  styleUrls: ['./descriptors-tc-details.component.css']
})
export class DescriptorsTcDetailsComponent implements OnInit, AfterViewInit {

   
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false})
  table: MatTable<DescriptorTCDetailsItemKV>;
  dataSource: DescriptorsTcDetailsDatasource;

  tableData: DescriptorTCDetailsItemKV[] = [];
  testcase: any;
  cps: DescriptorTCDetailsItemKV[] = [];

  displayedColumns = ['key', 'value'];
  testcasecolumns = ['id','name', 'version','executionAction','configurationAction', 'resetAction'];
  
  constructor(private descriptorTcService:  DescriptorsTcService) { }
  ngAfterViewInit(): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {
    var tcdId = localStorage.getItem('tcdId');
    this.dataSource = new DescriptorsTcDetailsDatasource(this.tableData);
    this.getTcDescriptor(tcdId);
  }
  getTcDescriptor(tcdId: string) {
    this.descriptorTcService.getTcDescriptor(tcdId).subscribe((tcDescriptorInfo: TcDescriptorInfo) =>
    {
      var tcDescriptor = tcDescriptorInfo;
      this.tableData.push({key: "Name", value: [tcDescriptor['name']]});
      this.tableData.push({key: "Id", value: [tcDescriptor['testcaseBlueprintId']]});
      this.tableData.push({key: "version", value: [tcDescriptor['version']]});
      var values = [];
      if (tcDescriptor['executionAction']){
        var executionAction = tcDescriptor['executionAction'];
        var id = executionAction.nfvoActionId;
        var ref = executionAction.nfvoReference;
        var input = '';
        for(var i = 0; i < executionAction.inputParameter.length; i++){
          input += (executionAction.inputParameter[i] + ',');
        }
        input = input.substring(0, input.length - 1);
        input == ''?values.push(  `${id},${ref}`  ):values.push(  `${id},${ref} ( ${input} )`  );
      

        this.tableData.push({key:'Execution Action', value:values});
      }

      values = [];
      if (tcDescriptor['configurationAction']){
        var confAction = tcDescriptorInfo['configurationAction'];
        var id = confAction.nfvoActionId;
        var ref = confAction.nfvoReference;
        var input = '';
        for(var i = 0; i < confAction.inputParameter.length; i++){
          input += (confAction.inputParameter[i] + ',');
        }
        input = input.substring(0, input.length - 1);
        input == ''?values.push( `${id},${ref}` ):values.push(  `${id},${ref}  ( ${input} )` );

        this.tableData.push({key:'Configuration Action', value:values});
      }

      values = [];
      if (tcDescriptor['resetAction']){
        var resetAction = tcDescriptorInfo['resetAction'];
        var id = resetAction.nfvoActionId;
        var ref = resetAction.nfvoReference;
        var input = '';
        for(var i = 0; i < resetAction.inputParameter.length; i++){
          input += (resetAction.inputParameter[i] + ',');
        }
        input = input.substring(0, input.length - 1);
        input == ''?values.push( `${id},${ref}` ):values.push(  `${id},${ref}  ( ${input} )` );

        this.tableData.push({key:'Reset Action', value:values});
      }
     this.dataSource = new DescriptorsTcDetailsDatasource(this.tableData);
     this.dataSource.sort = this.sort;
     this.table.dataSource = this.dataSource;
     
    });
  }

}
