import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(public dialog: MatDialog) { }

  openConfirmDialog(msg = 'Are you sure you want to delete this record?'){
    return this.dialog.open(ConfirmDialogComponent, {
      width: '390px',
      // disableClose:true,
      panelClass:'confirm-dialog-container',
      data: {message: msg}
    });
  }
}
