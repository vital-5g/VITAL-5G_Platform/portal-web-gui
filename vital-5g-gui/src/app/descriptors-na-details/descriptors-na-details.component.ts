import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { DescriptorsNaDetailsDataSource, DescriptorsNaDetailsItemKV } from './descriptors-na-details.datasource';
import { NaDescriptorInfo } from '../descriptors-na/na-descriptor-info';
import { DescriptorsNaService } from '../NetAppsServices/descriptors-na.service';

@Component({
  selector: 'app-descriptors-na-details',
  templateUrl: './descriptors-na-details.component.html',
  styleUrls: ['./descriptors-na-details.component.css']
})
export class DescriptorsNaDetailsComponent implements OnInit {

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<DescriptorsNaDetailsItemKV>;
  dataSource: DescriptorsNaDetailsDataSource;

  tableData: DescriptorsNaDetailsItemKV[] = [];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['key', 'value'];

  constructor(private  descriptorsNaService: DescriptorsNaService) {}

  ngOnInit() {
    var vsdId = localStorage.getItem('vsdId');
    this.dataSource = new DescriptorsNaDetailsDataSource(this.tableData);
    this.getVsDescriptor(vsdId);
    // let elem1: HTMLElement = document.getElementById('show_blue');
    // elem1.setAttribute("style", "display:none;");
  }

  getVsDescriptor(vsdId: string) {
    this.descriptorsNaService.getVsDescriptor(vsdId).subscribe((naDescriptorInfo: NaDescriptorInfo) => 
      {
        //console.log(naDescriptorInfo);

        this.tableData.push({key: "Id", value: [naDescriptorInfo['vsDescriptorId']]});
        this.tableData.push({key: "Version", value: [naDescriptorInfo['version']]});
        this.tableData.push({key: "Name", value: [naDescriptorInfo['name']]});
        this.tableData.push({key: "VS Blueprint Id", value: [naDescriptorInfo['vsBlueprintId']]});
        this.tableData.push({key: "Service Slice Type", value: [naDescriptorInfo['sst']]});
        this.tableData.push({key: "Management Type", value: [naDescriptorInfo['managementType']]});
        var values = [];
        if (naDescriptorInfo['qosParameters']!=undefined){
          var qosParameters = new Map(Object.entries(naDescriptorInfo['qosParameters']));
          qosParameters.forEach((value: string, key: string) => {
            values.push(key + ": " + value)
          });
          this.tableData.push({key: "QoS", value: values});
        }


        if (naDescriptorInfo['sla']) {
          this.tableData.push({key: "Service Creation Time", value: [naDescriptorInfo['sla']['serviceCreationTime']]});
          this.tableData.push({key: "Availability Coverage", value: [naDescriptorInfo['sla']['availabilityCoverage']]});
          this.tableData.push({key: "Low Cost", value: [naDescriptorInfo['sla']['lowCostRequired']]});
        }
        
        this.dataSource = new DescriptorsNaDetailsDataSource(this.tableData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
      });
  }
}
