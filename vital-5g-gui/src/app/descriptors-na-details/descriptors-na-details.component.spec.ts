import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptorsNaDetailsComponent } from './descriptors-na-details.component';

describe('DescriptorsNaDetailsComponent', () => {
  let component: DescriptorsNaDetailsComponent;
  let fixture: ComponentFixture<DescriptorsNaDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriptorsNaDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptorsNaDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
