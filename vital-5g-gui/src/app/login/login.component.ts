import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService, RegistrationDetails, RoleDetails, Role } from '../auth.service';
import { UsersService, User } from '../users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  
  hide = true;
  loginFormGroup: FormGroup;
  roles: Role[];
  username: string ="";
  password: string ="";

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private usersService: UsersService
    ) { }

  ngOnInit() {
    this.authService.clearLogin();
    // TODO:remove comment below after the login for users is implemnted (if the request for getRoles was created as well, roles might be implemented on the token, so the line below wont be required)
    //this.getRoles();
    this.loginFormGroup = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // this.loginFormGroup = new FormGroup({
    //   username: new FormControl(),
    //   password: new FormControl() 
    // });
    
    // var sheetToChangeStyle = document.getElementById('show_reg');
    // sheetToChangeStyle.style.display = 'inline';
    // var sheetToChangeStyle = document.getElementById('show_login');
    // sheetToChangeStyle.style.display = 'none';

    // var sheetToBeRemoved = document.getElementById('menu_show');
    // var sheetParent = sheetToBeRemoved.parentNode;
    // sheetParent.removeChild(sheetToBeRemoved);

  }

  login() {
    this.authService.login(this.loginFormGroup.value,'/home').subscribe(resp =>{
    
    // localStorage.setItem('token', token)
    // localStorage.setItem('logged', 'true')
    // localStorage.setItem('roles','DESIGNER , EXPERIMENTER , Experimenter , SITE_MANAGER , ExperimentDeveloper , VnfDeveloper , SiteManager' )

    // this.router.navigate(['home']).then(() => {
    //   window.location.reload();
    // });
    }, error =>{

    })
    localStorage.setItem('username',this.username);
    localStorage.setItem('password',this.password);

return
    // let token = this.randomToken()
    
    // localStorage.setItem('token', token)
    // localStorage.setItem('logged', 'true')
    // localStorage.setItem('roles','DESIGNER , EXPERIMENTER , Experimenter , SITE_MANAGER , ExperimentDeveloper , VnfDeveloper , SiteManager' )

    // this.router.navigate(['home']).then(() => {
    //   window.location.reload();
    // });

    // var loginInfo = {};

    // var email = this.loginFormGroup.get('username').value;
    
    // var password = this.loginFormGroup.get('password').value;
    // if (email && password) {
    //   loginInfo['email'] = email;
    //   loginInfo['password'] = password;
    //  /*
    //  JSON.stringify(tokenInfo, null, 4),
    //     localStorage.getItem('token'),
    //      localStorage.getItem('roles') */
    //   this.authService.login(loginInfo, '/home').subscribe(tokenInfo => {
        
    //   });
    // }    
  }

  getRoles(){
    this.authService.getRoles()
    .pipe()
    .subscribe(
        data => {
          console.log(data['details']);
          this.roles = data['details'];
        },
        error => {
          console.log('RegisterComponent > Error retrieving roles during registration - ' + error);
      });    
  }


    randomString(length, chars) {
        var result = '';
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        return result;
    }

    randomToken(){
      return this.randomString(40, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    }
  
}
