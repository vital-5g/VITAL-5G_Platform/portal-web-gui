export class TcDescriptorInfo  {
    testCaseDescriptorId: string;
    name: string;
    version: string;
    testCaseBlueprintId: string;
    userParameters: Map<string, string>;
    public: boolean;
    executionAction: {
        nfvoActionId: string,
        inputParameter: [],
        nfvoReference: string
    };
    configurationAction: {
        nfvoActionId: string,
        inputParameter: [],
        nfvoReference: string
    };
    resetAction: {
        nfvoActionId: string,
        inputParameter: [],
        nfvoReference: string
    };
}