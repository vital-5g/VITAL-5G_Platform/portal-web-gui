import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { DescriptorsTcDataSource } from './descriptors-tc-datasource';
import { TcDescriptorInfo } from './tc-descriptor-info';
import { DescriptorsTcService } from '../descriptors-tc.service';
import { Router } from '@angular/router';
import { BlueprintsTcService } from '../blueprints-tc.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-descriptors-tc',
  templateUrl: './descriptors-tc.component.html',
  styleUrls: ['./descriptors-tc.component.css']
})
export class DescriptorsTcComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<TcDescriptorInfo>;
  dataSource: DescriptorsTcDataSource;
  tableData: TcDescriptorInfo[] = [];
  idToTcbId: Map<string, Map<string, string>> = new Map();

  selectedIndex = 0;
  tcFormGroup: FormGroup;
  uploadFormGroup: FormGroup;
  tcdObj: Object;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'name', 'version'] //, 'testCaseBlueprintId', 'userParameters', 'public'];
  executionaction_param: FormArray;
  configurationaction_param: FormArray;
  resetaction_param: FormArray;

  constructor(private descriptorsTcService: DescriptorsTcService, 
    private blueprintsTcService: BlueprintsTcService,
    private router: Router,
    private _formBuilder: FormBuilder,
    private authService: AuthService,
    ) {}

  ngOnInit() {
    this.tcFormGroup = this._formBuilder.group({
      //description: [''],
      name: ['', Validators.required],
      version: ['', Validators.required],

      // EA = executionAction
      EAnfvoActionId: ['',Validators.required],
      EAnfvoReference: ['',Validators.required],
      executionaction_param: this._formBuilder.array([this.createExecutionAction()]),

      // CA = configurationAction
      CAnfvoActionId: ['',Validators.required],
      CAnfvoReference: ['',Validators.required],
      configurationaction_param: this._formBuilder.array([this.createConfigurationAction()]),
      
      // RA = resetAction
      RAnfvoActionId: ['',Validators.required],
      RAnfvoReference: ['',Validators.required],
      resetaction_param: this._formBuilder.array([this.createResetAction()])
   
    });
    this.uploadFormGroup = this._formBuilder.group({
      tcbFileCtrl: ['', Validators.required]
    });

    this.dataSource = new DescriptorsTcDataSource(this.tableData);
    this.getTestCaseDescriptors();
    // let elem1: HTMLElement = document.getElementById('show_blue');
    // elem1.setAttribute("style", "display:none;");
  }

  
  // execution action
  createExecutionAction(): FormGroup{
    return this._formBuilder.group(
      {
        inputParamName:['',Validators.required],
      }
    );
  }

  addExcActionParam(): void {
    this.executionaction_param = this.tcFormGroup.get('executionaction_param') as FormArray;
    this.executionaction_param.push(this.createExecutionAction());
  }

  removeExcActionParam() {
    this.executionaction_param = this.tcFormGroup.get('executionaction_param') as FormArray;
    this.executionaction_param.removeAt(this.executionaction_param.length - 1);
  }

  // configuration action
  createConfigurationAction(): FormGroup{
    return this._formBuilder.group(
      {
        CAinputParamName:['',Validators.required],
      }
    );
  }

  addConfActionParam(): void {
    this.configurationaction_param = this.tcFormGroup.get('configurationaction_param') as FormArray;
    this.configurationaction_param.push(this.createConfigurationAction());
  }

  removeConfActionParam() {
    this.configurationaction_param = this.tcFormGroup.get('configurationaction_param') as FormArray;
    this.configurationaction_param.removeAt(this.configurationaction_param.length - 1);
  }

  
  // reset action
  createResetAction(): FormGroup{
    return this._formBuilder.group(
      {
        RAinputParamName:['',Validators.required]
      }
    );
  }

  addResetActionParam(): void {
    this.resetaction_param = this.tcFormGroup.get('resetaction_param') as FormArray;
    this.resetaction_param.push(this.createResetAction());
  }

  removeResetActionParam() {
    this.resetaction_param = this.tcFormGroup.get('resetaction_param') as FormArray;
    this.resetaction_param.removeAt(this.resetaction_param.length - 1);
  }


  getTestCaseDescriptors() {
    this.descriptorsTcService.getTcDescriptors().subscribe((tcDescriptorsInfos: TcDescriptorInfo[]) => 
      {
        //console.log(tcDescriptorsInfos);
        this.tableData = tcDescriptorsInfos;

        for (var i = 0; i < tcDescriptorsInfos.length; i ++) {
          this.idToTcbId.set(tcDescriptorsInfos[i]['testCaseDescriptorId'], new Map());
          this.getTcBlueprint(tcDescriptorsInfos[i]['testCaseDescriptorId'], tcDescriptorsInfos[i]['testCaseBlueprintId']);
        } 

        this.dataSource = new DescriptorsTcDataSource(this.tableData);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.table.dataSource = this.dataSource;
      });
  }

  getTcBlueprint(tcdId: string, tcbId: string) {
    this.blueprintsTcService.getTcBlueprint(tcbId).subscribe(tcBlueprintInfo => {
      var names = this.idToTcbId.get(tcdId);
      names.set(tcbId, tcBlueprintInfo['name']);
    })
  }

  viewTcBlueprint(tcbId: string) {
    //console.log(tcbId);
    localStorage.setItem('tcbId', tcbId);

    this.router.navigate(["/blueprints_tc"]);
  }
  viewTcDescriptor(tcDescriptorId: string) {
    localStorage.setItem('tcdId', tcDescriptorId);
    this.router.navigate(["/descriptors_tc_details"]);
  }
  deleteTcDescriptor(tcDescriptorId: string) {
    return this.descriptorsTcService.deleteTcDescriptor(tcDescriptorId).subscribe(
      () => { this.getTestCaseDescriptors(); }
    );

  }
  
  hideNextShowSubmit($event:any){
    var uploadProcedure = document.getElementById("uploadProcedure");
    var guidedProcedure = document.getElementById("guidedProcedure");
    if ($event.source.checked){
      uploadProcedure.style.display = 'inline';
      guidedProcedure.style.display = 'none';
    } else {
      guidedProcedure.style.display = 'inline';
      uploadProcedure.style.display = 'none';
    }

  }

  onUploadedTcd(event: any, tcds: File[]) {
    let promises = [];

    for (let tcd of tcds) {
      if(tcd.type=='application/json' && tcd.name.includes('json')){

        let vsdPromise = new Promise(resolve => {
            let reader = new FileReader();
            reader.readAsText(tcd);
            reader.onload = () => resolve(reader.result);
        });
        promises.push(vsdPromise);
    }else{
      this.authService.log(`the file is not json`, 'FAILED', false);
      (<HTMLInputElement> document.getElementById("submit")).disabled = true;  

    }
  }
  if(promises.length > 0){
    Promise.all(promises).then(fileContents => {
        this.tcdObj = JSON.parse(fileContents[0]);
        // this.encService.validateTcDescriptor(this.tcdObj)
        // .subscribe(res => {
        //   if(res===undefined){
        //     (<HTMLInputElement> document.getElementById("submit")).disabled = true;  
        //   }else{
        //     (<HTMLInputElement> document.getElementById("submit")).disabled = false;  
        //   }
        // });
           
      });
    }
  }

  createTCBViaJSONFile(tcd: File[]){
    if (tcd.length > 0) {
      var tcb = tcd[0];
      let promises = [];
      let tcbPromise = new Promise(resolve => {
          let reader = new FileReader();
          reader.readAsText(tcb);
          reader.onload = () => resolve(reader.result);
      });
      promises.push(tcbPromise);
      Promise.all(promises).then(fileContents => {
        var onBoardTcRequest = JSON.parse(fileContents[0]);

        this.descriptorsTcService.postTcDescriptor(onBoardTcRequest)
        .subscribe(() => {
          this.tcFormGroup.reset();
          this.selectedIndex = 0;
          this.getTestCaseDescriptors();
        }
        );
      });
    }
  }

  createOnBoardTcDescriptorRequest() {
    if(! this.tcFormGroup.invalid){
      var onBoardTcRequest = JSON.parse('{}');
      //var testCaseBlueprint = JSON.parse('{}');
      var executionAction = JSON.parse('{}');
      var configurationAction = JSON.parse('{}');
      var resetAction = JSON.parse('{}');

      //var description = this.tcFormGroup.get('description').value;
      var name = this.tcFormGroup.get('name').value;
      // var executionScript = this.tcFormGroup.get('executionScript').value;
      // var configurationScript = this.tcFormGroup.get('configurationScript').value;
      // var resetConfigScript = this.tcFormGroup.get('resetConfigScript').value;
      var version = this.tcFormGroup.get('version').value;

      // testCaseBlueprint['description'] = description;
      // testCaseBlueprint['name'] = name;
      // testCaseBlueprint['executionScript'] = executionScript;
      // testCaseBlueprint['configurationScript'] = configurationScript;
      // testCaseBlueprint['resetConfigScript'] = resetConfigScript;
      // testCaseBlueprint['version'] = version;

      // var userParams = this.tcFormGroup.controls.user_items as FormArray;
      // var user_controls = userParams.controls;
      // var userParamsMap = JSON.parse('{}');

      // for (var j = 0; j < user_controls.length; j++) {
      //   //console.log(user_controls[j].value);
      //   if ((user_controls[j].value)['userParamName'] != "") {
      //     userParamsMap[(user_controls[j].value)['userParamName']] = (user_controls[j].value)['userParamValue'];
      //   }
      // }

      // testCaseBlueprint['userParameters'] = userParamsMap;

      // var infraParams = this.tcFormGroup.controls.infra_items as FormArray;
      // var infra_controls = infraParams.controls;
      // var infraParamsMap = JSON.parse('{}');

      // for (var j = 0; j < infra_controls.length; j++) {
      //   //console.log(infra_controls[j].value);
      //   if ((infra_controls[j].value)['infraParamName'] != "") {
      //     infraParamsMap[(infra_controls[j].value)['infraParamName']] = (infra_controls[j].value)['infraParamValue'];
      //   }
      // }

      // testCaseBlueprint['infrastructureParameters'] = infraParamsMap;

      onBoardTcRequest['name'] = name;
      onBoardTcRequest['version'] = version;

      // execution action 
      
      var EAnfvoActionId = this.tcFormGroup.get('EAnfvoActionId').value;
      var EAnfvoReference = this.tcFormGroup.get('EAnfvoReference').value;

      var EAParams = this.tcFormGroup.controls.executionaction_param as FormArray;
      var EA_controls = EAParams.controls;
      var EAParamsMap = [];

      for (var j = 0; j < EA_controls.length; j++) {
        //console.log(EA_controls[j].value);
        if ((EA_controls[j].value)['inputParamName'] != "") {
          EAParamsMap.push((EA_controls[j].value)['inputParamName']);
        }
      }
      if(EAnfvoActionId != "" || EAParamsMap.length != 0 || EAnfvoReference != ""){
      executionAction['nfvoActionId']= EAnfvoActionId;
      executionAction['inputParameters']= EAParamsMap;
      executionAction['nfvoReference']= EAnfvoReference;

      onBoardTcRequest['executionAction'] = executionAction;
      }

      // configuration action 
      
      var CAnfvoActionId = this.tcFormGroup.get('CAnfvoActionId').value;
      var CAnfvoReference = this.tcFormGroup.get('CAnfvoReference').value;

      var CAParams = this.tcFormGroup.controls.configurationaction_param as FormArray;
      var CA_controls = CAParams.controls;
      var CAParamsMap = [];

      for (var j = 0; j < CA_controls.length; j++) {
        //console.log(EA_controls[j].value);
        if ((CA_controls[j].value)['CAinputParamName'] != "") {
          CAParamsMap.push((CA_controls[j].value)['CAinputParamName']);
        }
      }
      if(CAnfvoActionId != "" || CAParamsMap.length != 0 || CAnfvoReference != ""){
      configurationAction['nfvoActionId']= CAnfvoActionId;
      configurationAction['inputParameters']= CAParamsMap;
      configurationAction['nfvoReference']= CAnfvoReference;

      onBoardTcRequest['configurationAction'] = configurationAction;
      }

      // reset action 
      
      var RAnfvoActionId = this.tcFormGroup.get('RAnfvoActionId').value;
      var RAnfvoReference = this.tcFormGroup.get('RAnfvoReference').value;

      var RAParams = this.tcFormGroup.controls.resetaction_param as FormArray;
      var RA_controls = RAParams.controls;
      var RAParamsMap = [];

      for (var j = 0; j < RA_controls.length; j++) {
        //console.log(EA_controls[j].value);
        if ((RA_controls[j].value)['RAinputParamName'] != "") {
          RAParamsMap.push((RA_controls[j].value)['RAinputParamName']);
        }
      }
      if(RAnfvoActionId != "" || RAParamsMap.length != 0 || RAnfvoReference != ""){
      resetAction['nfvoActionId']= RAnfvoActionId;
      resetAction['inputParameters']= RAParamsMap;
      resetAction['nfvoReference']= RAnfvoReference;

      onBoardTcRequest['resetAction'] = resetAction;
      }

      console.log(onBoardTcRequest);
      debugger;
      this.descriptorsTcService.postTcDescriptor(onBoardTcRequest)
      .subscribe(() => {
        this.tcFormGroup.reset();
        this.selectedIndex = 0;
        this.getTestCaseDescriptors(); 
      });
    }

  }
}
