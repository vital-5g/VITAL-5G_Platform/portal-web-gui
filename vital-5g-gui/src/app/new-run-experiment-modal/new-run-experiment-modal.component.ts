import { Component,Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExpDescriptorInfo } from '../descriptors-e/exp-descriptor-info';
import { DescriptorsExpService } from '../descriptors-exp.service';
import { ExperimentsService } from '../experiments.service';
import { ExperimentInfo } from '../experiments/experiment-info';

@Component({
  selector: 'app-new-run-experiment-modal',
  templateUrl: './new-run-experiment-modal.component.html',
  styleUrls: ['./new-run-experiment-modal.component.css']
})
export class NewRunExperimentModalComponent implements OnInit {
  expdList: ExpDescriptorInfo[] = [];
  vsList: ExperimentInfo[] = [];
  

  constructor(private descriptorExpService: DescriptorsExpService,
    private expService: ExperimentsService,
    private dialogRef: MatDialogRef<NewRunExperimentModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    
  ) { }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.getExpdList();
    this.getVSList();
  }

  getExpdList(){
    this.descriptorExpService.getExpDescriptors().subscribe((expDescriptorsInfos: ExpDescriptorInfo[]) => 
    {
      this.expdList = expDescriptorsInfos;
    });
  }
  getVSList(){
    this.expService.getExperiments().subscribe((vsInstanceInfos: ExperimentInfo[]) => 
    {
      this.vsList = vsInstanceInfos;
    });
  }
}
