import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRunExperimentModalComponent } from './new-run-experiment-modal.component';

describe('NewRunExperimentModalComponent', () => {
  let component: NewRunExperimentModalComponent;
  let fixture: ComponentFixture<NewRunExperimentModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRunExperimentModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRunExperimentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
