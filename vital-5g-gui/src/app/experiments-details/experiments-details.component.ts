import { Component, OnInit, ViewChild, AfterViewInit, HostListener } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { ExperimentsDetailsDataSource, ExperimentsDetailsItemKV } from "./experiments-details.datasource";
import { ExperimentsService } from "../experiments.service";
import { ExperimentInfo } from "../experiments/experiment-info";
import { Execution } from "../experiments/execution";
import { SapInfo } from "../experiments/sapInfo";
import { DescriptorsExpService } from "../descriptors-exp.service";
import { environment } from "../environments/environments";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { MatDialog } from "@angular/material/dialog";
import { NewRunExperimentModalComponent } from "../new-run-experiment-modal/new-run-experiment-modal.component";
import { RunExperimentsService } from '../run-experiments.service';
import { RunExperimentInfo } from "../run-experiments/run-experiment-info";

@Component({
  selector: "app-experiments-details",
  templateUrl: "./experiments-details.component.html",
  styleUrls: ["./experiments-details.component.css"],
})
export class ExperimentsDetailsComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false })
  table: MatTable<ExperimentsDetailsItemKV>;
  dataSource: ExperimentsDetailsDataSource;

  tableData: ExperimentsDetailsItemKV[] = [];

  // experiment: ExperimentInfo;
  exptableData: any = [];
  expdataSource = new MatTableDataSource(this.tableData);

  experiment: any;

  executions: Execution[];

  sapInfos: SapInfo[];

  cps: ExperimentsDetailsItemKV[] = [];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ["key", "value"];
  executionsColumns = ["name", "id", "state", "reportUrl", "tcr"];

  innerWidth = 0;
  iframeW = 800;
  iframeH = 200;
  displayIFrames = false;
  showGrafana = false;
  srciframe1: string = "";
  srciframe2: string = "";
  srciframe3: string = "";
  iframe1Safe: SafeResourceUrl;
  iframe2Safe: SafeResourceUrl;

  
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.setIframeSize()
  }
  
  baseIFrameURL = environment.iframeURL;
  targetSite = "";
  constructor(
    public dialog: MatDialog,
    private experimentsService: ExperimentsService,
    private descriptorsExpService: DescriptorsExpService,
    protected sanitizer: DomSanitizer,
    private run_experimentService: RunExperimentsService

  ) {}

  ngOnInit() {
    var expId = localStorage.getItem("expId");
    this.dataSource = new ExperimentsDetailsDataSource(this.tableData);
    this.getExperiment(expId);
  }

  ngAfterViewInit(): void {
    this.innerWidth = window.innerWidth;

    setTimeout(() => {
      this.setIframeSize();
    }, 1000);
  }

  getExperiment(expId: string) {
    this.experimentsService.getExperiments().subscribe((vsInstances: any) => {
      this.experimentsService
        .getExperiment(expId, null)
        .subscribe((vsInstanceInfo: any) => {
          vsInstanceInfo["activeExperiments"] = [];

               console.log(vsInstanceInfo);
          for (var i = 0; i < vsInstances.length; i++) {
            if (vsInstances[i]["verticalServiceInstanceId"] === expId) {
              vsInstanceInfo["activeExperiments"] =
                vsInstances[i].activeExperiments;
              break;
            }
          }

          this.experiment = vsInstanceInfo;
          this.targetSite = this.experiment.testbed;
          this.showGrafana = true;
          this.setIframeSize();

          this.tableData.push({
            key: "Id",
            value: [this.experiment.verticalServiceInstanceId],
          });
          this.tableData.push({ key: "Name", value: [this.experiment.name] });
        
          this.tableData.push({
            key: "Status",
            value: [this.experiment.status],
          });

          for (var i = 0; i < vsInstanceInfo.activeExperiments.length; i++) {
            // if (
            //   this.experiment.experimentDescriptorId ===
            //   vsInstanceInfo[i]["expDescriptorId"]
            // ) {
            //   this.tableData.push({
            //     key: "Experiment Descriptor",
            //     value: [vsInstanceInfo[i]["name"]],
            //   });
            // }
          }

          //console.log(this.experiment.timeslot.startTime);
          this.tableData.push({
            key: "Target Sites",
            value: this.experiment.testbed,
          });

          this.tableData.push({
            key: "Description",
            value: [this.experiment.description],
          });

          this.tableData.push({
            key: "Tenant Id",
            value: [this.experiment.tenantId],
          });
          this.tableData.push({
            key: "5G Network Slice",
            value: [this.experiment.fiveGNetworkSliceId],
          });

          this.tableData.push({
            key: "Vertical service descriptor",
            value: [this.experiment.vsdId],
          });

          if(this.experiment.monitoringInfo !== undefined){
            if(this.experiment.monitoringInfo.infrastructureMonitoringUrl !== undefined){
              this.tableData.push({
                key: "Infrastructure Monitoring Url",
                value: [this.experiment.monitoringInfo.infrastructureMonitoringUrl],
              });}
            if(this.experiment.monitoringInfo.serviceMonitoringUrl !== undefined){
              this.tableData.push({
                key: "Service Monitoring Url",
                value: [this.experiment.monitoringInfo.serviceMonitoringUrl],
              });
              
            }
            if(this.experiment.monitoringInfo.networkMonitoringUrl !== undefined){
              this.tableData.push({
                key: "Network Monitoring Url",
                value: [this.experiment.monitoringInfo.networkMonitoringUrl],
              });
            }
            }
            
          // if (this.getRole().indexOf("SITE_MANAGER") >= 0) {
          //   if (this.experiment.tenantId != null) {
          //     this.tableData.push({
          //       key: "Tenant Id",
          //       value: [this.experiment.tenantId],
          //     });
          //   }
          //   if (this.experiment.lcTicketId != null) {
          //     this.tableData.push({
          //       key: "Ticket Id",
          //       value: [this.experiment.lcTicketId],
          //     });
          //   }
          //   if (
          //     this.experiment.openTicketIds != null &&
          //     this.experiment.openTicketIds.length > 0
          //   ) {
          //     this.tableData.push({
          //       key: "Open Ticket Ids",
          //       value: this.experiment.openTicketIds,
          //     });
          //   }
          // }

          // var startDate = new Date(this.experiment.timeslot.startTime);

          // var stopDate = new Date(this.experiment.timeslot.stopTime);

          // this.tableData.push({
          //   key: "Time Slot",
          //   value: [
          //     "Start Date: " + startDate.toLocaleString(),
          //     "Stop Date: " + stopDate.toLocaleString(),
          //   ],
          // });

          // var values = [];
          // if (this.experiment["sapInfo"] !== undefined) {
          //   for (var i = 0; i < this.experiment["sapInfo"].length; i++) {
          //     values.push(
          //       this.experiment["sapInfo"][i]["sapdId"] +
          //         " - " +
          //         this.experiment["sapInfo"][i]["address"]
          //     );
          //   }
          // }

          // this.tableData.push({ key: "Sap Info", value: values });
          // this.executions = this.experiment.executions;

          this.dataSource = new ExperimentsDetailsDataSource(this.tableData);
          this.dataSource.sort = this.sort;
          //this.dataSource.paginator = this.paginator;
          this.table.dataSource = this.dataSource;

          if(this.experiment.monitoringInfo !== undefined){
              if(this.experiment.monitoringInfo.infrastructureMonitoringUrl !== undefined){
                    this.srciframe1 = this.experiment.monitoringInfo.infrastructureMonitoringUrl;
                    this.showGrafana=true;
              }
              if(this.experiment.monitoringInfo.serviceMonitoringUrl !== undefined){
                this.srciframe2 = this.experiment.monitoringInfo.serviceMonitoringUrl;
                this.showGrafana =true;
              }
              if(this.experiment.monitoringInfo.networkMonitoringUrl !== undefined){
                this.srciframe3 = this.experiment.monitoringInfo.networkMonitoringUrl;
                this.showGrafana =true;
              }
          }
          else{
            this.srciframe1='';//'http://172.28.18.102:3000/d-solo/KQyHrje7z/vital-5g-centralized-monitoring-platform?orgId=1&from=1658207154724&to=1658228754725&panelId=6';
            this.srciframe2='';//'http://172.28.18.102:3000/d-solo/KQyHrje7z/vital-5g-centralized-monitoring-platform?orgId=1&from=1658207154724&to=1658228754725&panelId=8';
            this.srciframe3='';//'http://172.28.18.102:3000/d-solo/KQyHrje7z/vital-5g-centralized-monitoring-platform?orgId=1&from=1658207154724&to=1658228754725&panelId=8';
            
            this.showGrafana=true;
          }

        });
    });
  }
  cleanURL(oldURL: string): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(oldURL);
  }
  getRole() {
    return localStorage.getItem("role");
  }

  openResultsDialog(reportUrl: string) {
    window.open(reportUrl, "_blank");
  }

  setIframeSize() {
    setTimeout(() => {
      this.displayIFrames = false;
      let i1 = document.querySelector<HTMLElement>(".iframe-container")
      if(i1){
            this.iframeW = i1.offsetWidth + 300;
            this.iframeH = i1.offsetHeight + 100;
          this.displayIFrames = true;
      }
    }, 500);
  }
  getExperiments() {
    this.run_experimentService.getExperiments().subscribe((experimentInfos: RunExperimentInfo[]) =>
      {
        //console.log(expDescriptorsInfos);
        this.exptableData = experimentInfos;
        this.expdataSource = new MatTableDataSource(this.exptableData);
        this.expdataSource.sort = this.sort;
        this.expdataSource.paginator = this.paginator;
      });
  }
  openDialogs(): void {
    const dialogRef = this.dialog.open(NewRunExperimentModalComponent, {
      width: '30%',
      data: {id: this.experiment.verticalServiceInstanceId}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(!result){
        return
      }
      this.run_experimentService.postExperiment(result).subscribe(resp => {
        this.getExperiments();
      })
    });
  }
}
