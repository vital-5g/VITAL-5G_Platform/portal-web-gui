import { AfterViewInit, Component, HostListener, OnInit } from "@angular/core";
import { environment } from "../environments/environments";
import { MaterialModule } from "../material/material.module";

@Component({
  templateUrl: "./monitoring.component.html",
  styleUrls: ["./monitoring.component.css"],
})
export class MonitoringComponent implements OnInit, AfterViewInit {
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    this.setIframeSize()
  }

  innerWidth = 0;
  iframeW = 800;
  iframeH = 200;
  displayIFrames = false;
  baseIFrameURL = environment.iframeURL;
  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.innerWidth = window.innerWidth;

    setTimeout(() => {
      this.setIframeSize();
    }, 1000);
  }

  setIframeSize() {
    setTimeout(() => {
      this.displayIFrames = false;
    let i1 = document.getElementById("iframe1");
    this.iframeW = i1.offsetWidth - 15;
    this.iframeH = i1.offsetHeight - 10;
    this.displayIFrames = true
    }, 500);
  }
}
