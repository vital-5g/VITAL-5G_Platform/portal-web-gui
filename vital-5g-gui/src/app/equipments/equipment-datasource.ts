import { DataSource } from "@angular/cdk/collections";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { merge, Observable, of as observableOf } from "rxjs";
import { map } from "rxjs/operators";
import { EquipmentInfo } from "./equipment-info";

/**
 * Data source for the Equipments view. This class 
 * encapsulate all logic for fetching and manipulating the displayed data 
 * (including sorting, pagination, and filtering).
 */
export class EquipmentDatasource extends DataSource<EquipmentInfo> {
    data: EquipmentInfo[] = [];
    paginator: MatPaginator;
    sort: MatSort;

    constructor(data: EquipmentInfo[]) {
        super();
        this.data = data;
    }

    /**
     * Connect this datasource to the table. The table will only be updated when
     * the returned stream emits new items. @returns A stream of the items to be
     * rendered.
     */
    connect(): Observable<EquipmentInfo[]> {
        //combine everything that affects the rendered data into one update
        // stream for the data table to consume
        const dataMutations = [
            observableOf(this.data),
            this.paginator.page,
            this.sort.sortChange
        ];

        return merge(...dataMutations).pipe(map(() => {
            return this.getPagedData(this.getSortedData([...this.data]));
        }))
    }

    /**
     * Called when the table is being destroyed. Use this function to clean up any open
     * connections or free any held resources that were set up during connect.
     */
    disconnect() {}

    /**
     * Paginate the data on the client-side. If server-side pagination is used,
     * this would be replaced by requesting the proper data from the server.
     */
    private getPagedData(data: EquipmentInfo[]) {
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    }

    /**
     * Sort the data (client-side). If server-side sorting is used, this would be
     * replaced by requesting the proper data from the server.
     */
    private getSortedData(data: EquipmentInfo[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a,b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.type, b.type, isAsc);
                case 'id': return compare(+a.equipmentId, +b.equipmentId, isAsc);
                default: return 0;
            }
        })
    }
}

    /** Simple sort comparator for example ID/type columns (for client-side sorting). */
    function  compare(a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

