import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { EquipmentsService } from '../equipments.service';
import { EquipmentDatasource } from './equipment-datasource';
import { EquipmentInfo } from './equipment-info';

@Component({
  selector: 'app-equipments',
  templateUrl: './equipments.component.html',
  styleUrls: ['./equipments.component.css']
})
export class EquipmentsComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<EquipmentInfo>;
  dataSource: EquipmentDatasource;
  equipmentInfos: EquipmentInfo[] = [];
  idToEqipId: Map<string, Map<string, string>> = new Map();
  selectedIndex = 0;

  /** Columns displayed in the table. Column Ids can be added, removed, or reordered. */
  displayedColumns = ['expBlueprintId', 'name', 'version','view','delete','forcedelete'];

  constructor(private equimpmentService: EquipmentsService, private router: Router) { }

  ngOnInit() {
    this.dataSource = new EquipmentDatasource(this.equipmentInfos);
    this.getEquipments();
  }
  getEquipments(): void {
    this.equimpmentService.getEquipments().subscribe((equipmentInfos: EquipmentInfo[]) => {
      this.equipmentInfos = equipmentInfos;
      for(var i = 0; i < equipmentInfos.length; i++) {
        this.idToEqipId.set(equipmentInfos[i]['equipmentId'], new Map());
      }
      
      this.dataSource = new EquipmentDatasource(this.equipmentInfos);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.table.dataSource = this.dataSource;
    })
  }

  
  viewEquipment(eqpId: string) {
    //console.log(expId);
    localStorage.setItem('eqpId', eqpId);

    this.router.navigate(["/multisite_equipment_detail"]);
  }

  
  deleteEquipment(equipmentId: string) {
    this.equimpmentService.deleteEquipment(equipmentId).subscribe(
      () => { this.getEquipments(); }
    );
  }
  forcedeleteEquipment(equipmentId: string) {
    this.equimpmentService.forcedeleteEquipment(equipmentId).subscribe(
      () => {this.getEquipments();}
    );
  }

}
