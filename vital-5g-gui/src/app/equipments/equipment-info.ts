export class EquipmentInfo {
   equipmentId: string;
   location: string;
   type: string;
   accessLevel: string;
   subType: string;

}
