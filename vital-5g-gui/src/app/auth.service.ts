import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from './environments/environments';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import * as jwt_decode from "jwt-decode";

export class Token {
  access_token: string;
  refresh_token: string;
  username: string;
}

export class Role {
  id: string;
  name: string;
  clientRole: boolean;
  composite: boolean;
  containerId: string;
  description: string;
}

export class RoleDetails {
  details: Role[];
}


export class UseCases{
  useCases: string[];
}

export class RegistrationDetails {
  details: {
    user_id: string;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl = environment.rbacBaseUrl;
  private registerUrl = "register"
  private rolesInfoUrl = "realmroles";

  httpOptions = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      })
  };
  refreshhttpOptions = {
    headers: new HttpHeaders(
      { 'Content-Type':  'application/x-www-form-urlencoded',
      Accept: '*/*',
        //'Authorization': 'Bearer ' + localStorage.getItem('refreshtoken')
      })
  };

  loginHttpOptions = {
    headers: new HttpHeaders(
      { 
        'Content-Type':  'application/x-www-form-urlencoded',
        Accept: '*/*',
      })
  };

  constructor(private http: HttpClient,
    private messageService: MessageService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _location: Location ) { }


  registerUser(email: string, username: string, firstName: string,
    lastName: string, password: string, role: Role): Observable<RegistrationDetails> {
    let data = {
        "email": email,
        "username": username,
        "firstName": firstName,
        "lastName": lastName,
        "password": password,
        "roles": [role.name]
    };

    return this.http.post<RegistrationDetails>(this.baseUrl + this.registerUrl, data, this.httpOptions)
        .pipe(
            tap((data: RegistrationDetails) => {
                this.log(`login w/ id=${email}`, 'SUCCESS', true);
                return data;
            }),
            catchError(this.handleError<RegistrationDetails>('registerUser'))
    );
  }

  getRoles(): Observable<RoleDetails> {
    //TODO:uncomment below later if needed
    return null
    return this.http.get<RoleDetails>(this.baseUrl + this.rolesInfoUrl, this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched rolesDetails - SUCCESS')),
        catchError(this.handleError<RoleDetails>('getExpDescriptor'))
      );
  }

  getUseCases(): Observable<UseCases> {
    return this.http.get<UseCases>(this.baseUrl + "extra/use-cases", this.httpOptions)
      .pipe(
        tap(_ => console.log('fetched rolesDetails - SUCCESS')),
        catchError(this.handleError<UseCases>('getExpDescriptor'))
      );
  }

  setUpToken(token:any){
    localStorage.setItem('token', token.access_token);
    localStorage.setItem('refreshtoken', token.refresh_token);
    localStorage.setItem('logged', 'true')
    this.parseToken(token.access_token);
  }

  
  login(loginInfo: Object, redirection: string): Observable<Token> {
    let body = new URLSearchParams();
    body.set('username', loginInfo['username']);
    body.set('password',  loginInfo['password']);
    body.set('client_id', environment.client_id? environment.client_id : '');
    body.set('grant_type', environment.grant_type? environment.grant_type : '');
    body.set('client_secret', environment.client_secret? environment.client_secret : '');

     //console.log(body.toString()) //http://172.28.18.104:8080/auth/realms/vital5g/protocol/openid-connect/token
     //debugger
    return this.http.post(`${environment.authURL}token`, body.toString(), this.loginHttpOptions)
      .pipe(
        tap((token: Token) =>
        {
          console.log(token)
          //debugger
          if (false && token['details'] !== undefined && token['details'] ===
            'User account not activated. Please, wait until the administrator validates your account') {
            this.log(token['details'], 'FAILED', true);
          } else {
            // this.log(`login w/ id=${loginInfo['email']}`, 'SUCCESS', false);
            this.setUpToken(token)
            this.router.navigate([redirection]).then(() => {
              window.location.reload();
            });
          }
        })
        ,catchError(this.handleError<Token>('login'))
      );
  }

  refresh(refreshInfo: Object): Observable<Token> {
    
    debugger
    let body = new URLSearchParams();
    //body.set('username', localStorage.getItem('username'));
    //body.set('password',  localStorage.getItem('password'));
    body.set('client_id', environment.client_id? environment.client_id : '');
    body.set('grant_type', environment.grant_type? environment.refresh_grant_type : '');
    body.set('client_secret', environment.client_secret? environment.client_secret : '');
    body.set('refresh_token', localStorage.getItem('refreshtoken'));

    return this.http.post(`${environment.authURL}token`, body.toString(), this.refreshhttpOptions)
      .pipe(
        tap((token: Token) =>
        {
          debugger
          localStorage.setItem('token', token.access_token);
          localStorage.setItem('refreshtoken', token.refresh_token);
          //this.log(`refresh login`, 'SUCCESS', false);
        }),
        catchError(this.handleError<Token>('refresh'))
      );
  }

  logout(redirection: string): Observable<Token> {
    return this.http.get(this.baseUrl + 'logout', this.httpOptions)
      .pipe(
        tap((token: Token) =>
        {
          this.log(`logout`, 'SUCCESS', false);
          localStorage.removeItem('username');
          localStorage.removeItem('role');
          localStorage.removeItem('roles');
          localStorage.removeItem('token');
          localStorage.removeItem('refreshtoken');
          localStorage.setItem('logged', 'false')
          this.router.navigate([redirection]).then(() => {
            window.location.reload();
          });
        }),
        catchError(this.handleError<Token>('logout'))
      );
  }

  parseToken(token: string) {
    var decodedToken = jwt_decode(token);
    //console.log(JSON.stringify(decodedToken, null, 4));

    var username = decodedToken['preferred_username'];
    var name = decodedToken['name'];
    var email = decodedToken['email'];
    var roles = decodedToken['realm_access']['roles'];

    //this.usersService.setUser({roles, name, email, username});

    localStorage.setItem('roles', roles.toString());
    //TODO:remove below roles after the login for users is implemnted, then change the conditions for pages visibilities accourding to the new rules.
    // localStorage.setItem('roles','DESIGNER , EXPERIMENTER , Experimenter , SITE_MANAGER , ExperimentDeveloper , VnfDeveloper , SiteManager' )

    localStorage.setItem('username', username);
  }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401 || error.status == 400 || error.status == 0) {
        if(operation == 'login'){
          this.log(`Wrong login credentials`, 'FAILED', false);
        }
        else{
          debugger
          if (operation.indexOf('refresh') >= 0 || operation.indexOf('login') >= 0) {
            // TODO: better job of transforming error for user consumption
          //console.log("do not refresh");
          //debugger
            this.log(`${operation} failed: ${error.message}`, 'FAILED', false);
            localStorage.removeItem('username');
            localStorage.removeItem('role');
            localStorage.removeItem('roles');
            localStorage.removeItem('token');
            localStorage.removeItem('refreshtoken');
            //window.location.reload();
            this.router.navigate(['/login']).then(() => {
              window.location.reload();
            });
          } else {
          // console.log("do refresh")
          // debugger
            this.log(`${operation} failed: ${error.message}`, 'FAILED', false);
            this.refresh({
             // access_token: localStorage.getItem('token'),
              refresh_token: localStorage.getItem('refreshtoken')
            }).subscribe(token => {
              if (token) {
                console.log('Token successfully refreshed after 401');
              }
            });
          }
        }

      } else {
        if (false && error.status === 400) {
          if (operation.indexOf('refresh') >= 0 || operation.indexOf('login') >= 0) {
            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`, 'FAILED', false);
            localStorage.removeItem('username');
            localStorage.removeItem('role');
            localStorage.removeItem('roles');
            localStorage.removeItem('token');
            localStorage.removeItem('refreshtoken');
            this.router.navigate(['/login']).then(() => {
              window.location.reload();
            });
          }
        } else {
          debugger
            console.log(error.status + ' after ' + operation);
            this.log(`${operation} failed: ${error.error}`, 'FAILED', false);
        }
      }

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  handleValidatorError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
        if (error.status === 400  || error.status === 500) {
          console.log(error.message + ' after ' + operation);
          this.log(`${error.error.message} failed`, 'FAILED', false);
        } 
      return of(result as T);
    };
  }

  /** Log a Service message with the MessageService */
  log(message: string, action: string, reload: boolean) {
    this.messageService.add(`${message}`);
    this.openSnackBar(`${message}`, action, reload);
  }

  openSnackBar(message: string, action: string, reload: boolean) {
    this._snackBar.open(message, action, {
      duration: 0,
    }).afterDismissed().subscribe(() => {
      //console.log('The snack-bar was dismissed');
      /*if (reload)
        window.location.reload();
        */
    });
  }

  fakeLogout(){
    this.clearLogin();
    this.router.navigateByUrl('login')
  }

  clearLogin(){
    localStorage.removeItem('username');
    localStorage.removeItem('role');
    localStorage.removeItem('roles');
    localStorage.removeItem('token');
    localStorage.removeItem('refreshtoken');
    localStorage.setItem('logged', 'false')
  }
}
