import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';

import { RunExperimentsDetailsComponent } from './run-experiments-details.component';

describe('RunExperimentsDetailsComponent', () => {
  let component: RunExperimentsDetailsComponent;
  let fixture: ComponentFixture<RunExperimentsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports : [MatSnackBarModule,RouterTestingModule],
      declarations: [ RunExperimentsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    
    TestBed.configureTestingModule({imports: [HttpClientModule]});
    fixture = TestBed.createComponent(RunExperimentsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
