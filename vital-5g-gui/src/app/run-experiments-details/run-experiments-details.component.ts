import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { ExperimentsDetailsDataSource, RunExperimentsDetailsItemKV } from './run-experiments-details.datasource';
import { MatDialog } from "@angular/material/dialog";
import { RunExperimentsService } from '../run-experiments.service';
import { NewExperimentExecutionModalComponent } from '../new-experiment-execution-modal/new-experiment-execution-modal.component';
import { RunExperimentsComponent } from '../run-experiments/run-experiments.component';


@Component({
  selector: 'app-run-experiments-details',
  templateUrl: './run-experiments-details.component.html',
  styleUrls: ['./run-experiments-details.component.css']
})
export class RunExperimentsDetailsComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false })
  table: MatTable<RunExperimentsDetailsItemKV>;
  dataSource: ExperimentsDetailsDataSource;

  tableData: RunExperimentsDetailsItemKV[] = [];

  // experiment: ExperimentInfo;
  exptableData: any = [];
  expdataSource = new MatTableDataSource(this.tableData);

  experiment: any;

  displayedColumns = ["key", "value"];
  executionsColumns = ["elcmId", "status", "vsId"];

  constructor(
    public dialog: MatDialog,
    private run_experimentService: RunExperimentsService,
  ) { }
  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    var elcmId = localStorage.getItem("elcmId");
    this.dataSource = new ExperimentsDetailsDataSource(this.tableData);
    this.getExperiment(elcmId);
  }

  getExperiment(expId: string) {
    this.run_experimentService.getExperiments().subscribe((vsInstances: any) => {
      this.run_experimentService
        .getExperiment(expId, null)
        .subscribe((vsInstanceInfo: any) => {
          vsInstanceInfo["activeExperiments"] = [];

               console.log(vsInstanceInfo);
          for (var i = 0; i < vsInstances.length; i++) {
            if (vsInstances[i]["verticalServiceInstanceId"] === expId) {
              vsInstanceInfo["activeExperiments"] =
                vsInstances[i].activeExperiments;
              break;
            }
          }

          this.experiment = vsInstanceInfo;

          this.tableData.push({
            key: "Id",
            value: [this.experiment.elcmId],
          });
        
          this.tableData.push({
            key: "Status",
            value: [this.experiment.status],
          });

          var values = [];
          if (this.experiment["experimentConfigurationParameters"] !== undefined) {
            if (Object.keys(this.experiment["experimentConfigurationParameters"]).length > 0) {
              for (let item in this.experiment["experimentConfigurationParameters"]){
              values.push(
                item +
                  " - " +
                  this.experiment["experimentConfigurationParameters"][item]
              );
            }
            }
          }

          this.tableData.push({ key: "Experiment Configuration Parameters", value: values });

          this.tableData.push({
            key: "Experiment Descriptor",
            value: [this.experiment.experimentDescriptorId],
          });
          this.tableData.push({
            key: "Network Service Instance",
            value: [this.experiment.networkServiceInstanceId],
          });
          this.tableData.push({
            key: "testbed",
            value: [this.experiment.testbed],
          });
          this.tableData.push({
            key: "Vertical Service Instance",
            value: [this.experiment.vsId],
          });
          this.dataSource = new ExperimentsDetailsDataSource(this.tableData);
          this.dataSource.sort = this.sort;
          //this.dataSource.paginator = this.paginator;
          this.table.dataSource = this.dataSource;
        });
    });
  }

  openDialogs(): void {
    const dialogRef = this.dialog.open(NewExperimentExecutionModalComponent, {
      width: '30%',
      data: {},//{tenantId: 'NXW'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(!result){
        return
      }
    var elcmId = localStorage.getItem("elcmId");
      this.run_experimentService.postExperimentExecution(elcmId,result).subscribe(resp => {
        this.run_experimentService.getExperiments();
      })
    });
  }
}
