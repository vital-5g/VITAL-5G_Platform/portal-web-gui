

let AcceptedTypes = ['application/json', 'application/x-zip-compressed', 'application/zip','application/x-gzip',''];
let namesToInclude = ['json', 'zip', 'zip', '.gz', '.yaml'];
let MaxFileSize = 20 *1000000 //2mb;

enum files {
    JSON,
    ZIP,
    ZIP2,
    GZIP,
    YAML,
}

export{AcceptedTypes, namesToInclude, MaxFileSize, files}