import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultisiteFiveGComponent } from './multisite-five-g.component';

describe('MultisiteFiveGComponent', () => {
  let component: MultisiteFiveGComponent;
  let fixture: ComponentFixture<MultisiteFiveGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultisiteFiveGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultisiteFiveGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
