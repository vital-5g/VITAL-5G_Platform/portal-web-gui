# VITAL-5G-Gui

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Environment setup

### Install nodejs

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
node -v

```

### Install Angular CLI

```
sudo npm install -g @angular/cli
```

### Install the project dependencies

```
npm install 
```

## Development server (starting the application)

Run `cd vital-5g-gui`.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
Run `ng serve --open` for a dev server and opening the default browser.
The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Node compatibility

In case of any problem in building or running the application, use Node JS v12.20.2.
